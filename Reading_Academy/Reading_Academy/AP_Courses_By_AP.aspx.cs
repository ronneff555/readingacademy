﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Reading_Academy
{
    public partial class AP_Courses_By_AP : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["DevConnectionStr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    LoadRegionDropDown(this, null);


                }
                catch (Exception ex)
                {

                }
            }
            else
            {

            }
}

        private void LoadRegionDropDown(object sender, EventArgs e)
        {
            try
            {
                string constring = "Data Source=ESCXI-DATA-INT;Initial Catalog = Canvas_Test;Trusted_Connection=true; User ID = ESCXI-DATA-INT\rpeacock; Password=R3@d1ngAc@d!!;Integrated Security=True";
                using (SqlConnection con = new SqlConnection(constring))
                {
                    using (SqlCommand cmd = new SqlCommand("SELECT id, name, canvas_sub_account_id FROM authorized_providers WHERE id < 21 ORDER BY id", con))
                    {
                        cmd.CommandType = CommandType.Text;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                DropDownListRegions.DataSource = dt;
                                DropDownListRegions.DataBind();
                                DropDownListRegions.DataTextField = "Name";
                                DropDownListRegions.DataValueField = "canvas_sub_account_id";
                                DropDownListRegions.DataBind();
                                DropDownListRegions_SelectedIndexChanged(this, null);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }

        }
        protected void DropDownListRegions_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strRegionID = this.DropDownListRegions.SelectedValue.ToString();
            try
            {
                string conString = "Data Source=ESCXI-DATA-INT;Initial Catalog = Canvas_Test;Trusted_Connection=true; User ID = ESCXI-DATA-INT\rpeacock; Password=R3@d1ngAc@d!!;Integrated Security=True";
                using (SqlConnection con = new SqlConnection(conString))
                {
                    string strSQL = "";
                    strSQL = "SELECT convert(varchar,c.canvas_id) + '-' + c.name as Course, t.name AS Term ";
                    strSQL += " FROM course_dim c INNER JOIN enrollment_term_dim t ON c.enrollment_term_id=t.id ";
                    strSQL += " WHERE c.account_id LIKE('%" + strRegionID + "') ";
                    strSQL += " AND c.workflow_state = 'available' AND c.name NOT LIKE('%Passport%') ";
                    strSQL += " ORDER BY t.canvas_id, c.canvas_id";

                    using (SqlCommand cmd = new SqlCommand(strSQL, con))
                    {
                        cmd.CommandType = CommandType.Text;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {

                            SqlDataAdapter Adpt = new SqlDataAdapter(cmd);
                            DataTable dt = new DataTable();
                            Adpt.Fill(dt);
                            GridViewRegions.DataSource = dt;
                            GridViewRegions.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}