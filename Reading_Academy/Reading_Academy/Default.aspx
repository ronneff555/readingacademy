﻿<%@ Page Title="TRA Reading Academy" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Reading_Academy._Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!DOCTYPE html>

    <div>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

        <style>
            
            .center {
              text-align: center;
              border: 3px solid green;
            }

            body {
                font-family: Tahoma;
                font-size: 14pt;
            }

            table {
                width: 1000px;
            }

            td.header {
                background-color: #999999;
                font-weight: bold;
            }

            td.altrow {
                background-color: #eeeeee;
            }

            td.redtext {
                color: red;
                font-style: italic;
                font-weight: bold;
                font-size: 11pt;
            }

            span.texttip {
                font-size: 9pt;
                background-color: #ffffcc;
            }

            select {
                margin-top: 5px;
            }

            span.error {
                color: red;
            }

            div.applicationResults {
                display: none;
            }

            a {
                color: #f06037;
            }

            h3 {
                color: #0d6cb9;
            }
        </style>
    </div>


    <div>
        <table cellpadding="5" cellspacing="0" border="0">
            <tr>
                <td colspan="2">

                    <img src="images/TRA_logo.jpg" border="0" /></td>
                <td style="text-align: right; vertical-align: top;"><a href="cl/leader_login.php">Sign In</a></td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr />
                    <a href='/cl/'>
                        <img src='images/TRA_Home_Banner2.jpg' border='0' /></a><hr />

                </td>
            </tr>

            <tr>
                <td><a href="AP_Dashboard.aspx">
                    <img src="images/TRA_AP_button.png" border="0" /></a></td>
                <td><a href="cl/leader_login.php">
                    <img src="images/TRA_CL_button.png" border="0" /></a></td>
                <td><a href='https://register.tealearn.com/' target='_blank'>
                    <img src="images/TRA_CH_button.png" border="0" /></a></td>
            </tr>

            <tr>
                <td colspan="3">
                    <h3>HB 3 Texas Reading Academies Development</h3>
                    As reading and writing are foundational to school success, TEA works to ensure that all Texas students have access to high-quality reading and writing instruction and highly-effective teachers. House Bill 3 addresses key reading practices. For the most updated information from TEA, please visit HB3 Reading Academies.
                        <br />
                    <br />
                    HB3 requires each teacher and principal in grades K-3 to attend reading academies by 2021-22. 
                        <br />
                    <br />
                    TEA plans to propose through rulemaking that enrollment in reading academies by the summer of 2022 adheres to this requirement. 
                        <br />
                    <br />
                    This would mean that there are three school years for completion:  2020-21, 2021-22, and 2022-23.
                </td>
            </tr>
        </table>
        <br /> 
        <br /> 
        <div class="center">
        <%--<asp:DropDownList AutoPostBack="true"  ID="DropDownListRegions" runat="server" OnSelectedIndexChanged="DropDownListRegions_SelectedIndexChanged" Width="166px"></asp:DropDownList>    --%>
 
 
        <div>  

 
        <br />  
        <asp:GridView ID="GridView1" runat="server" CellPadding="4" GridLines="None" ForeColor="#333333">  
            <AlternatingRowStyle BackColor="White" />  
            <Columns>
                <asp:BoundField ApplyFormatInEditMode="True" />
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True" />  
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />  
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />  
            <RowStyle BackColor="#EFF3FB" />  
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />  
            <SortedAscendingCellStyle BackColor="#F5F7FB" />  
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />  
            <SortedDescendingCellStyle BackColor="#E9EBEF" />  
            <SortedDescendingHeaderStyle BackColor="#4870BE" />  
        </asp:GridView>  
    </div>  
        <br />  
    </div>  
    </div>

    </html>

</asp:Content>
