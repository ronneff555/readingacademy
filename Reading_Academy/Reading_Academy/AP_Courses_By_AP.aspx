﻿<%@ Page Title="Courses By Authorized Provider" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AP_Courses_By_AP.aspx.cs" Inherits="Reading_Academy.AP_Courses_By_AP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

        <style>
            
            .center {
              text-align: center;
              border: 3px solid #507CD1;
            }

            body {
                font-family: Tahoma;
                font-size: 14pt;
            }

            table {
                text-align: left;
            }

            td.header {
                background-color: #999999;
                font-weight: bold;
            }

            td.altrow {
                background-color: #eeeeee;
            }

            td.redtext {
                color: red;
                font-style: italic;
                font-weight: bold;
                font-size: 11pt;
            }

            span.texttip {
                font-size: 9pt;
                background-color: #ffffcc;
            }

            select {
                margin-top: 5px;
            }

            span.error {
                color: red;
            }

            div.applicationResults {
                display: none;
            }

            a {
                color: #f06037;
            }

            h3 {
                color: #0d6cb9;
            }
        </style>
    </div>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">  
 
    <div>  
        <br /> 
        <br /> 
        <div class="center">
         
 
        <div>  

 <asp:DropDownList AutoPostBack="true"  ID="DropDownListRegions" runat="server" OnSelectedIndexChanged="DropDownListRegions_SelectedIndexChanged" Width="166px"></asp:DropDownList>    
        <br />  
        <asp:GridView ID="GridViewRegions" runat="server" CellPadding="4" GridLines="None" ForeColor="#333333" Width="1200px">  
            <AlternatingRowStyle BackColor="White" />  
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True" />  
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />  
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />  
            <RowStyle BackColor="#EFF3FB" />  
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />  
            <SortedAscendingCellStyle BackColor="#F5F7FB" />  
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />  
            <SortedDescendingCellStyle BackColor="#E9EBEF" />  
            <SortedDescendingHeaderStyle BackColor="#4870BE" />  
        </asp:GridView>  
    </div>  
        <br />  
    </div>  
    </div> 
     
</asp:Content>
