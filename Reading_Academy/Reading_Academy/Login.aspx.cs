﻿using System;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Reading_Academy
{
    public partial class Login : System.Web.UI.Page
    {

        static string connectionString = ConfigurationManager.ConnectionStrings["DevConnectionStr"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "usp_login";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserName", txtUserId.Text.ToString());
                cmd.Parameters.AddWithValue("@Password", txtPassword.Text.ToString());
                cmd.Connection = con;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    Session["UserId"] = txtUserId.Text.ToString();
                    txtInfo.Text = "Login Successful!";

                    reader.Close();
                    con.Close();
                //TODO Remove this!!!
                Response.Redirect("~/Default.aspx");
                //Response.Redirect("~/AP_CoursesByAP.aspx");
                //Response.Redirect("https://tra.esc11.net/reports/~reports.php");


                }
                else
                {
                    txtInfo.Text = "Invalid credentials";
                    Session["UserId"] = "";
                }

                reader.Close();

                con.Close();

            }
            catch (Exception ex)
            {

            }
        }
    }
}