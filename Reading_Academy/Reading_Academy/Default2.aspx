﻿<%@ Page Title="TRA Reading Academy" Language="C#" MasterPageFile="~/Site.Master AutoEventWireup="true" CodeBehind="Default2.aspx.cs" Inherits="Reading_Academy.Default2" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Ron</title>
</head>
<body>
    <form id="formDashBoard" runat="server">
        <div>
            <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

            <style>
                body {
                    font-family: Tahoma;
                    font-size: 14pt;
                }

                table {
                    width: 1000px;
                }

                td.header {
                    background-color: #999999;
                    font-weight: bold;
                }

                td.altrow {
                    background-color: #eeeeee;
                }

                td.redtext {
                    color: red;
                    font-style: italic;
                    font-weight: bold;
                    font-size: 11pt;
                }

                span.texttip {
                    font-size: 9pt;
                    background-color: #ffffcc;
                }

                select {
                    margin-top: 5px;
                }

                span.error {
                    color: red;
                }

                div.applicationResults {
                    display: none;
                }

                a {
                    color: #f06037;
                }

                h3 {
                    color: #0d6cb9;
                }
            </style>
        </div>
      
        
           <div>
            <table cellpadding="5" cellspacing="0" border="0">
                <tr>
                    <td colspan="2">

                        <img src="~images/TRA_logo.jpg" border="0" /></td>
                    <td style="text-align: right; vertical-align: top;"><a href="cl/leader_login.php">Sign In</a></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr />
                        <a href='/cl/'>
                            <img src='images/TRA_Home_Banner2.jpg' border='0' /></a><hr />

                    </td>
                </tr>

                <tr>
                    <td><a href="ap/~dashboard.php">
                        <img src="images/TRA_AP_button.png" border="0" /></a></td>
                    <td><a href="cl/leader_login.php">
                        <img src="images/TRA_CL_button.png" border="0" /></a></td>
                    <td><a href='https://register.tealearn.com/' target='_blank'>
                        <img src="images/TRA_CH_button.png" border="0" /></a></td>
                </tr>

                <tr>
                    <td colspan="3">
                        <h3>HB 3 Texas Reading Academies Development</h3>
                        As reading and writing are foundational to school success, TEA works to ensure that all Texas students have access to high-quality reading and writing instruction and highly-effective teachers. House Bill 3 addresses key reading practices. For the most updated information from TEA, please visit HB3 Reading Academies.
                        <br />
                        <br />
                        HB3 requires each teacher and principal in grades K-3 to attend reading academies by 2021-22. 
                        <br />
                        <br />
                        TEA plans to propose through rulemaking that enrollment in reading academies by the summer of 2022 adheres to this requirement. 
                        <br />
                        <br />
                        This would mean that there are three school years for completion:  2020-21, 2021-22, and 2022-23.
                    </td>
                </tr>
            </table>
        </div>
        </form>
     </body>
</html>
</asp:Content>

<%--<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>ASP.NET</h1>
        <p class="lead">ASP.NET is a free web framework for building great Web sites and Web applications using HTML, CSS, and JavaScript.</p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Getting started</h2>
            <p>
                ASP.NET Web Forms lets you build dynamic websites using a familiar drag-and-drop, event-driven model.
            A design surface and hundreds of controls and components let you rapidly build sophisticated, powerful UI-driven sites with data access.
            </p>
            <p>
                <a class="btn btn-default" href="https://go.microsoft.com/fwlink/?LinkId=301948">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Get more libraries</h2>
            <p>
                NuGet is a free Visual Studio extension that makes it easy to add, remove, and update libraries and tools in Visual Studio projects.
            </p>
            <p>
                <a class="btn btn-default" href="https://go.microsoft.com/fwlink/?LinkId=301949">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Web Hosting</h2>
            <p>
                You can easily find a web hosting company that offers the right mix of features and price for your applications.
            </p>
            <p>
                <a class="btn btn-default" href="https://go.microsoft.com/fwlink/?LinkId=301950">Learn more &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>--%>
