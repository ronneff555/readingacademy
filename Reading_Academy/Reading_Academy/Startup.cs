﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Reading_Academy.Startup))]
namespace Reading_Academy
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
