<?php
session_start();

$ap_filter='';

$username = "tra_readonly";
$password = "D@taW4$!";
$dbname = "readWH";

$conn = odbc_connect("TRA_DW", $username, $password);
if (odbc_error()) {
 echo odbc_errormsg($conn);
}
else{

//////////WRITE CSV DATA FILE HERE/////////////////////////
if(isset($_POST['btn_export'])){

$term_id=$_POST['term_id'];
$ap_id=$_POST['provider_id'];

///////////////////////////////////////////////////////////

#I create a file and ready to write 
$myfile = fopen('php://output', 'w') or die("Unable to open file!");

#create an empty variable to story the result for string concatination
$fileData = null;

if($ap_id=='999999'){
	if($term_id=='999999'){
		$fileData .="Module,Completed,In_Progress,Not_Started,Pre_Test_Avg,Post_Test_Avg,Growth\n";
	}else{
		$ap_filter= "AND c.enrollment_term_id ='".$term_id."' ";
		$fileData .="Term,Module,Completed,In_Progress,Not_Started,Pre_Test_Avg,Post_Test_Avg,Growth\n";
	}
}else{
	if($term_id=='999999'){
		$ap_filter=" AND c.account_id LIKE '%000".$ap_id."' ";
		$fileData .="Provider,Module,Completed,In_Progress,Not_Started,Pre_Test_Avg,Post_Test_Avg,Growth\n";
	}else{
		$ap_filter=" AND c.account_id LIKE '%0000".$ap_id."' AND c.enrollment_term_id ='".$term_id."' ";
		$fileData .="Provider,Term,Module,Completed,In_Progress,Not_Started,Pre_Test_Avg,Post_Test_Avg,Growth\n";
	}
}


$a=2;
while($a < 13){
if($ap_id=='999999'){}else{
	$sql = "SELECT name FROM authorized_providers WHERE canvas_sub_account_id=".$ap_id;
	$result = odbc_exec($conn, $sql);
	while($row = odbc_fetch_array($result)) {
	$fileData .= $row['name'].",";
	}
	odbc_free_result($result);
	}

if($term_id=='999999'){}else{
	$sql = "SELECT name FROM enrollment_term_dim WHERE id=".$term_id;
	$result = odbc_exec($conn, $sql);
	while($row = odbc_fetch_array($result)) {
	$fileData .= $row['name'].",";
	}
	odbc_free_result($result);
	}

$fileData .= "Module ".$a." Progression,";


$sql2 = "SELECT COUNT(DISTINCT s.id) as Completed_PostTest_D
	FROM            user_dim u 
	INNER JOIN  submission_dim s ON u.id = s.user_id
	INNER JOIN  assignment_dim a ON a.id = s.assignment_id  
	INNER JOIN  enrollment_dim e ON u.id = e.user_id AND a.course_id = e.course_id
	INNER JOIN course_dim c ON c.id=a.course_id
	WHERE (u.workflow_state = 'registered') AND (u.name NOT IN ('Test Student')) AND (e.workflow_state = 'active')
	AND a.title LIKE ('Module ".$a."%posttest%') 
	AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
	OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
	".$ap_filter."
	AND s.workflow_state = 'graded' AND s.grade IS NOT NULL";
	$result2 = odbc_exec($conn, $sql2);
	while($row2 = odbc_fetch_array($result2)) {
	$variableD=$row2['Completed_PostTest_D'];
	$fileData .= $row2['Completed_PostTest_D'].",";
	}
	odbc_free_result($result2);
	
$sql2 = "SELECT COUNT(DISTINCT s.id) as Completed_PreTest_C
	FROM            user_dim u 
	INNER JOIN  submission_dim s ON u.id = s.user_id
	INNER JOIN  assignment_dim a ON a.id = s.assignment_id  
	INNER JOIN  enrollment_dim e ON u.id = e.user_id AND a.course_id = e.course_id
	INNER JOIN course_dim c ON c.id=a.course_id
	WHERE (u.workflow_state = 'registered') AND (u.name NOT IN ('Test Student')) AND (e.workflow_state = 'active')
	AND a.title LIKE ('Module ".$a."%pretest%') 
	AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
	OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
	".$ap_filter."
	AND s.workflow_state = 'graded' AND s.grade IS NOT NULL";
	$result2 = odbc_exec($conn, $sql2);
	while($row2 = odbc_fetch_array($result2)) {
	$variableC=$row2['Completed_PreTest_C'];
	}
	odbc_free_result($result2);
	
	$inProgress = $variableC - $variableD;
	$fileData .= $inProgress.",";

$sql2 = "SELECT COUNT(DISTINCT s.id) as PreTest_NotStarted_H
	FROM            user_dim u 
	INNER JOIN  submission_dim s ON u.id = s.user_id
	INNER JOIN  assignment_dim a ON a.id = s.assignment_id  
	INNER JOIN  enrollment_dim e ON u.id = e.user_id AND a.course_id = e.course_id
	INNER JOIN course_dim c ON c.id=a.course_id
	WHERE (u.workflow_state = 'registered') AND (u.name NOT IN ('Test Student')) AND (e.workflow_state = 'active')
	AND a.title LIKE ('Module ".$a."%pretest%') 
	AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
	OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
	".$ap_filter."
	AND s.workflow_state = 'unsubmitted'";
	$result2 = odbc_exec($conn, $sql2);
	while($row2 = odbc_fetch_array($result2)) {
	$variableH=$row2['PreTest_NotStarted_H'];
	$fileData .= $row2['PreTest_NotStarted_H'].",";
	}
	odbc_free_result($result2);

$sql2 = "SELECT AVG(CAST(s.grade as decimal(10,2))) AVG_PostTest_G
	FROM            user_dim u 
	INNER JOIN  submission_dim s ON u.id = s.user_id
	INNER JOIN  assignment_dim a ON a.id = s.assignment_id  
	INNER JOIN  enrollment_dim e ON u.id = e.user_id AND a.course_id = e.course_id
	INNER JOIN course_dim c ON c.id=a.course_id
	WHERE (u.workflow_state = 'registered') AND (u.name NOT IN ('Test Student')) AND (e.workflow_state = 'active')
	AND a.title LIKE ('Module ".$a."%posttest%') 
	AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
	OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
	".$ap_filter."
	AND s.workflow_state = 'graded' AND s.grade IS NOT NULL";
	$result2 = odbc_exec($conn, $sql2);
	while($row2 = odbc_fetch_array($result2)) {
	$variableG=$row2['AVG_PostTest_G'];
	}
	odbc_free_result($result2);

$sql2 = "SELECT AVG(CAST(s.grade as decimal(10,2))) as AVG_PreTest_F
	FROM            user_dim u 
	INNER JOIN  submission_dim s ON u.id = s.user_id
	INNER JOIN  assignment_dim a ON a.id = s.assignment_id  
	INNER JOIN  enrollment_dim e ON u.id = e.user_id AND a.course_id = e.course_id
	INNER JOIN course_dim c ON c.id=a.course_id
	WHERE (u.workflow_state = 'registered') AND (u.name NOT IN ('Test Student')) AND (e.workflow_state = 'active')
	AND a.title LIKE ('Module ".$a."%pretest%') 
	AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
	OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
	".$ap_filter."
	AND s.workflow_state = 'graded' AND s.grade IS NOT NULL";
	$result2 = odbc_exec($conn, $sql2);
	while($row2 = odbc_fetch_array($result2)) {
	$variableF=$row2['AVG_PreTest_F'];
	}
	odbc_free_result($result2);

$avgGrowth = round($variableG,2) - round($variableF,2);
	$fileData .= round($variableF,2).",";
	$fileData .= round($variableG,2).",";
	$fileData .= $avgGrowth."\n";

$a++;
}

















    
    header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=module progression_export.csv");

#write the entire data into the file and close the file
fwrite($myfile, $fileData);
fclose($myfile);
}


}
?>