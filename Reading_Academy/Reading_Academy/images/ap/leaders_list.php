<?php
session_start();
if(isset($_SESSION['active_AP'])){$login="<a href='logout.php' class='logout'>[Logout]</a>";
?>


<html>
<head>
<title>TEA Reading Academy - Available Cohort Leaders</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

<style>
body{font-family:Tahoma;background-color:#efefef;color:#707476;}
a, a:visited, a:hover{color:#ef6036;text-decoration:none;}
div.pageMaster{width:1050px;background-color:#fff;border:1px solid #000;}
div.header{width:1000px;height:125px;border:0px solid #000;margin-top:20px;}
	div.logo{float:left;margin-left:15px;width:400px;border:0px solid green;}
	div.title{float:right;font-weight:bold;font-size:30pt;margin-right:30px;color:#0c6bba;text-transform:uppercase;margin-top:30px;}
	div.breadcrumb{font-size:10pt;margin-top:0px;padding-left:40px;color:#ef6036;width:960px;text-align:left;border:0px solid green;}
	div.breadcrumb a, div.breadcrumb a:visited, div.breadcrumb a:hover{color:#0c6bba;}
td{font-size:13px;}
.logout, .logout a, .logout a:hover, .logout a:visited{font-size:8pt;color:#ef6036!important;}
div.welcome{float:right;color:#707476;font-size:10pt;margin-right:10px;}

pre { font-size: 13px; font-family: "Consolas", "Menlo", "Monaco", "Lucida Console", "Liberation Mono", "DejaVu Sans Mono", "Bitstream Vera Sans Mono", "Courier New", monospace, serif; background-color: #FFFFCC; padding: 5px 5px 5px 5px; }
pre .comment { color: #008000; }
pre .builtin { color: #FF0000; }
table.clList{font-size:10pt;width:1000px;}
table.clList td, table.clList th{padding:3px;}
th{font-weight:bold;}
tr.clRow{background-color:#fffbc9;}
</style>

</head>
<body>

<?php

$servername = "localhost";
$username = "root";
$password = "R3@d1ngAc@d";
$dbname = "readacad";

$conn = odbc_connect("ReadingAcademy", $username, $password);
if (odbc_error()) {
 echo odbc_errormsg($conn);
}
else{

$regionLabel = 'No Region Filter Selected';
$selected = '0';

echo "<center>";
echo "<div class='pageMaster'>";
echo "<div class='welcome'>".$_SESSION['firstName']." ".$_SESSION['lastName']."<br />".$login."</div>";
echo "<div class='header'>";
echo "<div class='logo'><a href='~dashboard.php'><img src='../images/TRA_logo.jpg' /></a></div>";
echo "<div class='title'>Available<br />Cohort Leaders</div>";
echo "</div>";
?>
<hr />
<p>Use the drop down below to filter candidates by those who selected a region of interest during their application process.</p>
<?php
if(isset($_POST['filterReg'])){
$regionLabel = "Region ".$_POST['region']." Selected";
$selected = $_POST['region'];
}

echo "<form method='POST' action='leaders_list.php'>";
echo "<select name='region'>";
echo "<option value='ALL' value='0'>All Candidates</option>";
$a=1;
while($a < 21){
if($selected == $a){
echo "<option value='".$a."' selected>Region ".$a."</option>";
}else{
echo "<option value='".$a."'>Region ".$a."</option>";
}

$a++;
}
echo "</select>";
echo "<input type='submit' name='filterReg' value='Go' />";
echo "</form>";

echo "<h4>".$regionLabel."</h4>";

echo "<table border='1' class='clList'>";
echo "<tr><th>TRA ID</th><th>Last Name</th><th>First Name</th><th>Email</th><th>Alternate Email</th><th>ELAR Certified</th><th>Bilit Certified</th><th>Region Filter</th></tr>";
if($selected == 0){
$sql2 = "SELECT DISTINCT l.leader_ID, firstName, lastName, email, altEmail, passedScreener, bilit, l.provider_ID FROM tbl_leader l INNER JOIN tbl_leader_interest i ON l.leader_ID=i.leader_ID WHERE l.provider_ID=0 AND i.provider_ID>0 AND (passedScreener=1 OR bilit=1) ORDER BY lastName";
}else{
$sql2 = "SELECT l.leader_ID, firstName, lastName, email, altEmail, passedScreener, bilit, i.provider_ID FROM tbl_leader l INNER JOIN tbl_leader_interest i ON l.leader_ID=i.leader_ID WHERE l.provider_ID = 0 AND i.provider_ID=".$selected." AND (passedScreener=1 OR bilit=1) ORDER BY lastName";
}
$result2 = odbc_exec($conn, $sql2);
while($row2 = odbc_fetch_array($result2)) {
$leader_ID = $row2['leader_ID'];
$firstName = $row2['firstName'];
$lastName = $row2['lastName'];
$email = $row2['email'];
$altEmail = $row2['altEmail'];
if($row2['passedScreener']==1){$passedScreener = 'Yes';}else{$passedScreener='No';}
if($row2['bilit']==1){$bilit='Yes';$class='clRow';}else{$bilit='-';$class='';}

echo "<tr class='".$class."'><td>".$leader_ID."</td><td>".$lastName."</td><td>".$firstName."</td><td><a href='mailto:".$email."'>".$email."</a></td><td><a href='mailto:".$altEmail."'>".$altEmail."</a></td><td>".$passedScreener."</td><td>".$bilit."</td><td>".$selected."</td></tr>";

}
echo "</table><br />&nbsp;";


?>







<?php
odbc_close($conn);
}
?>

</body>
</html>

<?php
///////////////////IF SESSION IS NOT ACTIVE RUN BELOW////////
}else{

?>

<html>
<head>
<meta http-equiv="refresh" content="0;url=https://tra.esc11.net/ap/~dashboard.php" />
<title>TRA Authorized Provider Logout</title>
</head>
<body>
You are currently being logged out of the system.
</body>
</html>

<?php
}
?>