<?php
session_start();
?>

<style>
.error_msg{display:none;}
</style>

<?php

$breadcrumb = "Login";

//CONNECTION INFORMATION DETAILS
$servername = "localhost";
$username = "root";
$password = "R3@d1ngAc@d";
$dbname = "readacad";

if(isset($_POST['check_access'])){
$email = $_POST['email'];
$accessCode = $_POST['accessCode'];

$conn_sess = odbc_connect("ReadingAcademy", $username, $password);
$sql_ss = "SELECT accessID, active, firstName, lastName, accessLevel, providerCanvas FROM tbl_access WHERE email = '".$email."' AND accessCode = '".$accessCode."' AND active='1' AND providerCanvas IS NOT NULL";
$result_ss = odbc_exec($conn_sess, $sql_ss);
while($row_ss = odbc_fetch_array($result_ss)) {
$_SESSION['active_AP'] = $active = $row_ss['active'];
$_SESSION['accessCode'] = $accessCode;
$_SESSION['email'] = $email;
$_SESSION['providerCanvas'] = $providerCanvas = $row_ss['providerCanvas'];
$firstName = $_SESSION['firstName'] = $row_ss['firstName'];
$lastName = $_SESSION['lastName'] = $row_ss['lastName'];
$accessLevel = $_SESSION['accessLevel'] = $row_ss['accessLevel'];

$sql_access = "INSERT INTO tbl_access_log (accessID) VALUES (".$row_ss['accessID'].")";
odbc_exec($conn_sess, $sql_access);

}
odbc_close($conn_sess);
if(isset($active)){}else{
echo "<style>";
echo ".error_msg{display:block;color:red;margin-top:3px;font-weight:bold;font-size:10pt;}";
echo "</style>";
}

}

if(isset($_SESSION['active_AP'])){
$breadcrumb = "Home Page";
$login="<a href='logout.php' class='logout'>[Logout]</a>";}else{$_SESSION['firstName']=$_SESSION['lastName']=$login='';}


if(isset($_SESSION['active_AP'])){
echo "<style>";
echo "#login_form{display:none;}";
echo "#page_content{display:block;}";
echo "</style>";
}else{
echo "<style>";
echo "#login_form{display:block;}";
echo "#page_content{display:none;}";
echo "</style>";
}


//CREATE THE CONNECTION TO THE DATABASE
$conn = odbc_connect("ReadingAcademy", $username, $password);
if (odbc_error()) {
// echo odbc_errormsg($conn);
}
else{
?>

<html>
<head>

<style>
body{font-family:Tahoma;background-color:#efefef;}
a, a:visited, a:hover{color:#ef6036;text-decoration:none;}
div.pageMaster{width:1000px;background-color:#fff;border:1px solid #000;}
div.header{width:1000px;height:125px;border:0px solid #000;margin-top:20px;}
	div.logo{float:left;margin-left:15px;width:400px;border:0px solid green;}
	div.welcome{float:right;color:#707476;font-size:10pt;margin-right:10px;}
	div.title{float:right;font-weight:bold;font-size:50pt;margin-right:30px;color:#0c6bba;text-transform:uppercase;margin-top:30px;}
	div.breadcrumb{font-size:10pt;margin-top:0px;padding-left:40px;color:#ef6036;width:960px;text-align:left;border:0px solid green;}
	div.breadcrumb a, div.breadcrumb a:visited, div.breadcrumb a:hover{color:#0c6bba;}
#report_table{border:0px solid #707476;padding:5px;width:1000px;}
#report_table td{text-align:center;font-size:12pt;width:25%;padding-bottom:25px;vertical-align:top;}
#report_table td img{border:1px solid #707476;}
span.description{font-size:10pt;}
.logout{font-size:8pt;}

#login_form{margin-top:30px;border:1px solid #0c6bba;width:400px;padding:20px;margin-bottom:40px;}
</style>
<title>TEA Reading Academy Reporting Site</title>

</head>
<body>
<center>
<div class='pageMaster'>
<div class='welcome'><?php echo $_SESSION['firstName']." ".$_SESSION['lastName']."<br />".$login; ?></div>
<div class='header'>
	<div class='logo'><img src='../../images/TRA_logo.jpg' /></div>
	<div class='title'>AP Dashboard</div>
</div>
	
<div class='breadcrumb'><?php echo $breadcrumb; ?></div>
<hr style='width:960px;' />
The Texas Reading Academies authorized provider dashboard is a secure area for authorized providers to pull progress <br />and performance data. <br /><br />
<div id='login_form'>
	<form method='post' action='~dashboard.php'>
	Email: <input type='text' name='email' class='input_text' /><br />
	Password: <input type='password' name='accessCode' class='input_text' /><br />
	<input type='submit' name='check_access' class='input_submit' value='Login' /><br />
	<?php echo "<span class='error_msg'>Login Failed</span>"; ?>
	</form>
</div>

<div id='page_content'>	
<table cellspacing='4' id='report_table'>
<tr><td style='text-align:left;padding-left:16px;'><h3 style='margin-bottom:-15px;'>AP Toolbox</h3></td></tr>
<tr><td style='text-align:left;padding-left:16px;'><a href='leaders_list.php'>Available Cohort Leaders List</a></td></tr></table>


<table cellspacing='4' id='report_table'>
<tr><td colspan='4' style='text-align:left;padding-left:16px;'><h3 style='margin-bottom:-15px;'>Progress and Performance Reports</h3></td></tr>
<tr>
<td><a href='https://tra.esc11.net/ap/overview.php'><img src='images/dashboard.jpg' alt='TRA Dashboard Screenshot' /><br />TRA Dashboard</a><br /><span class='description'>The TRA live dashboard reports the number of active learners, active courses, and learner progress at the state and Authorized Provider level. </span></td>
<td><a href='https://tra.esc11.net/ap/learner_progress_AP.php'><img src='images/learner_progress.jpg' alt='TRA Learner Progress Screenshot' /><br />Learner Progress by Course</a><br /><span class='description'>This report will provide individual learner module progress by course.</span></td>
<td><a href='https://tra.esc11.net/ap/course_completion.php'><img src='images/course_completion.jpg' alt='TRA Course Completion Screenshot' /><br />Course Level Completion</a><br /><span class='description'>Course completion reports the progress of learners at the course level and provides an average score on pretests and posttests.</span></td>
<td><a href='https://tra.esc11.net/ap/state_module_progression.php'><img src='images/state_module_progress.jpg' alt='TRA State Module Progress Screenshot' /><br />State Module Progression</a><br /><span class='description'>This reports the module progress of all learners and provides the change in percentage points from the pretests to the posttests. Filtering can be done by Authorized Provider and/or Term.</span></td>
</tr>
<tr>
<td><a href='https://tra.esc11.net/ap/module_progression.php'><img src='images/module_progress.jpg' alt='TRA Module Progress Screenshot' /><br />Module Progression</a><br /><span class='description'>Module progression reports the module progress of learners at the course level and provides the change in percentage points from the pretests to the posttests. Reports are organized by the Authorized Provider.</span></td>
<td><a href='https://tra.esc11.net/ap/artifacts_module.php'><img src='images/artifact_search.jpg' alt='TRA Assignment Search Screenshot' /><br />Assignment Search</a><br /><span class='description'>This report allows you to see submissions and averages on all assignments. Terms to search for include: artifact, check for understanding, quick check, pretest, posttest, and module.</span></td>
<td><a href='https://tra.esc11.net/ap/ap.php'><img src='images/courses_AP.jpg' alt='TRA Course List Screenshot' /><br />Course List</a><br /><span class='description'>This report provides a list of all courses served through your Authorized Provider.</span></td>
<td><a href='https://tra.esc11.net/ap/csv_pull.php'><img src='images/csv_export.jpg' alt='TRA CSV Export Screenshot' /><br />CSV Data Export</a><br /><span class='description'>This allows you to pull raw assignment data at the user level by filtering term, course, and assignment. A CSV file will download. Filtering is recommended to ensure the CSV will download.</span></td>
</tr>


</table>

<br/><br />

</div>


</div>

</body>
</html>
<?php
}
odbc_close($conn);
?>