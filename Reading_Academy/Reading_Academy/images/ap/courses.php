<?php
session_start();
if(isset($_SESSION['active_AP'])){$login="<a href='logout.php' class='logout'>[Logout]</a>";


$username2 = "tra_readonly";
$password2 = "D@taW4$!";
$dbname2 = "readWH";

$conn2 = odbc_connect("TRA_DW", $username2, $password2);

//CONNECTION INFORMATION DETAILS
$servername = "localhost";
$username = "root";
$password = "R3@d1ngAc@d";
$dbname = "readacad";

//CREATE THE CONNECTION TO THE DATABASE
$conn = odbc_connect("ReadingAcademy", $username, $password);
if (odbc_error()) {
// echo odbc_errormsg($conn);
}
else{

$sql = "SELECT COUNT(id) as courses
 FROM course_dim c
  WHERE workflow_state IN('available','completed') 
  AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
  OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {$totalCourses = number_format($row['courses']);}odbc_free_result($result);

?>


<html>
<head>
<style>
body{font-family:Tahoma;background-color:#efefef;}
a, a:visited, a:hover{color:white;text-decoration:none;}
div.pageMaster{width:1000px;background-color:#fff;border:1px solid #000;height:1290px;}
div.header{width:1000px;height:125px;border:0px solid #000;margin-top:20px;}
	div.logo{float:left;margin-left:15px;width:400px;border:0px solid green;}
	div.title{float:right;font-weight:bold;font-size:34pt;margin-right:30px;color:#0c6bba;text-transform:uppercase;margin-top:5px;text-align:left;width:500px;}
	div.breadcrumb{font-size:10pt;margin-top:0px;padding-left:40px;color:#ef6036;width:960px;text-align:left;border:0px solid green;}
	div.breadcrumb a, div.breadcrumb a:visited, div.breadcrumb a:hover{color:#0c6bba;}
div.tiles{width:1000px;border:0px solid #000;}
	div.tile1, div.tile2, div.tile3, div.tile4, div.tile5, div.tile6, div.tile7, div.dataOf{margin-top:15px;margin-left:40px;width:280px;height:340px;float:left;border:1px solid black;color:white;text-align:center;}
	div.tile1{background-color:#ef6036;}
	div.tile2{background-color:#0c6bba;height:220px;}
	div.tile3{color:#0c6bba;width:560px;font-size:1pt;height:210px;padding-top:10px;background-color:#efefef;}
		div.dataOf{color:#ef6036;width:430px;font-size:10pt;height:15px;padding-top:0px;background-color:transparent;margin-top:10px;text-align:left;border:0px;}
	div.tile4{color:#0c6bba;width:430px;font-size:1pt;height:840px;padding-top:10px;background-color:#efefef;}
	div.tile5{color:#0c6bba;width:430px;font-size:1pt;height:185px;padding-top:10px;background-color:#efefef;margin-left:20px;}
	div.tile6{color:#0c6bba;width:430px;font-size:1pt;height:120px;padding-top:10px;background-color:#efefef;margin-left:20px;}
	div.tile7{display:none;color:#0c6bba;width:430px;font-size:1pt;height:105px;padding-top:10px;background-color:#efefef;margin-left:20px;}
		div.tileTitle{font-size:24pt;margin-top:45px;}
		div.tileTotal{font-size:48pt;margin-bottom:50px;}
		div.breakdownLeft, div.breakdownLeftAlt{text-align:left;float:left;margin-left:20px;;}
		div.breakdownRight, div.breakdownRightAlt{text-align:right;margin-right:0px;padding-right:20px;}
		div.breakdownLeftAlt, div.breakdownRightAlt{color:#ef6036;}
			div.breakdownCat{font-size:12pt;border-bottom:0px solid #000;}
h3{font-size:18pt;margin-top:-5px;margin-bottom:5px;color:#707476;}
div.tile4 a{color:#0c6bba;}
.logout, .logout a, .logout a:hover, .logout a:visited{font-size:8pt;color:#ef6036!important;}
div.welcome{float:right;color:#707476;font-size:10pt;margin-right:10px;}
</style>

</head>
<body>
<center>
<div class='pageMaster'>
<?php
echo "<div class='welcome'>".$_SESSION['firstName']." ".$_SESSION['lastName']."<br />".$login."</div>";
?>
<div class='header'>
	<div class='logo'><a href='~dashboard.php'><img src='../images/TRA_logo.jpg' /></a></div>
	<div class='title'><span style='font-size:40pt;color:#ef6036;'>Active Courses</span><br />Data Sheet</div>
</div>
<div class='breadcrumb'><a href='overview.php'>Overview</a> > Courses</div>
<div class='tiles'>
	<div class='tile2'>
		<div class='tileTitle'>Active Courses</div>
		<div class='tileTotal'><?php echo $totalCourses; ?></div>
	</div>
	<div class='tile3'>
	<h3># of Courses by Course Type</h3>
<?php
$sql = "SELECT COUNT(c.id) as studentType
  FROM course_dim c
  WHERE c.workflow_state IN('available','completed')
    AND c.[name] LIKE ('% AOB%')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>Admin ELAR Blended</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['studentType'])."</div>";
echo "</div>";
echo "<hr />";
}
odbc_free_result($result);

$sql = "SELECT COUNT(c.id) as studentType
  FROM course_dim c
  WHERE c.workflow_state IN('available','completed')
    AND c.[name] LIKE ('% AOC%')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>Admin ELAR Comprehensive</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['studentType'])."</div>";
echo "</div>";
echo "<hr />";
}
odbc_free_result($result);

$sql = "SELECT COUNT(c.id) as studentType
  FROM course_dim c
  WHERE c.workflow_state IN('available','completed')
    AND c.[name] LIKE ('% BOB%')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>Bilit Blended</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['studentType'])."</div>";
echo "</div>";
echo "<hr />";
}
odbc_free_result($result);

$sql = "SELECT COUNT(c.id) as studentType
  FROM course_dim c
  WHERE c.workflow_state IN('available','completed')
    AND c.[name] LIKE ('% BOC%')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>Bilit Comprehensive</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['studentType'])."</div>";
echo "</div>";
echo "<hr />";
}
odbc_free_result($result);

$sql = "SELECT COUNT(c.id) as studentType
  FROM course_dim c
  WHERE c.workflow_state IN('available','completed')
    AND c.[name] LIKE ('% EOB%')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>ELAR Blended</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['studentType'])."</div>";
echo "</div>";
echo "<hr />";
}
odbc_free_result($result);

$sql = "SELECT COUNT(c.id) as studentType
  FROM course_dim c
  WHERE c.workflow_state IN('available','completed')
    AND c.[name] LIKE ('% EOC%')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>ELAR Comprehensive</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['studentType'])."</div>";
echo "</div>";
echo "<hr />";
}
odbc_free_result($result);

$sql = "SELECT COUNT(c.id) as studentType
  FROM course_dim c
  WHERE c.workflow_state IN('available','completed')
    AND c.[name] LIKE ('% EBB%')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>ELAR/Bilit Blended</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['studentType'])."</div>";
echo "</div>";
echo "<hr />";
}
odbc_free_result($result);

$sql = "SELECT COUNT(c.id) as studentType
  FROM course_dim c
  WHERE c.workflow_state IN('available','completed')
    AND c.[name] LIKE ('% EBC%')";
	$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>ELAR/Bilit Comprehensive</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['studentType'])."</div>";
echo "</div>";
echo "<hr />";
}
odbc_free_result($result);

?>

</div>



<div class='tile4'>
	<h3># of Courses by Region</h3><hr />
<?php
//$provider_ID=88;
//$reg=1;
//while($provider_ID < 108){
 $sql = "SELECT COUNT(c.id) as learners, a.name as provider, a.canvas_id as provider_ID
  FROM course_dim c 
  INNER JOIN account_dim a ON c.account_id=a.id
  WHERE c.workflow_state IN('available','completed')
    AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
  OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
 AND a.workflow_state = 'active' AND a.depth='2' AND a.name NOT IN ('Deleted Courses')
  GROUP BY a.name, a.canvas_id";
  $result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
$old_text = array(" Texas Reading Academies K-3", "The University of Texas at Austin - The Meadows Center for Preventing Educational Risk");
$new_text = array("", "UT - The Meadows");
$provider=str_replace($old_text, $new_text, $row['provider']);
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'><a href='courses_AP.php?provider_ID=".$row['provider_ID']."'>".$provider."</a></div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['learners'])."</div>";
echo "</div>";
//if($provider_ID < 107){echo "<hr />";}
echo "<hr />";
}
//$provider_ID++;
//$reg++;
//}
odbc_free_result($result);

?>

</div>


<div class='tile6'>
	<h3># of Courses by Pathway</h3><hr />	
<?php
	$a=1;
	while($a < 5){

	switch ($a) {
    case "1":
        $pathway = 'Admin';
		$comp= 'AOC';
		$blend = 'AOB';
        break;
    case "2":
        $pathway = 'ELAR';
		$comp= 'EOC';
		$blend = 'EOB';
        break;
    case "3":
        $pathway = 'Bilit';
		$comp= 'BOC';
		$blend = 'BOB';     
		break;
	case "4":
        $pathway = 'Dual Path';
		$comp= 'EBC';
		$blend = 'EBB';
        break;
}

$sql = "SELECT COUNT(c.id) as student
  FROM course_dim c 
  WHERE c.workflow_state IN('available','completed')
    AND (c.[name] LIKE ('% ".$comp."%') OR c.[name] LIKE ('% ".$blend."%'))";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
	echo "<div class='breakdownLeft'>";
	echo "<div class='breakdownCat'>".$pathway."</div>";
	echo "</div>";
	echo "<div class='breakdownRight'>";
	echo "<div class='breakdownCat'>".number_format($row['student'])."</div>";
	echo "</div>";
	if($a<4){echo "<hr />";}
	}
	odbc_free_result($result);
	$a++;
	}
	?>
	</div>

<div class='tile5'>
		<h3># of Courses by Term</h3><hr />
	<?php
	$sql = "SELECT id, name as term_name  FROM enrollment_term_dim WHERE name LIKE ('%Reading Acad%') AND date_start > '2020/06/15' ORDER BY date_start";
		$result = odbc_exec($conn2, $sql);
	while($row = odbc_fetch_array($result)) {
		$term_id = $row['id'];
	

	$sql2="SELECT COUNT(c.id) as Enrolled_Term
  FROM course_dim c 
  INNER JOIN enrollment_term_dim t ON t.id=c.enrollment_term_id
  WHERE c.workflow_state IN('available','completed')
    AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
  OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
  AND c.enrollment_term_id = '".$term_id."'";
  		$result2 = odbc_exec($conn2, $sql2);
	while($row2 = odbc_fetch_array($result2)) {
	if($row2['Enrolled_Term'] > 0){
	echo "<div class='breakdownLeft'>";
	$term_name = str_replace('Reading Academies', '', $row['term_name']);
	echo "<div class='breakdownCat'>".$term_name."</div>";
	echo "</div>";
	echo "<div class='breakdownRight'>";
	echo "<div class='breakdownCat'>".number_format($row2['Enrolled_Term'])."</div>";
	echo "</div>";
	echo "<hr />";
	}
	}
	}
	odbc_free_result($result);
	odbc_free_result($result2);
	?>
</div>

<div class='tile7'>
	<h3>Learner Progress</h3><hr />
	
	
		TBD
</div>

</div>
</center>
</body>
</html>
<?php
odbc_close($conn);
}

///////////////////IF SESSION IS NOT ACTIVE RUN BELOW////////
}else{

?>

<html>
<head>
<meta http-equiv="refresh" content="0;url=https://tra.esc11.net/ap/~dashboard.php" />
<title>TRA Authorized Provider Logout</title>
</head>
<body>
You are currently being logged out of the system.
</body>
</html>

<?php
}
?>