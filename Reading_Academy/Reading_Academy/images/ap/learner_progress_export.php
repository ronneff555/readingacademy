<?php
session_start();

$username = "tra_readonly";
$password = "D@taW4$!";
$dbname = "readWH";

$conn = odbc_connect("TRA_DW", $username, $password);
if (odbc_error()) {
 echo odbc_errormsg($conn);
}
else{

//////////WRITE CSV DATA FILE HERE/////////////////////////
if(isset($_POST['btn_export'])){

$course_selected=$_POST['course_selected'];
$termName=$_POST['termName'];
$courseName=$_POST['courseName'];

///////////////////////////////////////////////////////////

#I create a file and ready to write 
$myfile = fopen('php://output', 'w') or die("Unable to open file!");

#create an empty variable to story the result for string concatination
$fileData = null;
$fileData .="Course_ID,Course_Name,Term,Learner,District,Campus,Mod1,Mod2,Mod3,Mod4,Mod5,Mod6,Mod7,Mod8,Mod9,Mod10,Mod11,Mod12\n";



$sql = "
SELECT c.id as course_id
	  ,c.canvas_id as course_canvas_id
      ,c.[name] as courseName
	  ,t.name as termName
	  ,u.canvas_id as user_canvas_id
	  ,u.id as user_id
	  ,u.[name] as learnerName
	  ,s.[name] as district
	  ,a.[name] as campus
  FROM [readWH].[dbo].[course_dim] c 
  INNER JOIN enrollment_dim e ON c.id=e.course_id
  INNER JOIN enrollment_term_dim t ON c.enrollment_term_id=t.id
  INNER JOIN user_dim u ON u.id=e.user_id
  INNER JOIN user_demographics d ON d.user_id=u.canvas_id
  LEFT OUTER JOIN districts s ON s.id=d.district_id
  LEFT OUTER JOIN campuses a ON a.id=d.campus_id
  WHERE c.canvas_id='".$course_selected."' AND u.name NOT IN('Test Student') AND e.type = 'StudentEnrollment'
  AND e.workflow_state NOT IN('deleted')
  ORDER BY campus, learnerName";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
$u_id=$row['user_id'];
//while(odbc_fetch_row($result)){

$fileData .= $course_selected.",";
$fileData .= $courseName.",";
$fileData .= $termName.",";
$fileData .= $row['learnerName'].",";
$fileData .= $row['district'].",";
$fileData .= $row['campus'].",";


$sql2 = "
SELECT COUNT (s.id) as Count
  FROM [readWH].[dbo].[assignment_dim] a
  INNER JOIN course_dim c ON a.course_id=c.id
  INNER JOIN submission_dim s ON s.assignment_id=a.id
  INNER JOIN user_dim u ON u.id=s.user_id
  WHERE a.title LIKE ('%Module 2%pretest%') AND c.canvas_id = '".$course_selected."' AND s.user_id = '".$u_id."' AND s.workflow_state IN('submitted','pending_review','graded')";
  $result2 = odbc_exec($conn, $sql2);
while($row2 = odbc_fetch_array($result2)) {
$mod2=$row2['Count'];
if($mod2 > 0){$mod1='Yes';}else{$mod1='No';}
$fileData .= $mod1.",";
}

$a=2;
while($a < 13){
$sql3 = 
"SELECT COUNT (s.id) as Count
  FROM [readWH].[dbo].[assignment_dim] a
  INNER JOIN course_dim c ON a.course_id=c.id
  INNER JOIN submission_dim s ON s.assignment_id=a.id
  INNER JOIN user_dim u ON u.id=s.user_id
 WHERE c.canvas_id = '".$course_selected."' AND a.title LIKE ('%posttest%') AND (a.title LIKE ('% ".$a.":%') OR a.title LIKE ('% ".$a."B:%') OR a.title LIKE ('% ".$a."A:%'))
  AND s.user_id = '".$u_id."'
  AND s.workflow_state IN('submitted','pending_review','graded')";
    $result3 = odbc_exec($conn, $sql3);
while($row3 = odbc_fetch_array($result3)) {
  $mod=$row3['Count'];
  if($a < 12){
if($mod > 0){$fileData .= "Yes,";}else{$fileData .= "No,";}
}else{
if($mod > 0){$fileData .= "Yes\n";}else{$fileData .= "No\n";}
}

}
$a++;
}
 
 }  /////////////THIS ENDS THE LOOP FOR THE LEARNER///////////////

    
    header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=learner_progress_export.csv");

#write the entire data into the file and close the file
fwrite($myfile, $fileData);
//fputcsv($myfile, $fileData);
fclose($myfile);
}

}
?>