<?php
session_start();
if(isset($_SESSION['active_AP'])){$login="<a href='logout.php' class='logout'>[Logout]</a>";


$username2 = "tra_readonly";
$password2 = "D@taW4$!";
$dbname2 = "readWH";

$conn2 = odbc_connect("TRA_DW", $username2, $password2);

//CONNECTION INFORMATION DETAILS
$servername = "localhost";
$username = "root";
$password = "R3@d1ngAc@d";
$dbname = "readacad";

//CREATE THE CONNECTION TO THE DATABASE
$conn = odbc_connect("ReadingAcademy", $username, $password);
if (odbc_error()) {
// echo odbc_errormsg($conn);
}
else{

$sql = "SELECT COUNT(e.id) as students
  FROM enrollment_dim e 
  INNER JOIN course_dim c ON c.id=e.course_id
  WHERE e.type = 'StudentEnrollment' AND e.workflow_state IN('active') AND c.workflow_state IN('available','completed')
    AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
  OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
  AND c.account_id LIKE ('%000".$_GET['provider_ID']."')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {$totalStudents = number_format($row['students']);$total_studentCount=$row['students'];}odbc_free_result($result);

$sql = "SELECT p.name FROM account_dim a INNER JOIN authorized_providers p ON a.canvas_id=p.canvas_sub_account_id WHERE a.canvas_id = ".$_GET['provider_ID']."";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)){
$old_text = array(" Texas Reading Academies K-3", "The University of Texas at Austin - The Meadows Center for Preventing Educational Risk");
$new_text = array("", "UT - The Meadows");
$providerName = str_replace($old_text, $new_text, $row['name']);

}

?>


<html>
<head>
<style>
body{font-family:Tahoma;background-color:#efefef;}
a, a:visited, a:hover{color:white;text-decoration:none;}
div.pageMaster{width:1000px;background-color:#fff;border:1px solid #000;height:950px;}
div.header{width:1000px;height:125px;border:0px solid #000;margin-top:20px;}
	div.logo{float:left;margin-left:15px;width:400px;border:0px solid green;}
	div.title{float:right;font-weight:bold;font-size:30pt;margin-right:30px;color:#0c6bba;text-transform:uppercase;margin-top:5px;text-align:left;width:500px;}
	div.breadcrumb{font-size:10pt;margin-top:0px;padding-left:40px;color:#ef6036;width:960px;text-align:left;border:0px solid green;}
	div.breadcrumb a, div.breadcrumb a:visited, div.breadcrumb a:hover{color:#0c6bba;}
div.tiles{width:1000px;border:0px solid #000;}
	div.tile1, div.tile2, div.tile3, div.tile4, div.tile5, div.tile6, div.tile7, div.dataOf{margin-top:15px;margin-left:40px;width:280px;height:340px;float:left;border:1px solid black;color:white;text-align:center;}
	div.tile1{background-color:#ef6036;height:220px;}
	div.tile2{background-color:#0c6bba;}
	div.tile3{color:#0c6bba;width:560px;font-size:1pt;height:210px;padding-top:10px;background-color:#efefef;}
		div.dataOf{color:#ef6036;width:430px;font-size:10pt;height:15px;padding-top:0px;background-color:transparent;margin-top:10px;text-align:left;border:0px;}
	div.tile4{display:none;color:#0c6bba;width:430px;font-size:1pt;height:490px;padding-top:10px;background-color:#efefef;}
	div.tile5{color:#0c6bba;width:430px;font-size:1pt;height:165px;padding-top:10px;background-color:#efefef;margin-left:20px;}
	div.tile6{color:#0c6bba;width:430px;font-size:1pt;height:120px;padding-top:10px;background-color:#efefef;margin-left:40px;}
	div.tile7{color:#0c6bba;width:430px;font-size:1pt;height:105px;padding-top:10px;background-color:#efefef;margin-left:40px;margin-top:-30px;}
		div.tileTitle{font-size:24pt;margin-top:45px;}
		div.tileTotal{font-size:48pt;margin-bottom:50px;}
		div.breakdownLeft, div.breakdownLeftAlt{text-align:left;float:left;margin-left:20px;;}
		div.breakdownRight, div.breakdownRightAlt{text-align:right;margin-right:0px;padding-right:20px;}
		div.breakdownLeftAlt, div.breakdownRightAlt{color:#ef6036;}
			div.breakdownCat{font-size:12pt;border-bottom:0px solid #000;}
h3{font-size:18pt;margin-top:-5px;margin-bottom:5px;color:#707476;}
div.tile4 a{color:#0c6bba;}
.logout, .logout a, .logout a:hover, .logout a:visited{font-size:8pt;color:#ef6036!important;}
div.welcome{float:right;color:#707476;font-size:10pt;margin-right:10px;}
</style>

</head>
<body>
<center>
<div class='pageMaster'>
<?php
echo "<div class='welcome'>".$_SESSION['firstName']." ".$_SESSION['lastName']."<br />".$login."</div>";
?>
<div class='header'>
	<div class='logo'><a href='~dashboard.php'><img src='../images/TRA_logo.jpg' /></a></div>
	<div class='title'><span style='color:#ef6036;'><?php echo $providerName; ?></span><br />Data Sheet</div>
</div>
<div class='breadcrumb'><a href='overview.php'>Overview</a> > <a href='learners.php'>Learners</a> > Learners by <?php echo $providerName; ?></div>
<div class='tiles'>
	<div class='tile1'>
		<div class='tileTitle'>Active Learners</div>
		<div class='tileTotal'><?php echo $totalStudents; ?></div>
	</div>
	<div class='tile3'>
	<h3># of Learners by Course Type</h3><hr />
<?php

$sql = "SELECT COUNT(e.id) as studentType
  FROM enrollment_dim e 
  INNER JOIN course_dim c ON c.id=e.course_id
  WHERE e.type = 'StudentEnrollment' AND e.workflow_state IN('active') AND c.workflow_state IN('available','completed')
    AND c.[name] LIKE ('% AOB%')
	 AND c.account_id LIKE ('%000".$_GET['provider_ID']."')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>Admin ELAR Blended</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['studentType'])."</div>";
echo "</div>";
echo "<hr />";
}
odbc_free_result($result);

$sql = "SELECT COUNT(e.id) as studentType
  FROM enrollment_dim e 
  INNER JOIN course_dim c ON c.id=e.course_id
  WHERE e.type = 'StudentEnrollment' AND e.workflow_state IN('active') AND c.workflow_state IN('available','completed')
    AND c.[name] LIKE ('% AOC%')
	 AND c.account_id LIKE ('%000".$_GET['provider_ID']."')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>Admin ELAR Comprehensive</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['studentType'])."</div>";
echo "</div>";
echo "<hr />";
}
odbc_free_result($result);

$sql = "SELECT COUNT(e.id) as studentType
  FROM enrollment_dim e 
  INNER JOIN course_dim c ON c.id=e.course_id
  WHERE e.type = 'StudentEnrollment' AND e.workflow_state IN('active') AND c.workflow_state IN('available','completed')
    AND c.[name] LIKE ('% BOB%')
	 AND c.account_id LIKE ('%000".$_GET['provider_ID']."')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>Bilit Blended</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['studentType'])."</div>";
echo "</div>";
echo "<hr />";
}
odbc_free_result($result);

$sql = "SELECT COUNT(e.id) as studentType
  FROM enrollment_dim e 
  INNER JOIN course_dim c ON c.id=e.course_id
  WHERE e.type = 'StudentEnrollment' AND e.workflow_state IN('active') AND c.workflow_state IN('available','completed')
    AND c.[name] LIKE ('% BOC%')
	 AND c.account_id LIKE ('%000".$_GET['provider_ID']."')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>Bilit Comprehensive</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['studentType'])."</div>";
echo "</div>";
echo "<hr />";
}
odbc_free_result($result);

$sql = "SELECT COUNT(e.id) as studentType
  FROM enrollment_dim e 
  INNER JOIN course_dim c ON c.id=e.course_id
  WHERE e.type = 'StudentEnrollment' AND e.workflow_state IN('active') AND c.workflow_state IN('available','completed')
    AND c.[name] LIKE ('% EOB%')
	 AND c.account_id LIKE ('%000".$_GET['provider_ID']."')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>ELAR Blended</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['studentType'])."</div>";
echo "</div>";
echo "<hr />";
}
odbc_free_result($result);

$sql = "SELECT COUNT(e.id) as studentType
  FROM enrollment_dim e 
  INNER JOIN course_dim c ON c.id=e.course_id
  WHERE e.type = 'StudentEnrollment' AND e.workflow_state IN('active') AND c.workflow_state IN('available','completed')
    AND c.[name] LIKE ('% EOC%')
	 AND c.account_id LIKE ('%000".$_GET['provider_ID']."')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>ELAR Comprehensive</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['studentType'])."</div>";
echo "</div>";
echo "<hr />";
}
odbc_free_result($result);

$sql = "SELECT COUNT(e.id) as studentType
  FROM enrollment_dim e 
  INNER JOIN course_dim c ON c.id=e.course_id
  WHERE e.type = 'StudentEnrollment' AND e.workflow_state IN('active') AND c.workflow_state IN('available','completed')
    AND c.[name] LIKE ('% EBB%')
	 AND c.account_id LIKE ('%000".$_GET['provider_ID']."')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>ELAR/Bilit Blended</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['studentType'])."</div>";
echo "</div>";
echo "<hr />";
}
odbc_free_result($result);

$sql = "SELECT COUNT(e.id) as studentType
  FROM enrollment_dim e 
  INNER JOIN course_dim c ON c.id=e.course_id
  WHERE e.type = 'StudentEnrollment' AND e.workflow_state IN('active') AND c.workflow_state IN('available','completed')
    AND c.[name] LIKE ('% EBC%')
	 AND c.account_id LIKE ('%000".$_GET['provider_ID']."')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>ELAR/Bilit Comprehensive</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['studentType'])."</div>";
echo "</div>";
echo "<hr />";
}
odbc_free_result($result);

?>

</div>

	<div class='tile4'>
	<h3># of Learners by Region</h3><hr />
<?php
$provider_ID=88;
$reg=1;
while($provider_ID < 108){
 $sql = "SELECT COUNT(e.id) as learners
  FROM enrollment_dim e 
  INNER JOIN course_dim c ON c.id=e.course_id
  INNER JOIN user_dim u ON u.id=e.user_id
  INNER JOIN account_dim a ON c.account_id=a.id
  WHERE e.type = 'StudentEnrollment' AND e.workflow_state IN('active') AND c.workflow_state IN('available','completed')
    AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
  OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
  AND a.canvas_id = '".$provider_ID."'";
  $result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'><a href='learners_ESC.php?provider_ID=".$provider_ID."'>ESC Region ".$reg."</a></div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".number_format($row['learners'])."</div>";
echo "</div>";
if($provider_ID < 107){echo "<hr />";}
}
$provider_ID++;
$reg++;
}
odbc_free_result($result);

?>

</div>


<div class='tile6'>
	<h3># of Learners by Pathway</h3><hr />	
<?php
	$a=1;
	while($a < 5){

	switch ($a) {
    case "1":
        $pathway = 'Admin';
		$comp= 'AOC';
		$blend = 'AOB';
        break;
    case "2":
        $pathway = 'ELAR';
		$comp= 'EOC';
		$blend = 'EOB';
        break;
    case "3":
        $pathway = 'Bilit';
		$comp= 'BOC';
		$blend = 'BOB';     
		break;
	case "4":
        $pathway = 'Dual Path';
		$comp= 'EBC';
		$blend = 'EBB';
        break;
}

$sql = "SELECT COUNT(e.id) as student
  FROM enrollment_dim e 
  INNER JOIN course_dim c ON c.id=e.course_id
  WHERE e.type = 'StudentEnrollment' AND e.workflow_state IN('active') AND c.workflow_state IN('available','completed')
    AND (c.[name] LIKE ('% ".$comp."%') OR c.[name] LIKE ('% ".$blend."%'))
	 AND c.account_id LIKE ('%000".$_GET['provider_ID']."')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)) {
	echo "<div class='breakdownLeft'>";
	echo "<div class='breakdownCat'>".$pathway."</div>";
	echo "</div>";
	echo "<div class='breakdownRight'>";
	echo "<div class='breakdownCat'>".number_format($row['student'])."</div>";
	echo "</div>";
	if($a<4){echo "<hr />";}
	}
	odbc_free_result($result);
	$a++;
	}
	?>
	</div>

<div class='tile5'>
		<h3># of Learners by Term</h3><hr />
	<?php

	$sql = "SELECT id, name as term_name  FROM enrollment_term_dim WHERE name LIKE ('%Reading Acad%') AND date_start > '2020/06/15' ORDER BY date_start";
		$result = odbc_exec($conn2, $sql);
	while($row = odbc_fetch_array($result)) {
		$term_id = $row['id'];
	

	$sql2="SELECT COUNT(e.id) as Enrolled_Term
  FROM enrollment_dim e 
  INNER JOIN course_dim c ON c.id=e.course_id
  INNER JOIN enrollment_term_dim t ON t.id=c.enrollment_term_id
  INNER JOIN user_dim u ON u.id=e.user_id
  INNER JOIN account_dim a ON c.account_id=a.id
  WHERE e.type = 'StudentEnrollment' AND e.workflow_state IN('active') AND c.workflow_state IN('available','completed')
    AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
  OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
  AND c.enrollment_term_id = '".$term_id."'
   AND c.account_id LIKE ('%000".$_GET['provider_ID']."')";
  		$result2 = odbc_exec($conn2, $sql2);
	while($row2 = odbc_fetch_array($result2)) {
	if($row2['Enrolled_Term'] > 0){
	echo "<div class='breakdownLeft'>";
	$term_name = str_replace('Reading Academies', '', $row['term_name']);
	echo "<div class='breakdownCat'>".$term_name."</div>";
	echo "</div>";
	echo "<div class='breakdownRight'>";
	echo "<div class='breakdownCat'>".number_format($row2['Enrolled_Term'])."</div>";
	echo "</div>";
	echo "<hr />";
	}
	}
	}
	odbc_free_result($result);
	odbc_free_result($result2);
	?>
</div>

<div class='tile7'>
	<h3>Learner Progress</h3><hr />
				
			<?php
			$sql = "SELECT COUNT(student_id) as completed
  FROM [readWH].[dbo].[course_completions] m
  INNER JOIN enrollment_dim e ON e.canvas_id=m.enrollment_id
    INNER JOIN course_dim c ON c.id=e.course_id
  INNER JOIN user_dim u ON u.id=e.user_id
  WHERE e.type = 'StudentEnrollment' AND e.workflow_state IN('active') AND c.workflow_state IN('available','completed')
    AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
  OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
  AND m.completion_date IS NOT NULL
   AND c.account_id LIKE ('%000".$_GET['provider_ID']."')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)){
$completed = number_format($row['completed']);
$completedCount = $row['completed'];
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>Completed</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".$completed."</div>";
echo "</div><hr />";
}
odbc_free_result($result);

$sql = "SELECT COUNT(DISTINCT(u.id)) as In_Progress
  FROM [readWH].[dbo].[submission_dim] s 
  INNER JOIN assignment_dim a ON s.assignment_id=a.id 
    INNER JOIN course_dim c ON c.id=a.course_id
  INNER JOIN enrollment_dim e ON e.course_id=c.id
  INNER JOIN user_dim u ON u.id=e.user_id
  WHERE e.type = 'StudentEnrollment' AND e.workflow_state IN('active') 
 AND  a.title LIKE ('%Module 2%pretest%')  AND s.workflow_state IN ('graded') AND s.attempt='1'
        AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
  OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
   AND c.account_id LIKE ('%000".$_GET['provider_ID']."')";
$result = odbc_exec($conn2, $sql);
while($row = odbc_fetch_array($result)){
$in_progressCount = $row['In_Progress']-$completedCount;
$in_progress = number_format($in_progressCount);

echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>In Progress</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
echo "<div class='breakdownCat'>".$in_progress."</div>";
echo "</div><hr />";
}
odbc_free_result($result);

$not_started=$total_studentCount-$in_progressCount-$completedCount;
echo "<div class='breakdownLeft'>";
echo "<div class='breakdownCat'>Not Started</div>";
echo "</div>";
echo "<div class='breakdownRight'>";
if($not_started < 0){
echo "<div class='breakdownCat'>0</div>";
}else{
echo "<div class='breakdownCat'>".number_format($not_started)."</div>";
}
echo "</div>";

?>
</div>


</div>
</center>
</body>
</html>

<?php
odbc_close($conn);
odbc_close($conn2);
}

///////////////////IF SESSION IS NOT ACTIVE RUN BELOW////////
}else{

?>

<html>
<head>
<meta http-equiv="refresh" content="0;url=https://tra.esc11.net/ap/~dashboard.php" />
<title>TRA Authorized Provider Logout</title>
</head>
<body>
You are currently being logged out of the system.
</body>
</html>

<?php
}
?>