<?php
session_start();

$username = "tra_readonly";
$password = "D@taW4$!";
$dbname = "readWH";

$conn = odbc_connect("TRA_DW", $username, $password);
if (odbc_error()) {
 echo odbc_errormsg($conn);
}
else{

//////////WRITE CSV DATA FILE HERE/////////////////////////
if(isset($_POST['btn_export'])){

$course_selected=$_POST['course_selected'];
$termName=$_POST['termName'];
$courseName=$_POST['courseName'];
$provider=$_POST['provider'];

///////////////////////////////////////////////////////////

#I create a file and ready to write 
$myfile = fopen('php://output', 'w') or die("Unable to open file!");

#create an empty variable to story the result for string concatination
$fileData = null;
$fileData .="Course_ID,Provider,Course_Name,Term,Module,Attempts,Completed,In_Progress,Not_Started,Avg_Growth\n";

$a=2;
while($a < 13){
$sql = "
SELECT DISTINCT
c.id as cid,
c.canvas_id as course_id,
a.title as moduleName, 
c.[name] as course_name,
t.[name] as term_name,
a.id as assignment_id
  FROM submission_dim s
  INNER JOIN assignment_dim a ON a.id=s.assignment_id
  INNER JOIN course_dim c ON c.id=a.course_id
  INNER JOIN enrollment_term_dim t ON t.id=c.enrollment_term_id
    WHERE c.canvas_id = '".$course_selected."' AND (a.title LIKE ('%pretest%') OR a.title LIKE ('%posttest%')) AND (a.title LIKE ('% ".$a.":%') OR a.title LIKE ('% ".$a."B:%') OR a.title LIKE ('% ".$a."A:%'))
    ORDER BY a.title DESC";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
$moduleName=$row['moduleName'];
$course_ID=$row['course_id'];
$assignment_ID=$row['assignment_id'];
$cid=$row['cid'];

if(strpos($moduleName, 'Posttest') !== false){
$fileData .= $course_selected.",";
$fileData .= $provider.",";
$fileData .= $courseName.",";
$fileData .= $termName.",";
$fileData .= "Module ".$a." Progression,";
}

$sql2 = "
SELECT COUNT(student_id) as Enrolled
  FROM course_completions WHERE course_id=".$course_ID." AND enrollment_status = 'active'";
$result2 = odbc_exec($conn, $sql2);
while($row2 = odbc_fetch_array($result2)) {
if(strpos($moduleName, 'Posttest') !== false){
$fileData .= $row2['Enrolled'].",";
}
}

$sql3 = "
SELECT        COUNT(s.id) AS Completed
FROM            user_dim AS u INNER JOIN
                         submission_dim AS s INNER JOIN
                         assignment_dim AS a ON a.id = s.assignment_id ON u.id = s.user_id INNER JOIN
                         enrollment_dim ON u.id = enrollment_dim.user_id AND a.course_id = enrollment_dim.course_id
WHERE        (a.id = '".$assignment_ID."') AND (u.workflow_state = 'registered') AND (u.name NOT IN ('Test Student')) AND (a.course_id = '".$cid."') AND (enrollment_dim.workflow_state = 'active') AND 
                         (s.workflow_state = 'graded')";
$result3 = odbc_exec($conn, $sql3);
while($row3 = odbc_fetch_array($result3)) {

if(strpos($moduleName, 'Posttest') !== false){
$fileData .= $row3['Completed'].",";
}

if(strpos($moduleName, 'Pretest') !== false){
$pretest=$row3['Completed'];
}else{
$inProgress = intval($pretest)-intval($row3['Completed']);
$fileData .= $inProgress.",";
}


}

$sql4 = "
SELECT        COUNT(s.id) AS Unsubmitted
FROM            user_dim AS u INNER JOIN
                         submission_dim AS s INNER JOIN
                         assignment_dim AS a ON a.id = s.assignment_id ON u.id = s.user_id INNER JOIN
                         enrollment_dim ON u.id = enrollment_dim.user_id AND a.course_id = enrollment_dim.course_id
WHERE        (a.id = '".$assignment_ID."') AND (u.workflow_state = 'registered') AND (u.name NOT IN ('Test Student')) AND (a.course_id = '".$cid."') AND (enrollment_dim.workflow_state = 'active') AND 
                         (s.workflow_state = 'unsubmitted')";
$result4 = odbc_exec($conn, $sql4);
while($row4 = odbc_fetch_array($result4)) {
if(strpos($moduleName, 'Pretest') !== false){
$notStarted = $row4['Unsubmitted'];
}else{
$fileData .= $notStarted.",";
}


}


$sql5 = "
SELECT AVG(CAST(grade as decimal(10,2))) as Average
    FROM submission_dim s INNER JOIN assignment_dim a ON a.id=s.assignment_id WHERE a.id = ".$assignment_ID." AND s.workflow_state='graded'";
$result5 = odbc_exec($conn, $sql5);
while($row5 = odbc_fetch_array($result5)) {

if(strpos($moduleName, 'Pretest') !== false){
$beginAvg = $row5['Average'];
}else{
$growth = $row5['Average'] - $beginAvg;
if(($growth <= 0) AND ($pretest < 1)) {
$fileData .= "N/A \n";
}else{
$fileData .= $growth." points\n";
}
}

}

}
$a++;
}

    
    header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=module progression_export.csv");

#write the entire data into the file and close the file
fwrite($myfile, $fileData);
//fputcsv($myfile, $fileData);
fclose($myfile);
}

}
?>