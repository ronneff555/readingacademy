<?php
include 'header.php';



// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
else{
?>

<h1>Cohort Leader Details</h1>
<hr />
<?php

if(isset($_POST['leaderUpdate'])){
$leader_ID=$_POST['leader_ID'];

$sql = "SELECT l.leader_ID, l.firstName, l.lastName, l.primaryPhone, l.email, l.canvasAdmin, l.altEmail, l.passedScreener, l.bilit, l.leaderType, l.trainingNotes, l.trainingRequested, l.trainingConfirmed, l.welcomeEmail, l.sandboxSetup, l.zoomSetup, l.emailSetup, l.setupEmail, l.centralEnrolled, p.providerName FROM tbl_leader l INNER JOIN tbl_provider p ON l.provider_ID=p.provider_ID WHERE l.leader_ID = '$leader_ID'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
 // output data of each row
 while($row = $result->fetch_assoc()) {

echo "<table cellspacing='0' cellpadding='5' border='0' width='1000px'>";
echo "<tr><td valign='top' wdith='50%'>";
echo "<h2>".$row['firstName']." ".$row['lastName']."<br />";
echo $row['providerName']."</h2>";
echo "Email: <a href='".$row['email']."'>".$row['email']."</a><br />";
echo "Alt. Email: <a href='".$row['altEmail']."'>".$row['altEmail']."</a><br />";
echo "Phone: ". $row['primaryPhone']."<br />";
echo "Canvas Admin: ";
if($row['canvasAdmin']==1){
echo "<span style='color:green;'>Yes</span>";
}else{
echo "<span style='color:red;'>No</span>";
}
echo "<br />";
echo "<b>Notes:<br /><textarea style='width:450px;' rows='4'>".$row['trainingNotes']."</textarea>";

echo "</td><td valign='top' width='50%'>";

echo "<table cellspacing='0' cellpadding='5' border='1' width='490px' style='background-color:#f7f6f6;'>";
echo "<tr><td valign='top' colspan='3'>";
echo "<h4>Details:</h4>";
echo "Biliterate: ";
if($row['bilit']==1){
echo "<span style='color:green;'>Yes</span>";
}else{
echo "<span style='color:red;'>No</span>";
}
echo "<br />";
echo "Leader Type: ".$row['leaderType']."<br />";
echo "Training Requested: ".(date("F j, Y", strtotime($row['trainingRequested'])))."<br />";
echo "Enrolled in Central: ".(date("F j, Y", strtotime($row['centralEnrolled'])))."<br />";
echo "Training Confirmed: ".(date("F j, Y", strtotime($row['trainingConfirmed'])))."<br />";
echo "Welcome Email Sent: ".(date("F j, Y", strtotime($row['welcomeEmail'])))."<br />";
echo "</td></tr>";
echo "<tr>";
echo "<td align='center'>Sandbox Setup</td>";
echo "<td align='center'>Zoom Setup</td>";
echo "<td align='center'>Email Setup</td>";
echo "</tr>";
echo "<tr>";
echo "<td align='center'>";
if($row['sandboxSetup']==1){
echo "<span style='color:green;'>Yes</span>";
}else{
echo "<span style='color:red;'>No</span>";
}
echo"</td>";
echo "<td align='center'>";
if($row['zoomSetup']==1){
echo "<span style='color:green;'>Yes</span>";
}else{
echo "<span style='color:red;'>No</span>";
}
echo "</td>";
echo "<td align='center'>";
if($row['emailSetup']==1){
echo "<span style='color:green;'>Yes</span>";
}else{
echo "<span style='color:red;'>No</span>";
}
echo "</td>";
echo "</tr>";
echo "<tr><td colspan='3'>";

echo "</td></tr>";
echo "</table>";

echo "</td></tr>";
echo "</table>";
echo "<hr />";

echo "<h3>Associated Cohorts</h3>";
echo "<table cellspacing='0' cellpadding='5' border='0' width='1000px'>";
echo "<tr>";
echo "<td valign='top' wdith='20%'>Cohort Name 1</td>";
echo "<td valign='top' wdith='20%'>Leader</td>";
echo "<td valign='top' wdith='20%'>Target Audience</td>";
echo "<td valign='top' wdith='20%'>Blended</td>";
echo "<td valign='top' wdith='20%'>September 2020</td>";
echo "</tr>";
echo "<tr style='background-color:#eeeeee;'>";
echo "<td valign='top' wdith='20%'>Cohort Name 2</td>";
echo "<td valign='top' wdith='20%'>Leader</td>";
echo "<td valign='top' wdith='20%'>ELAR</td>";
echo "<td valign='top' wdith='20%'>Blended</td>";
echo "<td valign='top' wdith='20%'>October 2020</td>";
echo "</tr>";
echo "<tr>";
echo "<td valign='top' wdith='20%'>Cohort Name 3</td>";
echo "<td valign='top' wdith='20%'>Leader</td>";
echo "<td valign='top' wdith='20%'>ELAR</td>";
echo "<td valign='top' wdith='20%'>Blended</td>";
echo "<td valign='top' wdith='20%'>April 2021</td>";
echo "</tr>";
echo "<tr style='background-color:#eeeeee;'>";
echo "<td valign='top' wdith='20%'>Cohort Name 4</td>";
echo "<td valign='top' wdith='20%'>Support Person</td>";
echo "<td valign='top' wdith='20%'>Admin</td>";
echo "<td valign='top' wdith='20%'>Blended</td>";
echo "<td valign='top' wdith='20%'>June 2021</td>";
echo "</tr>";
echo "</table>";

echo "<hr />";

echo "<h3>Continuing Education Courses</h3>";
}
}
}else {

?>
<form method="POST" action="leaders.php" name="leaderUpdate">
	  <select name="leader_ID">
<?php 

$sql2 = "SELECT leader_ID, firstName, lastName FROM tbl_leader ORDER BY lastName";
$result2 = $conn->query($sql2);

if ($result2->num_rows > 0) {
 // output data of each row
  while($row2 = $result2->fetch_assoc()) {
$leader_ID = $row2['leader_ID'];
$firstName = $row2['firstName'];
$lastName = $row2['lastName'];

echo "<option value='".$leader_ID."'>".$lastName.", ".$firstName."</option>";
}
} 
else { echo "No results"; }

?>	
</select>
<input type="Submit" name="leaderUpdate" value="Submit" />
</form> 

<?php
}

?>







<?php
$conn->close();
}
include 'footer.php';
?>