<?php
session_start();
if(isset($_SESSION['active_AP'])){$login="<a href='logout.php' class='logout'>[Logout]</a>";
?>



<html>
<head>
<title>TEA Reading Academy - Learner Progress Report</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

<style>
body{font-family:Tahoma;background-color:#efefef;color:#707476;}
a, a:visited, a:hover{color:#ef6036;text-decoration:none;}
div.pageMaster{width:1050px;background-color:#fff;border:1px solid #000;}
div.header{width:1000px;height:125px;border:0px solid #000;margin-top:20px;}
	div.logo{float:left;margin-left:15px;width:400px;border:0px solid green;}
	div.title{float:right;font-weight:bold;font-size:36pt;margin-right:30px;color:#0c6bba;text-transform:uppercase;margin-top:30px;}
	div.breadcrumb{font-size:10pt;margin-top:0px;padding-left:40px;color:#ef6036;width:960px;text-align:left;border:0px solid green;}
	div.breadcrumb a, div.breadcrumb a:visited, div.breadcrumb a:hover{color:#0c6bba;}
#title{width:100%;}
h1{color:#707476;margin-bottom:5px;}
td{font-size:11px;}
#course_details{display:none;}
.logout, .logout a, .logout a:hover, .logout a:visited{font-size:8pt;color:#ef6036!important;}
div.welcome{float:right;color:#707476;font-size:10pt;margin-right:10px;}
.greentext{background-color:#66cc66;color:green;}
.redtext{color:red;}
</style>

</head>
<body>

<?php


$provider_ID=$courseName=$inProgress=$pretest=$u_id=$provider_name=$mod=$mod2=$course_ID=$provider=$ap_select='0';
$course_selected=$termName='';
$provider_ID=$_SESSION['providerCanvas'];

if(isset($_POST['course_btn'])){
$provider_ID=$_POST['ap_select'];
$course_selected=$_POST['course_selected'];
echo "<style>";
echo "#course_details{display:block;}";
echo "</style>";
}

$ap_id = $canvas_sub_account_id = '';


$username = "tra_readonly";
$password = "D@taW4$!";
$dbname = "readWH";

$conn = odbc_connect("TRA_DW", $username, $password);
if (odbc_error()) {
 echo odbc_errormsg($conn);
}
else{

echo "<center>";
echo "<div class='pageMaster'>";
echo "<div class='welcome'>".$_SESSION['firstName']." ".$_SESSION['lastName']."<br />".$login."</div>";
echo "<div class='header'>";
echo "<div class='logo'><a href='~dashboard.php'><img src='../images/TRA_logo.jpg' /></a></div>";
echo "<div class='title'>Learner Progress</div>";
echo "</div>";

$sql = "SELECT id, name, canvas_sub_account_id FROM authorized_providers WHERE canvas_sub_account_id = '".$provider_ID."' ORDER BY id";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
$provider_name=$row['name'];
}
odbc_free_result($result);

echo "<h1>Courses by ".$provider_name."</h1>";
//echo "Blah Blah Blah.";
echo "<hr />";

echo "<table cellpadding='2' cellspacing='0' border='1' width='900px'>";
echo "<tr><td>Course ID</td><td>Provider</td><td>Course</td><td>Term</td>";
echo "<tr>";

$sql = "
SELECT c.id, c.canvas_id, c.enrollment_term_id, c.name as courseName, t.name AS termName, a.name AS provider, a.canvas_id as provider_ID FROM course_dim c 
INNER JOIN enrollment_term_dim t ON c.enrollment_term_id=t.id
INNER JOIN account_dim a ON a.id=c.account_id
WHERE c.workflow_state = 'available' AND c.name NOT LIKE ('%Passport%') AND t.name LIKE ('%Reading Academies%') AND a.workflow_state = 'active' AND a.canvas_id='".$provider_ID."' ORDER BY provider_ID, t.canvas_id, c.id";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
$course_ID=$row['canvas_id'];
$provider=$row['provider'];
echo "<tr><td>";
echo "<form method='post' action='learner_progress_AP.php'>";
echo "<input type='hidden' name='course_selected' value='".$row['canvas_id']."' />";
echo "<input type='hidden' name='ap_select' value='".$provider_ID."' />";
echo "<input type='submit' name='course_btn' value='".$row['canvas_id']."' />";
echo "</form>";
echo "</td><td>".$row['provider']."</td><td>".$row['courseName']."</td><td>".substr($row['termName'],18)."</td>";

echo "</tr>";
}
odbc_free_result($result);
echo "</table><br />&nbsp;";

echo "<div id='course_details'>";
echo "<h2>Module Completion by Learner</h2>";
echo "<hr />";
echo "<table cellpadding='2' cellspacing='0' border='1'>";
echo "<tr><td>Course ID</td><td>Course Name</td><td>Term</td>
<td>Learner</td>
<td>District</td>
<td>Campus</td>
<td>Mod 1</td>
<td>Mod 2</td>
<td>Mod 3</td>
<td>Mod 4</td>
<td>Mod 5</td>
<td>Mod 6</td>
<td>Mod 7</td>
<td>Mod 8</td>
<td>Mod 9</td>
<td>Mod 10</td>
<td>Mod 11</td>
<td>Mod 12</td><tr>";

//$sql = "SELECT id, name, canvas_sub_account_id FROM authorized_providers WHERE canvas_sub_account_id = ".$provider_ID." ORDER BY id";
//$result = odbc_exec($conn, $sql);
//while($row = odbc_fetch_array($result)) {
//$provider_name = $row['name'];
//}
//odbc_free_result($result);

$sql = "
SELECT c.id as course_id
	  ,c.canvas_id as course_canvas_id
      ,c.[name] as courseName
	  ,t.name as termName
	  ,u.canvas_id as user_canvas_id
	  ,u.id as user_id
	  ,u.[name] as learnerName
	  ,s.[name] as district
	  ,a.[name] as campus
  FROM [readWH].[dbo].[course_dim] c 
  INNER JOIN enrollment_dim e ON c.id=e.course_id
  INNER JOIN enrollment_term_dim t ON c.enrollment_term_id=t.id
  INNER JOIN user_dim u ON u.id=e.user_id
  INNER JOIN user_demographics d ON d.user_id=u.canvas_id
  LEFT OUTER JOIN districts s ON s.id=d.district_id
  LEFT OUTER JOIN campuses a ON a.id=d.campus_id
  WHERE c.canvas_id='".$course_selected."' AND u.name NOT IN('Test Student') AND e.type = 'StudentEnrollment'
  AND e.workflow_state NOT IN('deleted')
  ORDER BY campus, learnerName";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
$courseName=$row['courseName'];
$termName=substr($row['termName'],18);
$u_id=$row['user_id'];

echo "<td>".$row['course_canvas_id']."</td><td>".$courseName."</td><td>".$termName."</td>";
echo "<td>".$row['learnerName']."</td><td>".$row['district']."</td><td>".$row['campus']."</td>";

$sql2 = "
SELECT COUNT (s.id) as Count
  FROM [readWH].[dbo].[assignment_dim] a
  INNER JOIN course_dim c ON a.course_id=c.id
  INNER JOIN submission_dim s ON s.assignment_id=a.id
  INNER JOIN user_dim u ON u.id=s.user_id
  WHERE a.title LIKE ('%Module 2%pretest%') AND c.canvas_id = '".$course_selected."' AND s.user_id = '".$u_id."' AND s.workflow_state IN('submitted','pending_review','graded')";
  $result2 = odbc_exec($conn, $sql2);
while($row2 = odbc_fetch_array($result2)) {
$mod2=$row2['Count'];
if($mod2 > 0){echo "<td class='greentext'>Yes</td>";}else{echo "<td class='redtext'>No</td>";}
}

$a=2;
while($a < 13){
$sql3 = 
"SELECT COUNT (s.id) as Count
  FROM [readWH].[dbo].[assignment_dim] a
  INNER JOIN course_dim c ON a.course_id=c.id
  INNER JOIN submission_dim s ON s.assignment_id=a.id
  INNER JOIN user_dim u ON u.id=s.user_id
 WHERE c.canvas_id = '".$course_selected."' AND a.title LIKE ('%posttest%') AND (a.title LIKE ('% ".$a.":%') OR a.title LIKE ('% ".$a."B:%') OR a.title LIKE ('% ".$a."A:%'))
  AND s.user_id = '".$u_id."'
  AND s.workflow_state IN('submitted','pending_review','graded')";
    $result3 = odbc_exec($conn, $sql3);
while($row3 = odbc_fetch_array($result3)) {
  $mod=$row3['Count'];
if($mod > 0){echo "<td class='greentext'>Yes</td>";}else{echo "<td class='redtext'>No</td>";}
}
$a++;
}

echo "</tr>";

}
echo "</table><br />";

echo "<form method='post' action='learner_progress_export.php'>";
echo "<input type='hidden' name='course_selected' value='".$course_selected."' />";
echo "<input type='hidden' name='courseName' value='".$courseName."' />";
echo "<input type='hidden' name='termName' value='".$termName."' />";
echo "<input type='submit' name='btn_export' value='Export Results' />";
echo "</form>";

echo "</div>";

odbc_close($conn);
}
echo "</div>";
?>

</body>
</html>

<?php
///////////////////IF SESSION IS NOT ACTIVE RUN BELOW////////
}else{

?>

<html>
<head>
<meta http-equiv="refresh" content="0;url=https://tra.esc11.net/ap/~dashboard.php" />
<title>TRA Authorized Provider Logout</title>
</head>
<body>
You are currently being logged out of the system.
</body>
</html>
<?php
}
?>