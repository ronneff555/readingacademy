<?php
session_start();
if(isset($_SESSION['active_AP'])){$login="<a href='logout.php' class='logout'>[Logout]</a>";

$provider_ID=$_SESSION['providerCanvas'];

?>


<html>
<head>
<title>TEA Reading Academy - State Module Progression Report</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

<style>
body{font-family:Tahoma;background-color:#efefef;color:#707476;}
a, a:visited, a:hover{color:#0c6bba;text-decoration:none;}
div.pageMaster{width:1050px;background-color:#fff;border:1px solid #000;padding-bottom:20x;}
div.header{width:1000px;height:125px;border:0px solid #000;margin-top:20px;}
	div.logo{float:left;margin-left:15px;width:400px;border:0px solid green;}
	div.title{float:right;font-weight:bold;font-size:36pt;margin-right:30px;color:#0c6bba;text-transform:uppercase;margin-top:30px;}
	div.breadcrumb{font-size:10pt;margin-top:0px;padding-left:40px;color:#ef6036;width:960px;text-align:left;border:0px solid green;}
	div.breadcrumb a, div.breadcrumb a:visited, div.breadcrumb a:hover{color:#0c6bba;}
#title{width:100%;}
h1{color:#707476;}
#ap_div{float:left;background-color:white;margin-left:10px;margin-top:5px;}
#ap_results, #term_results, #course_results, #assignment_results, #data_results{display:none;}
#ap_results td, #term_results td, #course_results td, #assignment_results td, #data_results td{font-size:9pt;}
#term_div, #course_div, #assignment_div{float:left;margin-left:10px;display:none;background-color:white;margin-top:5px;}
#data_div{margin-top:200px;width:1000px!important;display:none;}
#data_submit{background-color:red;color:White;font-size:24px;width:350px;}
label{font-size:9pt;}
#ap_select_all, #term_select_all, #course_select_all, #assignment_select_all{border:0px;background-color:transparent;color:red;}
td{font-size:11px;}
#course_details{}
.logout, .logout a, .logout a:hover, .logout a:visited{font-size:8pt;color:#ef6036!important;}
div.welcome{float:right;color:#707476;font-size:10pt;margin-right:10px;}

.provider_td, .term_td{display:none;}
</style>

</head>
<body>

<?php
$ap_filter='';
$term_id=$ap_id = $canvas_sub_account_id = '';



if(isset($_POST['filter_submit'])){
$ap_id = $_POST['ap_select'];
$term_id=$_POST['term_select'];

////PICKED ALL PROVIDERS///
if($ap_id=='999999'){
	if($term_id=='999999'){}else{$ap_filter= "AND c.enrollment_term_id =".$term_id." ";echo "<style>.term_td{display:table-cell;}</style>";}
}else{
	if($term_id=='999999'){$ap_filter=" AND c.account_id LIKE '%000".$ap_id."' ";echo "<style>.provider_td{display:table-cell;}</style>";}else{$ap_filter=" AND c.account_id LIKE '%0000".$ap_id."' AND c.enrollment_term_id ='".$term_id."' ";echo "<style>.provider_td, .term_td{display:table-cell;}</style>";}
}
}

$username = "tra_readonly";
$password = "D@taW4$!";
$dbname = "readWH";

$conn = odbc_connect("TRA_DW", $username, $password);
if (odbc_error()) {
 echo odbc_errormsg($conn);
}
else{


echo "<center>";
echo "<div class='pageMaster'>";
echo "<div class='welcome'>".$_SESSION['firstName']." ".$_SESSION['lastName']."<br />".$login."</div>";
echo "<div class='header'>";
echo "<div class='logo'><a href='~dashboard.php'><img src='../images/TRA_logo.jpg' /></a></div>";
echo "<div class='title'>Module Progress</div>";
echo "</div>";


echo "<h1>State Level Module Progression</h1>";
echo "This reports the module progress of all learners and provides the change in percentage points from the pretests to the posttests.<br />Filtering can be done by Authorized Provider and/or Term.";
echo "<hr />";

echo "<b>Filter by:</b><br />";
echo "<form method='POST' action='state_module_progression.php'>";
echo "<select name='ap_select'>";
echo "<option value='999999'>All Providers</option>";
$sql = "SELECT id, name, canvas_sub_account_id FROM authorized_providers WHERE canvas_sub_account_id='".$provider_ID."' ORDER BY id";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
$canvas_sub_account_id=$row['canvas_sub_account_id'];
if($ap_id==$canvas_sub_account_id){
echo "<option value='".$row['canvas_sub_account_id']."' selected>".$row['name']."</option>";
}else{
echo "<option value='".$row['canvas_sub_account_id']."'>".$row['name']."</option>";
}


}
odbc_free_result($result);
echo "</select>";

echo "<select name='term_select'>";
echo "<option value='999999'>All Terms</option>";
$sql = "SELECT id, name
  FROM enrollment_term_dim
  WHERE name LIKE('%Reading%')
  ORDER BY date_start";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
if($term_id==$row['id']){
echo "<option value='".$row['id']."' selected>".$row['name']."</option>";
}else{
	echo "<option value='".$row['id']."'>".$row['name']."</option>";
}


}
odbc_free_result($result);
echo "</select>";


echo "<input type='submit' name='filter_submit' value='Go' />";
echo "</form>";
echo "<hr />";



echo "<div id='course_details'>";
echo "<hr />";
echo "<h2>Module Progression</h2>";
echo "<table cellpadding='2' cellspacing='0' border='1' width='1000px'>";
echo "<tr><td class='provider_td'>Provider</td><td class='term_td'>Term</td><td>Module</td>";
echo "<td>Completed</td><td>In Progress</td><td>Not Started</td>";
echo "<td>Pre-Test Avg</td><td>Post-Test Avg</td>";
echo "<td>Growth</td><tr>";


$a=2;
while($a < 13){
echo "<tr>";
if(isset($_POST['filter_submit'])){
$sql = "SELECT name FROM authorized_providers WHERE canvas_sub_account_id=".$ap_id;
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
echo "<td class='provider_td'>".$row['name']."</td>";
}
odbc_free_result($result);
}
if(isset($_POST['filter_submit'])){
$sql = "SELECT name FROM enrollment_term_dim WHERE id=".$term_id;
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
echo "<td class='term_td'>".$row['name']."</td>";
}
odbc_free_result($result);
}
echo "<td>Module ".$a." Progression</td>";



$sql2 = "SELECT COUNT(DISTINCT s.id) as Completed_PostTest_D
  FROM            user_dim u 
INNER JOIN  submission_dim s ON u.id = s.user_id
INNER JOIN  assignment_dim a ON a.id = s.assignment_id  
INNER JOIN  enrollment_dim e ON u.id = e.user_id AND a.course_id = e.course_id
INNER JOIN course_dim c ON c.id=a.course_id
  WHERE (u.workflow_state = 'registered') AND (u.name NOT IN ('Test Student')) AND (e.workflow_state = 'active')
AND a.title LIKE ('Module ".$a."%posttest%') 
	    AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
  OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
  ".$ap_filter."
  AND s.workflow_state = 'graded' AND s.grade IS NOT NULL";
$result2 = odbc_exec($conn, $sql2);
while($row2 = odbc_fetch_array($result2)) {
$variableD=$row2['Completed_PostTest_D'];
echo "<td>".number_format($row2['Completed_PostTest_D'])."</td>";
}
odbc_free_result($result2);

$sql2 = "SELECT COUNT(DISTINCT s.id) as Completed_PreTest_C
  FROM            user_dim u 
INNER JOIN  submission_dim s ON u.id = s.user_id
INNER JOIN  assignment_dim a ON a.id = s.assignment_id  
INNER JOIN  enrollment_dim e ON u.id = e.user_id AND a.course_id = e.course_id
INNER JOIN course_dim c ON c.id=a.course_id
  WHERE (u.workflow_state = 'registered') AND (u.name NOT IN ('Test Student')) AND (e.workflow_state = 'active')
AND a.title LIKE ('Module ".$a."%pretest%') 
	    AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
  OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
  ".$ap_filter."
  AND s.workflow_state = 'graded' AND s.grade IS NOT NULL";
$result2 = odbc_exec($conn, $sql2);
while($row2 = odbc_fetch_array($result2)) {
$variableC=$row2['Completed_PreTest_C'];
}
odbc_free_result($result2);

$inProgress = $variableC - $variableD;
echo "<td>".number_format($inProgress)."</td>";

$sql2 = "SELECT COUNT(DISTINCT s.id) as PreTest_NotStarted_H
  FROM            user_dim u 
INNER JOIN  submission_dim s ON u.id = s.user_id
INNER JOIN  assignment_dim a ON a.id = s.assignment_id  
INNER JOIN  enrollment_dim e ON u.id = e.user_id AND a.course_id = e.course_id
INNER JOIN course_dim c ON c.id=a.course_id
  WHERE (u.workflow_state = 'registered') AND (u.name NOT IN ('Test Student')) AND (e.workflow_state = 'active')
AND a.title LIKE ('Module ".$a."%pretest%') 
	    AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
  OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
  ".$ap_filter."
  AND s.workflow_state = 'unsubmitted'";
$result2 = odbc_exec($conn, $sql2);
while($row2 = odbc_fetch_array($result2)) {
$variableH=$row2['PreTest_NotStarted_H'];
echo "<td>".number_format($row2['PreTest_NotStarted_H'])."</td>";
}
odbc_free_result($result2);

$sql2 = "SELECT AVG(CAST(s.grade as decimal(10,2))) AVG_PostTest_G
  FROM            user_dim u 
INNER JOIN  submission_dim s ON u.id = s.user_id
INNER JOIN  assignment_dim a ON a.id = s.assignment_id  
INNER JOIN  enrollment_dim e ON u.id = e.user_id AND a.course_id = e.course_id
INNER JOIN course_dim c ON c.id=a.course_id
  WHERE (u.workflow_state = 'registered') AND (u.name NOT IN ('Test Student')) AND (e.workflow_state = 'active')
AND a.title LIKE ('Module ".$a."%posttest%') 
	    AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
  OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
  ".$ap_filter."
  AND s.workflow_state = 'graded' AND s.grade IS NOT NULL";
$result2 = odbc_exec($conn, $sql2);
while($row2 = odbc_fetch_array($result2)) {
$variableG=$row2['AVG_PostTest_G'];
}
odbc_free_result($result2);

$sql2 = "SELECT AVG(CAST(s.grade as decimal(10,2))) as AVG_PreTest_F
  FROM            user_dim u 
INNER JOIN  submission_dim s ON u.id = s.user_id
INNER JOIN  assignment_dim a ON a.id = s.assignment_id  
INNER JOIN  enrollment_dim e ON u.id = e.user_id AND a.course_id = e.course_id
INNER JOIN course_dim c ON c.id=a.course_id
  WHERE (u.workflow_state = 'registered') AND (u.name NOT IN ('Test Student')) AND (e.workflow_state = 'active')
AND a.title LIKE ('Module ".$a."%pretest%') 
	    AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
  OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
  ".$ap_filter."
  AND s.workflow_state = 'graded' AND s.grade IS NOT NULL";
$result2 = odbc_exec($conn, $sql2);
while($row2 = odbc_fetch_array($result2)) {
$variableF=$row2['AVG_PreTest_F'];
}
odbc_free_result($result2);

$avgGrowth = round($variableG,2) - round($variableF,2);
echo "<td>".round($variableF,2)."</td><td>".round($variableG,2)."</td>";
if($avgGrowth > 0){
echo "<td style='color:green;font-weight:bold;'>".$avgGrowth."</td><tr>";
}else{
echo "<td style='color:red;font-style:italic;'>".$avgGrowth."</td><tr>";
}


$a++;
}


echo "</table>";
echo "<br />";
echo "<form method='post' action='state_module_progression_export.php'>";

if(isset($_POST['filter_submit'])){
echo "<input type='hidden' name='provider_id' value='".$_POST['ap_select']."' />";
echo "<input type='hidden' name='term_id' value='".$_POST['term_select']."' />";
}else{
echo "<input type='hidden' name='provider_id' value='999999' />";
echo "<input type='hidden' name='term_id' value='999999' />";
}
echo "<input type='submit' name='btn_export' value='Export Results' />";
echo "</form>";

echo "<br />";
echo "</div>";

odbc_close($conn);
}
?>
</div>
</body>
</html>


<?php
///////////////////IF SESSION IS NOT ACTIVE RUN BELOW////////
}else{

?>

<html>
<head>
<meta http-equiv="refresh" content="0;url=https://tra.esc11.net/ap/~dashboard.php" />
<title>TRA Authorized Provider Logout</title>
</head>
<body>
You are currently being logged out of the system.
</body>
</html>

<?php
}
?>