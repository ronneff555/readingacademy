<?php
session_start();
if(isset($_SESSION['active_AP'])){$login="<a href='logout.php' class='logout'>[Logout]</a>";
$provider_ID=$_SESSION['providerCanvas'];
?>


<html>
<head>
<title>TEA Reading Academy - CSV Data Pull</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

<style>
body{font-family:Tahoma;background-color:#efefef;color:#707476;}
a, a:visited, a:hover{color:#ef6036;text-decoration:none;}
div.pageMaster{width:1050px;background-color:#fff;border:1px solid #000;}
div.header{width:1000px;height:125px;border:0px solid #000;margin-top:20px;}
	div.logo{float:left;margin-left:15px;width:400px;border:0px solid green;}
	div.title{float:right;font-weight:bold;font-size:40pt;margin-right:30px;color:#0c6bba;text-transform:uppercase;margin-top:30px;}
	div.breadcrumb{font-size:10pt;margin-top:0px;padding-left:40px;color:#ef6036;width:960px;text-align:left;border:0px solid green;}
	div.breadcrumb a, div.breadcrumb a:visited, div.breadcrumb a:hover{color:#0c6bba;}
#title{width:100%;}
h1{color:#707476;margin-bottom:5px;}
#ap_div{float:left;background-color:white;margin-left:10px;margin-top:5px;}
#ap_results, #term_results, #course_results, #assignment_results, #data_results{display:none;}
#ap_results td, #term_results td, #course_results td, #assignment_results td, #data_results td{font-size:9pt;}
#term_div, #course_div, #assignment_div{float:left;margin-left:10px;display:none;background-color:white;margin-top:5px;}
#data_div{margin-top:200px;width:1000px!important;display:none;}
#data_submit{background-color:red;color:White;font-size:24px;width:350px;}
label{font-size:9pt;}
#ap_select_all, #term_select_all, #course_select_all, #assignment_select_all{border:0px;background-color:transparent;color:red;}
.logout, .logout a, .logout a:hover, .logout a:visited{font-size:8pt;color:#ef6036!important;}
div.welcome{float:right;color:#707476;font-size:10pt;margin-right:10px;}
</style>

</head>
<body>

<?php

$username = "tra_readonly";
$password = "D@taW4$!";
$dbname = "readWH";

$conn = odbc_connect("TRA_DW", $username, $password);
if (odbc_error()) {
 echo odbc_errormsg($conn);
}
else{

$apArray = $ap_canvas_id = $termArray = $courseArray = $course_id = $assignment_id = '0';
$assignmentArray = $assignment_name = '';
$term_abbr=$term_name=$ra=$term='';
$number_rows='0';

//////////AP POST//////////
//if(isset($_POST['ap_submit'])){
echo "<style>";
echo "#ap_form{display:none;}";
echo "#ap_results{display:block;background-color:#ececec;}";
echo "#term_div{display:block;}";
echo "</style>";

//////////TERMS POST//////////
if(isset($_POST['term_submit'])){
echo "<style>";
echo "#ap_form, #term_form{display:none;}";
echo "#ap_results, #term_results{display:block;background-color:#ececec;}";
echo "#ap_div, #term_div, #course_div{display:block;}";
echo "</style>";
if(!empty($_POST['term_id'])){$term_id = ($_POST['term_id']); $termArray = "'0','".implode($term_id, "','")."'";}
//$apArray = $_POST['apArray_selected'];
//echo "AP LIST - ".$apArray."<br />";
//echo "TERM LIST - ".$termArray."<br />";
}
//////////COURSES POST//////////
if(isset($_POST['course_submit'])){
echo "<style>";
echo "#ap_form, #term_form, #course_form{display:none;}";
echo "#ap_results, #term_results, #course_results{display:block;background-color:#ececec;}";
echo "#ap_div, #term_div,#course_div, #assignment_div{display:block;}";
echo "</style>";
if(!empty($_POST['course_id'])){$course_id = ($_POST['course_id']); $courseArray = "'0','".implode($course_id, "','")."'";}
//$apArray = $_POST['apArray_selected'];
$termArray = $_POST['termArray_selected'];
//echo "AP LIST - ".$apArray."<br />";
//echo "TERM LIST - ".$termArray."<br />";
//echo "COURSE LIST - ".$courseArray."<br />";
}
//////////ASSIGNMENT POST//////////
if(isset($_POST['assignment_submit'])){
echo "<style>";
echo "#ap_form, #term_form, #course_form, #assignment_form{display:none;}";
echo "#ap_results, #term_results, #course_results, #assignment_results{display:block;background-color:#ececec;}";
echo "#ap_div, #term_div,#course_div, #assignment_div, #data_div{display:block;}";
echo "</style>";
if(!empty($_POST['assignment_name'])){$assignment_name = ($_POST['assignment_name']); $assignmentArray = "'99999999','".implode($assignment_name, "','")."'";}
//$apArray = $_POST['apArray_selected'];
$termArray = $_POST['termArray_selected'];
$courseArray = $_POST['courseArray_selected'];
//echo "AP LIST - ".$apArray."<br />";
//echo "TERM LIST - ".$termArray."<br />";
//echo "COURSE LIST - ".$courseArray."<br />";
//echo "ASSIGNMENT LIST - ".$assignmentArray."<br />";

$sql = "SELECT COUNT(s.id) as Count FROM [readWH].[dbo].[enrollment_term_dim] t INNER JOIN course_dim c ON t.id = c.enrollment_term_id INNER JOIN account_dim a ON a.id=c.account_id INNER JOIN authorized_providers p ON p.canvas_sub_account_id=a.canvas_id INNER JOIN assignment_dim s ON s.course_id=c.id INNER JOIN submission_dim m ON m.assignment_id=s.id INNER JOIN user_dim u ON u.id=m.[user_id] WHERE p.canvas_sub_account_id = '".$provider_ID."' AND c.workflow_state = 'available' AND t.id IN (".$termArray.") AND c.id IN(".$courseArray.") AND s.title IN(".$assignmentArray.")";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
$number_rows=$row['Count'];
}
odbc_free_result($result);

}

echo "<center>";
echo "<div class='pageMaster'>";
echo "<div class='welcome'>".$_SESSION['firstName']." ".$_SESSION['lastName']."<br />".$login."</div>";
echo "<div class='header'>";
echo "<div class='logo'><a href='~dashboard.php'><img src='../images/TRA_logo.jpg' /></a></div>";
echo "<div class='title'>CSV Data Export</div>";
echo "</div>";
echo "<div style='margin:10px 20px 10px 20px;'>This feature allows you to pull raw assignment data at the user level by filtering term, course, and assignment. A CSV file will download. Filtering is recommended to ensure the CSV will download.</div>";
echo "<div id='title'><h1>Auth Provider -> Term -> Courses -> Assignments</h1>";
echo "<span style='font-size:12px;'><a href='csv_pull.php'>Start Over</a></span><br />&nbsp;</div>";





////////////////////////////////////////////////////////////
/////////////// AUTHORIZE PROVIDER DIV /////////////////////
////////////////////////////////////////////////////////////

echo "<div id='ap_div'>";
echo "<div id='ap_form'>";
echo "<form name='form_AP' method='POST' action='csv_pull.php'>";
echo "<table border='1' cellspacing='0' cellpadding='5px' width='250px'>";
echo "<tr><th>Authorized Providers</th></tr>";
echo "<tr><td>";

$sql = "SELECT canvas_sub_account_id as ap_canvas_id, name as ap_name FROM authorized_providers WHERE workflow_state = 'active' ORDER BY id";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
echo "<input type='checkbox' name='ap_canvas_id[]' id='ap_".$row['ap_canvas_id']."' class='ap_checkbox' value='".$row['ap_canvas_id']."' /> <label for='ap_".$row['ap_canvas_id']."'>".$row['ap_name']."</label><br />";
}
odbc_free_result($result);

echo "<input type='button' class='select_all' id='ap_select_all' value='Check All' /><br />";
?>
   <script type="text/javascript"> 
$('#ap_select_all').ready(function(){
    $('.select_all:button').toggle(function(){
        $('.ap_checkbox').attr('checked','checked');
        $(this).val('Uncheck All')
    },function(){
        $('.ap_checkbox').removeAttr('checked');
        $(this).val('Check All');        
    })
})
</script>
<?php

echo "</td></tr>";
echo "<tr><td><input type='submit' id='ap_submit' name='ap_submit' value='Filter Providers' /></td></tr>";
echo "</table>";
echo "</form>";
echo "</div>";

echo "<div id='ap_results'>";
echo "<table border='1' cellspacing='0' cellpadding='5px' width=250px'>";
echo "<tr><th>Authorized Providers</th></tr>";
echo "<tr><td>";
$sql = "SELECT canvas_sub_account_id as ap_canvas_id, name as ap_name FROM authorized_providers WHERE canvas_sub_account_id = '".$provider_ID."' ORDER BY id";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
echo $row['ap_name']."<br />";
}
odbc_free_result($result);
echo "</td></tr></table>";
echo "</div>";
echo "</div>";


////////////////////////////////////////////////////////////
////////////////// TERMS DIV STARTS HERE////////////////////
////////////////////////////////////////////////////////////
echo "<div id='term_div'>";
echo "<div id='term_form'>";
echo "<form name='form_terms' method='POST' action='csv_pull.php'>";
echo "<table border='1' cellspacing='0' cellpadding='5px' width='250px'>";
echo "<tr><th>Available Terms</th></tr>";
echo "<tr><td>";

$sql = "SELECT DISTINCT t.id as term_id, t.canvas_id as term_canvas_id, t.name as term_name FROM enrollment_term_dim t INNER JOIN course_dim c ON t.id = c.enrollment_term_id INNER JOIN account_dim a ON a.id=c.account_id INNER JOIN authorized_providers p ON p.canvas_sub_account_id=a.canvas_id WHERE p.canvas_sub_account_id ='".$provider_ID."' AND t.canvas_id > 1 ORDER BY t.canvas_id";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
$term_name = $row['term_name'];
$term_abbr = str_replace("Reading Academies", "RA - ", $term_name);

echo "<input type='checkbox' name='term_id[]' id='term_".$row['term_id']."' class='term_checkbox' value='".$row['term_id']."' /> <label for='term_".$row['term_id']."'>".$term_abbr."</label><br />";
}
odbc_free_result($result);

echo "<input type='button' class='term_select_all' id='term_select_all' value='Check All' /><br />";
?>
   <script type="text/javascript"> 
$('#term_select_all').ready(function(){
    $('.term_select_all:button').toggle(function(){
        $('.term_checkbox').attr('checked','checked');
        $(this).val('Uncheck All')
    },function(){
        $('.term_checkbox').removeAttr('checked');
        $(this).val('Check All');        
    })
})
</script>
<?php

echo "</td></tr>";
echo "<tr><td>";
//echo '<input type="hidden" name="apArray_selected" value="'.$apArray.'" />';
echo "<input type='submit' id='term_submit' name='term_submit' value='Filter Terms' /></td></tr>";
echo "</table>";
echo "</form>";
echo "</div>";

echo "<div id='term_results'>";
echo "<table border='1' cellspacing='0' cellpadding='5px' width='250px'>";
echo "<tr><th>Available Terms</th></tr>";
echo "<tr><td>";
$sql = "SELECT name as term_name FROM enrollment_term_dim WHERE id IN(".$termArray.") ORDER BY id";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
$term_name = $row['term_name'];
$term_abbr = str_replace("Reading Academies", "RA - ", $term_name);
echo $term_abbr."<br />";
}
odbc_free_result($result);
echo "</td></tr></table>";
echo "</div>";
echo "</div>";

////////////////////////////////////////////////////////////
//////////////////// COURSES DIV ///////////////////////////
////////////////////////////////////////////////////////////

echo "<div id='course_div'>";
echo "<div id='course_form'>";
echo "<form name='form_course' method='POST' action='csv_pull.php'>";
echo "<table border='1' cellspacing='0' cellpadding='5px' width='250px'>";
echo "<tr><th>Courses</th></tr>";
echo "<tr><td>";

$sql = "SELECT DISTINCT c.[id] as course_id, c.[name] as course_name FROM enrollment_term_dim t INNER JOIN course_dim c ON t.id = c.enrollment_term_id INNER JOIN account_dim a ON a.id=c.account_id INNER JOIN authorized_providers p ON p.canvas_sub_account_id=a.canvas_id WHERE p.canvas_sub_account_id = '".$provider_ID."' AND c.workflow_state = 'available' AND t.id IN (".$termArray.") ORDER BY c.id";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
echo "<input type='checkbox' name='course_id[]' id='course_".$row['course_id']."' class='course_checkbox' value='".$row['course_id']."' /> <label for='course_".$row['course_id']."'>".$row['course_name']."</label><br />";
}
odbc_free_result($result);

echo "<input type='button' class='course_select_all' id='course_select_all' value='Check All' /><br />";
?>
   <script type="text/javascript"> 
$('#course_select_all').ready(function(){
    $('.course_select_all:button').toggle(function(){
        $('.course_checkbox').attr('checked','checked');
        $(this).val('Uncheck All')
    },function(){
        $('.course_checkbox').removeAttr('checked');
        $(this).val('Check All');        
    })
})
</script>
<?php

echo "</td></tr>";
//echo '<input type="hidden" name="apArray_selected" value="'.$apArray.'" />';
echo '<input type="hidden" name="termArray_selected" value="'.$termArray.'" />';
echo "<tr><td><input type='submit' id='course_submit' name='course_submit' value='Filter Courses' /></td></tr>";
echo "</table>";
echo "</form>";
echo "</div>";

echo "<div id='course_results'>";
echo "<table border='1' cellspacing='0' cellpadding='5px' width='250px'>";
echo "<tr><th>Courses</th></tr>";
echo "<tr><td>";
$sql = "SELECT id as course_id, name as course_name FROM course_dim WHERE id IN(".$courseArray.") ORDER BY id";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
echo $row['course_name']."<br />";
}
odbc_free_result($result);
echo "</td></tr></table>";
echo "</div>";
echo "</div>";

////////////////////////////////////////////////////////////
///////////////// ASSIGNMENT DIV ///////////////////////////
////////////////////////////////////////////////////////////

echo "<div id='assignment_div'>";
echo "<div id='assignment_form'>";
echo "<form name='form_assignment' method='POST' action='csv_pull.php'>";
echo "<table border='1' cellspacing='0' cellpadding='5px' width='250px'>";
echo "<tr><th>Assignments</th></tr>";
echo "<tr><td>";

$sql = "SELECT DISTINCT s.title as assignment_name FROM enrollment_term_dim t INNER JOIN course_dim c ON t.id = c.enrollment_term_id INNER JOIN account_dim a ON a.id=c.account_id INNER JOIN authorized_providers p ON p.canvas_sub_account_id=a.canvas_id INNER JOIN assignment_dim s ON s.course_id=c.id WHERE p.canvas_sub_account_id = '".$provider_ID."' AND c.workflow_state = 'available' AND t.id IN (".$termArray.") AND c.id IN(".$courseArray.") AND (s.title LIKE ('%pretest%') OR s.title LIKE ('%posttest%') OR s.title LIKE ('%artifact%')) AND s.workflow_state='published' ORDER BY s.title";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
$assign_name = $row['assignment_name'];
$assign_find = array('Comprehension', 'Module', 'Administrator', 'ADMINISTRATOR', 'Optional', 'Biliteracy');
$assign_replace = array('Comp', 'Mod', 'Admin', 'Admin', 'Opt', 'Bilit');
$assign_abbr = str_replace($assign_find, $assign_replace, $assign_name);
echo "<input type='checkbox' name='assignment_name[]' id='".$row['assignment_name']."' class='assignment_checkbox' value='".$row['assignment_name']."' /> <label for='".$row['assignment_name']."'>".$assign_abbr."</label><br />";
}
odbc_free_result($result);

echo "<input type='button' class='assignment_select_all' id='assignment_select_all' value='Check All' /><br />";
?>
   <script type="text/javascript"> 
$('#assignment_select_all').ready(function(){
    $('.assignment_select_all:button').toggle(function(){
        $('.assignment_checkbox').attr('checked','checked');
        $(this).val('Uncheck All')
    },function(){
        $('.assignment_checkbox').removeAttr('checked');
        $(this).val('Check All');        
    })
})
</script>
<?php

echo "</td></tr>";
//echo '<input type="hidden" name="apArray_selected" value="'.$apArray.'" />';
echo '<input type="hidden" name="termArray_selected" value="'.$termArray.'" />';
echo '<input type="hidden" name="courseArray_selected" value="'.$courseArray.'" />';
echo "<tr><td><input type='submit' id='assignment_submit' name='assignment_submit' value='Filter Assignments' /></td></tr>";
echo "</table>";
echo "</form>";
echo "</div>";

echo "<div id='assignment_results'>";
echo "<table border='1' cellspacing='0' cellpadding='5px' width='250px'>";
echo "<tr><th>Assignments</th></tr>";
echo "<tr><td>";
if(isset($_POST['assignment_submit'])|isset($_POST['data_submit'])){
$sql = "SELECT DISTINCT title FROM assignment_dim WHERE title IN(".$assignmentArray.") ORDER BY title";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
$assign_name = $row['title'];
$assign_find = array('Comprehension', 'Module', 'Administrator', 'ADMINISTRATOR', 'Optional', 'Biliteracy');
$assign_replace = array('Comp', 'Mod', 'Admin', 'Admin', 'Opt', 'Bilit');
$assign_abbr = str_replace($assign_find, $assign_replace, $assign_name);
echo $assign_abbr."<br />";
}
odbc_free_result($result);
}
echo "</td></tr></table>";
echo "</div>";
echo "</div>";

////////////////////////////////////////////////////////////
/////////////////// DATA/CSV DIV ///////////////////////////
////////////////////////////////////////////////////////////

echo "<div id='data_div'>";
echo "<div id='data_form'>";
echo "&nbsp;<br />";
echo "<form name='form_data' method='POST' action='csv_export.php'>";
echo "<table border='0' cellspacing='0' cellpadding='5px' width='1000px'>";
echo "<tr><th>Data Results</th></tr>";
echo "<tr><td style='text-align:center;'>";
//echo '<input type="hidden" name="apArray_selected" value="'.$apArray.'" />';
echo '<input type="hidden" name="termArray_selected" value="'.$termArray.'" />';
echo '<input type="hidden" name="courseArray_selected" value="'.$courseArray.'" />';
echo '<input type="hidden" name="assignmentArray_selected" value="'.$assignmentArray.'" />';
echo "<input type='submit' id='data_submit' name='data_submit' value='Export ".$number_rows." Results' /></td></tr>";
echo "</table>";
echo "</form>";
echo "</div>";

echo "</div>";

odbc_close($conn);
}

echo "</div>";
?>

</body>
</html>

<?php
///////////////////IF SESSION IS NOT ACTIVE RUN BELOW////////
}else{

?>

<html>
<head>
<meta http-equiv="refresh" content="0;url=https://tra.esc11.net/ap/~dashboard.php" />
<title>TRA Reports Logout</title>
</head>
<body>
You are currently being directed to the login screen.
</body>
</html>

<?php
}
?>