<?php
session_start();
$provider_ID=$_SESSION['providerCanvas'];

$username = "tra_readonly";
$password = "D@taW4$!";
$dbname = "readWH";

$conn = odbc_connect("TRA_DW", $username, $password);
if (odbc_error()) {
 echo odbc_errormsg($conn);
}
else{

//////////WRITE CSV DATA FILE HERE/////////////////////////
if(isset($_POST['data_submit'])){

//$apArray = $_POST['apArray_selected'];
$termArray = $_POST['termArray_selected'];
$courseArray = $_POST['courseArray_selected'];
$assignmentArray = $_POST['assignmentArray_selected'];

$sql = "SELECT c.[name] as course_name, c.canvas_id as course_id, s.canvas_id as assignment_id, t.[name] as term_name, u.canvas_id as student_id, u.[name] as student_name, i.[name] as district, l.[name] as campus, s.title as assignment_name, m.attempt as attempts, m.grade, m.workflow_state as submission_status 
FROM [readWH].[dbo].[enrollment_term_dim] t 
INNER JOIN course_dim c ON t.id = c.enrollment_term_id 
INNER JOIN account_dim a ON a.id=c.account_id 
INNER JOIN authorized_providers p ON p.canvas_sub_account_id=a.canvas_id 
INNER JOIN assignment_dim s ON s.course_id=c.id 
INNER JOIN submission_dim m ON m.assignment_id=s.id 
INNER JOIN user_dim u ON u.id=m.[user_id] 
INNER JOIN user_demographics d ON d.user_id=u.canvas_id
LEFT OUTER JOIN districts i ON i.id=d.district_id
LEFT OUTER JOIN campuses l ON l.id=d.campus_id
WHERE p.canvas_sub_account_id = '".$provider_ID."' AND c.workflow_state = 'available' AND t.id IN (".$termArray.") AND c.id IN(".$courseArray.") AND s.title IN(".$assignmentArray.") ORDER BY s.title";
$result = odbc_exec($conn, $sql);

#I create a file and ready to write 
$myfile = fopen('php://output', 'w') or die("Unable to open file!");

#create an empty variable to story the result for string concatination
$fileData = null;
$fileData .="course_id,course_name,term_name,student_id,student_name,district,campus,assignment_id,assignment_name,attempts,grade,submission_status\n";

#I loop through the result with while loop
while($row = odbc_fetch_array($result)) {
//while(odbc_fetch_row($result)){

$fileData .= $row['course_id'].",";
$fileData .= $row['course_name'].",";
$fileData .= $row['term_name'].",";
$fileData .= $row['student_id'].",";
$fileData .= $row['student_name'].",";
$fileData .= $row['district'].",";
$fileData .= $row['campus'].",";
$fileData .= $row['assignment_id'].",";
$fileData .= '"'.$row['assignment_name'].'",';
$fileData .= $row['attempts'].",";
$fileData .= $row['grade'].",";
$fileData .= $row['submission_status']."\n";
 
 }

    
    header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=export.csv");

#write the entire data into the file and close the file
fwrite($myfile, $fileData);
//fputcsv($myfile, $fileData);
fclose($myfile);
}

}
?>