<?php
session_start();
if(isset($_SESSION['active_AP'])){$login="<a href='logout.php' class='logout'>[Logout]</a>";

$provider_ID=$_SESSION['providerCanvas'];

?>

<html>
<head>
<title>TEA Reading Academy - Course Offerings</title>
<style>
body{font-family:Tahoma;background-color:#efefef;color:#707476;}
a, a:visited, a:hover{color:white;text-decoration:none;}
div.pageMaster{width:1000px;background-color:#fff;border:1px solid #000;}
div.header{width:1000px;height:125px;border:0px solid #000;margin-top:20px;}
	div.logo{float:left;margin-left:15px;width:400px;border:0px solid green;}
	div.title{float:right;font-weight:bold;font-size:40pt;margin-right:30px;color:#0c6bba;text-transform:uppercase;margin-top:30px;}
	div.breadcrumb{font-size:10pt;margin-top:0px;padding-left:40px;color:#ef6036;width:960px;text-align:left;border:0px solid green;}
	div.breadcrumb a, div.breadcrumb a:visited, div.breadcrumb a:hover{color:#0c6bba;}
div.tiles{width:1000px;border:0px solid #000;height:400px;}
	div.tile1, div.tile2, div.tile3, div.tile4, div.tile5{margin-top:15px;margin-left:40px;width:280px;height:340px;float:left;border:1px solid black;color:white;text-align:center;}
	div.tile1{background-color:#ef6036;}
	div.tile2{background-color:#0c6bba;}
	div.tile3{background-color:#707476;}
	div.tile4{background-color:#18ba0c;display:none;}
	div.tile5{background-color:#baac0c;display:none;}
		div.tileTitle{font-size:24pt;margin-top:25px;}
		div.tileTotal{font-size:48pt;margin-bottom:50px;}
		div.breakdownLeft{text-align:left;float:left;margin-left:20px;}
		div.breakdownRight{text-align:right;margin-right:20px;}
			div.breakdownCat{font-size:20pt;text-transform:uppercase;border-bottom:0px solid #000;}
.logout, .logout a, .logout a:hover, .logout a:visited{font-size:8pt;color:#ef6036!important;}
div.welcome{float:right;color:#707476;font-size:10pt;margin-right:10px;}
#title{width:100%;}
h1{color:#707476;margin-bottom:5px;}
td{font-size:11px;}
</style>

</head>
<body>
<center>
<div class='pageMaster'>
<div class='welcome'><?php echo $_SESSION['firstName']." ".$_SESSION['lastName']."<br />".$login; ?></div>
<div class='header'>
	<div class='logo'><a href='~dashboard.php'><img src='../images/TRA_logo.jpg' /></a></div>
	<div class='title'>Courses by AP</div>
</div>
<h1>Course List by Authorized Provider</h1>
This report provides a list of all courses by Authorized Provider.
<hr />



<?php

$username = "tra_readonly";
$password = "D@taW4$!";
$dbname = "readWH";

$conn = odbc_connect("TRA_DW", $username, $password);
if (odbc_error()) {
 echo odbc_errormsg($conn);
}
else{

echo "<table cellpadding='2' cellspacing='0' border='1'>";
echo "<tr><td>Course</td><td>Term</td><tr>";

$sql = "
SELECT c.canvas_id, c.name as courseName, t.name AS termName 
FROM course_dim c 
INNER JOIN enrollment_term_dim t ON c.enrollment_term_id=t.id
INNER JOIN account_dim p ON c.account_id=p.id
WHERE p.canvas_id='".$provider_ID."' AND c.workflow_state = 'available' AND c.name NOT LIKE ('%Passport%') ORDER BY t.canvas_id, courseName";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
echo "<tr><td>".$row['canvas_id']." - ".$row['courseName']."</td><td>".$row['termName']."</td></tr>";
}
odbc_free_result($result);
echo "</table>";

odbc_close($conn);
}

///////////////////IF SESSION IS NOT ACTIVE RUN BELOW////////
}else{

?>

<html>
<head>
<meta http-equiv="refresh" content="0;url=https://tra.esc11.net/ap/~dashboard.php" />
<title>TRA Reports Logout</title>
</head>
<body>
You are currently being directed to the login screen.
</body>
</html>

<?php
}
?>