<?php
session_start();
if(isset($_SESSION['active_AP'])){$login="<a href='logout.php' class='logout'>[Logout]</a>";
?>


<html>
<head>
<title>TEA Reading Academy - Artifacts Report</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

<style>
body{font-family:Tahoma;background-color:#efefef;color:#707476;}
a, a:visited, a:hover{color:#ef6036;text-decoration:none;}
div.pageMaster{width:1050px;background-color:#fff;border:1px solid #000;}
div.header{width:1000px;height:125px;border:0px solid #000;margin-top:20px;}
	div.logo{float:left;margin-left:15px;width:400px;border:0px solid green;}
	div.title{float:right;font-weight:bold;font-size:36pt;margin-right:30px;color:#0c6bba;text-transform:uppercase;margin-top:30px;}
	div.breadcrumb{font-size:10pt;margin-top:0px;padding-left:40px;color:#ef6036;width:960px;text-align:left;border:0px solid green;}
	div.breadcrumb a, div.breadcrumb a:visited, div.breadcrumb a:hover{color:#0c6bba;}
td{font-size:13px;}
.logout, .logout a, .logout a:hover, .logout a:visited{font-size:8pt;color:#ef6036!important;}
div.welcome{float:right;color:#707476;font-size:10pt;margin-right:10px;}
</style>

</head>
<body>
<?php

$search_phrase='';
$provider_ID=$_SESSION['providerCanvas'];


echo "<center>";
echo "<div class='pageMaster'>";
echo "<div class='welcome'>".$_SESSION['firstName']." ".$_SESSION['lastName']."<br />".$login."</div>";
echo "<div class='header'>";
echo "<div class='logo'><a href='~dashboard.php'><img src='../images/TRA_logo.jpg' /></a></div>";
echo "<div class='title'>Assignment Search</div>";
echo "</div>";


$a='Module 2';

if(isset($_GET['get_data'])){
$a=$_GET['module'];
$search_phrase="Search Phrase: <span style='font-weight:bold;color:#ef6036;'>".$a."</span>";
}
$ap_id = $canvas_sub_account_id = '';

$username = "tra_readonly";
$password = "D@taW4$!";
$dbname = "readWH";

$conn = odbc_connect("TRA_DW", $username, $password);
if (odbc_error()) {
 echo odbc_errormsg($conn);
}
else{

echo "<h1>Assignment Completion for All Courses</h1>";
echo "This report allows you to see submissions and averages on all assignments. Terms to search for include:<br />artifact, check for understanding, quick check, pretest, posttest, and module.";
echo "<hr />";

echo "<form method='GET' action='artifacts_module.php'>";
echo "Search Term: <input type='text' name='module' /><br />";
echo "<input type='submit' name='get_data' value='Search' />";
echo "</form>";


echo $search_phrase."<br />";

echo "<table border='1' cellpadding='5' width='1000px'>";
echo"<tr><td>Assignment</td><td>Submissions</td><td>Average Grade</td><td># of Passing Scores(>= 80)</td></tr>";
//while($a < 14){
$sql2 = "
SELECT DISTINCT a.title as Assignment
  FROM assignment_dim a
  INNER JOIN course_dim c ON c.id=a.course_id
  INNER JOIN account_dim p ON p.id=c.account_id
  WHERE p.canvas_id = '".$provider_ID."'
  AND a.grading_type = 'points' AND a.workflow_state ='published' 
  AND c.workflow_state IN ('available', 'completed')
  AND (c.[name] LIKE ('% EOC%') OR c.[name] LIKE ('% EBC%') OR c.[name] LIKE ('% AOC%') OR c.[name] LIKE ('% BOC%')
  OR c.[name] LIKE ('% EOB%') OR c.[name] LIKE ('% EBB%') OR c.[name] LIKE ('% AOB%') OR c.[name] LIKE ('% BOB%'))
  AND title LIKE ('%".$a."%')
  ORDER BY a.title";
$result2 = odbc_exec($conn, $sql2);
while($row2 = odbc_fetch_array($result2)) {
$assignment=$row2['Assignment'];
echo"<tr><td>".$row2['Assignment']."</td>";

$sql3 = "
SELECT COUNT(s.grade) as Attempts, 
AVG(CAST(s.grade as decimal(10,2))) as Average
    FROM submission_dim s 
	INNER JOIN assignment_dim a ON a.id=s.assignment_id
	INNER JOIN course_dim c ON c.id=a.course_id
	INNER JOIN account_dim p ON c.account_id=p.id
	WHERE s.assignment_id IN (
	SELECT id
  FROM assignment_dim WHERE grading_type = 'points' AND title = '".$assignment."') AND s.workflow_state = 'graded'
  AND p.canvas_id = '".$provider_ID."'";


$result3 = odbc_exec($conn, $sql3);
while($row3 = odbc_fetch_array($result3)) {
$attempts=$row3['Attempts'];
echo "<td>".$row3['Attempts']."</td><td>".$row3['Average']."</td>";
}

$sql4 = "
SELECT COUNT(grade) as Passed
    FROM submission_dim WHERE assignment_id IN (
	SELECT a.id
  FROM assignment_dim a
  INNER JOIN course_dim c ON a.course_id=c.id
  WHERE grading_type = 'points' AND title LIKE ('".$assignment."')
  AND c.account_id LIKE('%0000".$provider_ID."')
) 
  AND workflow_state = 'graded' AND ((grade >= '80') OR (grade = '100')
  )";

$result4 = odbc_exec($conn, $sql4);
while($row4 = odbc_fetch_array($result4)) {
//$failed=$row4['Failed'];
$Passed = $row4['Passed'];

echo "<td>".$Passed."</td></tr>";
}



//$a++;
//}
}
//echo "<td></td><td></td><td></td><td></td><td>";

//echo "</tr>";
//}
odbc_free_result($result2);
odbc_free_result($result3);
echo "</table><br />&nbsp;";
odbc_close($conn);
}
echo "</div>";

?>


</body>
</html>

<?php
///////////////////IF SESSION IS NOT ACTIVE RUN BELOW////////
}else{

?>

<html>
<head>
<meta http-equiv="refresh" content="0;url=https://tra.esc11.net/ap/~dashboard.php" />
<title>TRA Authorized Provider Logout</title>
</head>
<body>
You are currently being logged out of the system.
</body>
</html>


<?php
}
?>