<?php
session_start();
if(isset($_SESSION['active_AP'])){$login="<a href='logout.php' class='logout'>[Logout]</a>";
$provider_ID=$_SESSION['providerCanvas'];
?>


<html>
<head>
<title>TEA Reading Academy - Module Progression Report</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

<style>
body{font-family:Tahoma;background-color:#efefef;color:#707476;}
a, a:visited, a:hover{color:#0c6bba;text-decoration:none;}
div.pageMaster{width:1050px;background-color:#fff;border:1px solid #000;padding-bottom:20x;}
div.header{width:1000px;height:125px;border:0px solid #000;margin-top:20px;}
	div.logo{float:left;margin-left:15px;width:400px;border:0px solid green;}
	div.title{float:right;font-weight:bold;font-size:36pt;margin-right:30px;color:#0c6bba;text-transform:uppercase;margin-top:30px;}
	div.breadcrumb{font-size:10pt;margin-top:0px;padding-left:40px;color:#ef6036;width:960px;text-align:left;border:0px solid green;}
	div.breadcrumb a, div.breadcrumb a:visited, div.breadcrumb a:hover{color:#0c6bba;}
#title{width:100%;}
h1{color:#707476;}
#ap_div{float:left;background-color:white;margin-left:10px;margin-top:5px;}
#ap_results, #term_results, #course_results, #assignment_results, #data_results{display:none;}
#ap_results td, #term_results td, #course_results td, #assignment_results td, #data_results td{font-size:9pt;}
#term_div, #course_div, #assignment_div{float:left;margin-left:10px;display:none;background-color:white;margin-top:5px;}
#data_div{margin-top:200px;width:1000px!important;display:none;}
#data_submit{background-color:red;color:White;font-size:24px;width:350px;}
label{font-size:9pt;}
#ap_select_all, #term_select_all, #course_select_all, #assignment_select_all{border:0px;background-color:transparent;color:red;}
td{font-size:11px;}
#course_details{display:none;}
.logout, .logout a, .logout a:hover, .logout a:visited{font-size:8pt;color:#ef6036!important;}
div.welcome{float:right;color:#707476;font-size:10pt;margin-right:10px;}
#btn_courseID{margin:8px 0px -5px 0px;}
</style>

</head>
<body>

<?php

$courseName=$inProgress=$pretest=$notStarted=$beginAvg=$growth= '';
$course_selected=$course_name=$termName=$provider='';
if(isset($_POST['course_selected'])){
$course_selected=$_POST['course_selected'];
echo "<style>";
echo "#course_details{display:block;}";
echo "</style>";
}

$ap_id = $canvas_sub_account_id = '';

$username = "tra_readonly";
$password = "D@taW4$!";
$dbname = "readWH";

$conn = odbc_connect("TRA_DW", $username, $password);
if (odbc_error()) {
 echo odbc_errormsg($conn);
}
else{


echo "<center>";
echo "<div class='pageMaster'>";
echo "<div class='welcome'>".$_SESSION['firstName']." ".$_SESSION['lastName']."<br />".$login."</div>";
echo "<div class='header'>";
echo "<div class='logo'><a href='~dashboard.php'><img src='../images/TRA_logo.jpg' /></a></div>";
echo "<div class='title'>Module Progress</div>";
echo "</div>";


echo "<h1>Module Progression by Authorized Provider</h1>";
echo "Module progression reports the module progress of learners at the course level and provides the change in percentage points from the pretests to the posttests.</span>";
echo "<hr />";



echo "<table cellpadding='2' cellspacing='0' border='1' width='1000px'>";
echo "<tr><td>Course ID</td><td>Provider</td><td>Course</td><td>Term</td><td>Enrolled</td><td>Completed</td><td>In Progress</td><td>Not Started</td><tr>";

$sql = "
SELECT c.id, c.canvas_id, c.enrollment_term_id, c.name as courseName, t.name AS termName, a.name AS provider, a.canvas_id as provider_ID, RIGHT(c.name, 5) as courseType 
FROM course_dim c 
INNER JOIN enrollment_term_dim t ON c.enrollment_term_id=t.id
INNER JOIN account_dim a ON a.id=c.account_id
WHERE c.workflow_state = 'available' AND c.name NOT LIKE ('%Passport%') AND t.name LIKE ('%Reading Academies%') AND a.workflow_state = 'active' AND a.canvas_id='".$provider_ID."' ORDER BY provider_ID, t.canvas_id";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
$course_ID=$row['canvas_id'];
$provider=$row['provider'];
echo "<tr><td style='text-align:center;'>";
if(($row['courseType']=='EBCK3')|($row['courseType']=='EBBK3')){
echo $row['canvas_id'];
}else{
echo "<form method='POST' action='module_progression.php'>";
echo "<input type='submit' name='course_selected' value='".$row['canvas_id']."' id='btn_courseID'>";
echo "</form>";
}
echo "</td><td>".$row['provider']."</td><td>".$row['courseName']."</td><td>".substr($row['termName'],18)."</td>";

$sql2 = "
SELECT COUNT(DISTINCT(student_id)) as Enrolled
  FROM course_completions WHERE course_id=".$course_ID." AND enrollment_status = 'active'";
$result2 = odbc_exec($conn, $sql2);
while($row2 = odbc_fetch_array($result2)) {
echo "<td>".$row2['Enrolled']."</td>";
$enrolled_number=$row2['Enrolled'];
}
$sql3 = "
SELECT COUNT(DISTINCT(student_id)) as Completed
  FROM course_completions WHERE course_id=".$course_ID." AND registration_status='completed' AND enrollment_status = 'active'";
$result3 = odbc_exec($conn, $sql3);
while($row3 = odbc_fetch_array($result3)) {
echo "<td>".$row3['Completed']."</td>";
}
$sql4 = "
SELECT COUNT(DISTINCT(student_id)) as Progress
  FROM course_completions WHERE course_id=".$course_ID." AND registration_status='started' AND enrollment_status = 'active'";
$result4 = odbc_exec($conn, $sql4);
while($row4 = odbc_fetch_array($result4)) {
echo "<td>".$row4['Progress']."</td>";
}
$sql4 = "
SELECT COUNT(DISTINCT(student_id)) as Register
  FROM course_completions WHERE course_id=".$course_ID." AND registration_status='registered' AND enrollment_status = 'active'";
$result4 = odbc_exec($conn, $sql4);
while($row4 = odbc_fetch_array($result4)) {
echo "<td>".$row4['Register']."</td>";
}

echo "</tr>";
}
odbc_free_result($result);
echo "</table><br />&nbsp;";

echo "<div id='course_details'>";
echo "<hr />";
echo "<h2>Module Progression by Course</h2>";
echo "<table cellpadding='2' cellspacing='0' border='1'>";
echo "<tr><td>Course ID</td><td>Provider</td><td>Module</td><td>Term</td>";
echo "<td>Attempts</td>";
echo "<td>Completed</td><td>In Progress</td><td>Not Started</td>";
echo "<td>Avg Growth</td><tr>";
$a=2;
while($a < 13){
$sql = "
SELECT DISTINCT
c.id as cid,
c.canvas_id as course_id,
a.title as courseName, 
c.[name] as course_name,
t.[name] as term_name,
a.id as assignment_id
  FROM submission_dim s
  INNER JOIN assignment_dim a ON a.id=s.assignment_id
  INNER JOIN course_dim c ON c.id=a.course_id
  INNER JOIN enrollment_term_dim t ON t.id=c.enrollment_term_id
    WHERE c.canvas_id = '".$course_selected."' AND (a.title LIKE ('%pretest%') OR a.title LIKE ('%posttest%')) AND (a.title LIKE ('% ".$a.":%') OR a.title LIKE ('% ".$a."B:%') OR a.title LIKE ('% ".$a."A:%'))
    ORDER BY a.title DESC";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
$courseName=$row['courseName'];
$course_ID=$row['course_id'];
$assignment_ID=$row['assignment_id'];
$cid=$row['cid'];
$course_name=$row['course_name'];
$termName=$row['term_name'];

if(strpos($courseName, 'Posttest') !== false){
echo "<tr><td>".$course_ID."</a></td><td>".$provider."</td><td>Module ".$a." Progression</td><td>".substr($row['term_name'],18)."</td>";
}

$sql2 = "
SELECT COUNT(student_id) as Enrolled
  FROM course_completions WHERE course_id=".$course_ID." AND enrollment_status = 'active'";
$result2 = odbc_exec($conn, $sql2);
while($row2 = odbc_fetch_array($result2)) {
if(strpos($courseName, 'Posttest') !== false){
echo "<td>".$row2['Enrolled']."</td>";
}
}


$sql3 = "
SELECT        COUNT(s.id) AS Completed
FROM            user_dim AS u INNER JOIN
                         submission_dim AS s INNER JOIN
                         assignment_dim AS a ON a.id = s.assignment_id ON u.id = s.user_id INNER JOIN
                         enrollment_dim ON u.id = enrollment_dim.user_id AND a.course_id = enrollment_dim.course_id
WHERE        (a.id = '".$assignment_ID."') AND (u.workflow_state = 'registered') AND (u.name NOT IN ('Test Student')) AND (a.course_id = '".$cid."') AND (enrollment_dim.workflow_state = 'active') AND 
                         (s.workflow_state = 'graded')";
$result3 = odbc_exec($conn, $sql3);
while($row3 = odbc_fetch_array($result3)) {

if(strpos($courseName, 'Posttest') !== false){
echo "<td>".$row3['Completed']."</td>";
}

if(strpos($courseName, 'Pretest') !== false){
$pretest=$row3['Completed'];
}else{
$inProgress = intval($pretest)-intval($row3['Completed']);
echo "<td>".$inProgress."</td>";
}


}

$sql4 = "
SELECT        COUNT(s.id) AS Unsubmitted
FROM            user_dim AS u INNER JOIN
                         submission_dim AS s INNER JOIN
                         assignment_dim AS a ON a.id = s.assignment_id ON u.id = s.user_id INNER JOIN
                         enrollment_dim ON u.id = enrollment_dim.user_id AND a.course_id = enrollment_dim.course_id
WHERE        (a.id = '".$assignment_ID."') AND (u.workflow_state = 'registered') AND (u.name NOT IN ('Test Student')) AND (a.course_id = '".$cid."') AND (enrollment_dim.workflow_state = 'active') AND 
                         (s.workflow_state = 'unsubmitted')";
$result4 = odbc_exec($conn, $sql4);
while($row4 = odbc_fetch_array($result4)) {
if(strpos($courseName, 'Pretest') !== false){
$notStarted = $row4['Unsubmitted'];
}else{
echo "<td>".$notStarted."</td>";
}


}


$sql5 = "
SELECT AVG(CAST(grade as decimal(10,2))) as Average
    FROM submission_dim s INNER JOIN assignment_dim a ON a.id=s.assignment_id WHERE a.id = ".$assignment_ID." AND s.workflow_state='graded'";
$result5 = odbc_exec($conn, $sql5);
while($row5 = odbc_fetch_array($result5)) {

if(strpos($courseName, 'Pretest') !== false){
$beginAvg = $row5['Average'];
}else{
$growth = $row5['Average'] - $beginAvg;
if(($growth <= 0) AND ($pretest < 1)) {
echo "<td>N/A</td></tr>";
}else{
echo "<td>".$growth." points</td></tr>";
}
}

}

}
$a++;
}

echo "</table><br />";

echo "<form method='post' action='module_progression_export.php'>";
echo "<input type='hidden' name='course_selected' value='".$course_selected."' />";
echo "<input type='hidden' name='courseName' value='".$course_name."' />";
echo "<input type='hidden' name='termName' value='".$termName."' />";
echo "<input type='hidden' name='provider' value='".$provider."' />";
echo "<input type='submit' name='btn_export' value='Export Results' />";
echo "</form>";

echo "</div>";

odbc_close($conn);
}
?>
</div>
</body>
</html>


<?php
///////////////////IF SESSION IS NOT ACTIVE RUN BELOW////////
}else{

?>

<html>
<head>
<meta http-equiv="refresh" content="0;url=https://tra.esc11.net/ap/~dashboard.php" />
<title>TRA Authorized Provider Logout</title>
</head>
<body>
You are currently being logged out of the system.
</body>
</html>

<?php
}
?>