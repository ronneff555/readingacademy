<html>
<head>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

<style>
body{font-family:Lucida Grande, Tahoma;font-size:14pt;}
table{width:1000px;}
table.validateForm{width:450px;font-size:10pt;}
td.validateTD{width:40%;text-align:right;padding:2px 5px 2px 0px;}
td.header{background-color:#999999;font-weight:bold;}
td.altrow{background-color:#eeeeee;}
td.redtext{color:red;font-style:italic;font-weight:bold;font-size:11pt;}
span.texttip{font-size:9pt;background-color:#ffffcc;}
select{margin-top:5px;}
span.error{color:red;}
div.applicationResults,
div.createNewUser{display:none;}
a{color:#f06037;}
h3{color:#0d6cb9;}
#goLogin, #goValidate, #goValidate2, #goValidate4{color:white;background-color:#0d6cb9;width:120px;height:30px;font-weight:bold;margin-top:10px;}
.box_login{width:300px;}
.box_A1,.box_A2,.box_A3{height:93px;background-color:transparent;}
.box_A1{background-image:URL(../images/cl_box/CL_box_A1.png);background-repeat:no-repeat;width:23px!important;background-position:top left;}
.box_A2{background-image:URL(../images/cl_box/CL_box_A2.png);background-repeat:repeat-x;background-position:top middle;}
.box_A2{color:white;text-align:center;font-size:18pt;}
.box_A3{background-image:URL(../images/cl_box/CL_box_A3.png);background-repeat:no-repeat;width:19px!important;background-position:top right;}

.box_B1,.box_B2,.box_B3{background-color:transparent;}
.box_B1{background-image:URL(../images/cl_box/CL_box_B1.png);background-repeat:repeat-y;width:23px;background-position:top left;}
.box_B2{color:#666;text-align:center;padding-top:10px;}
.box_B3{background-image:URL(../images/cl_box/CL_box_B3.png);background-repeat:repeat-y;width:19px;background-position:top right;}

.box_C1,.box_C2,.box_C3{height:18px;background-color:transparent;}
.box_C1{background-image:URL(../images/cl_box/CL_box_C1.png);background-repeat:no-repeat;width:23px;background-position:bottom left;}
.box_C2{background-image:URL(../images/cl_box/CL_box_C2.png);background-repeat:repeat-x;background-position:bottom;}
.box_C3{background-image:URL(../images/cl_box/CL_box_C3.png);background-repeat:no-repeat;width:19px;background-position:bottom right;}
</style>

</head>
<body>
<table cellpadding="5" cellspacing="0" border="0">
<tr><td>
<a href="../index.php"><img src="../images/TRA_logo.jpg" border="0" /></a></td></tr>
<tr><td><hr /></td></tr>
<tr><td>
<center>
<?PHP
// DEFINE BLANK VARIABLES
$leader_ID = $firstName = $lastName = $altEmail = $primaryPhone = $email = $uniqueID = $user_email = "";
$emailErr = $uniqueIDErr = $Invalid = $notFound = "";
$nextStep = $nextStep2 = "";

//FUNCTION TO STRIP THE SLASHES AND SPECIAL CHARACTERS OUT OF THE POST VALUES
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = str_replace("'", "\\'", $data);
  $data = htmlspecialchars($data);
  return $data;
}


//CONNECTION INFORMATION DETAILS
$servername = "localhost";
$username = "root";
$password = "R3@d1ngAc@d";
$dbname = "readacad";


//CREATE THE CONNECTION TO THE DATABASE
$conn = odbc_connect("ReadingAcademy", $username, $password);
if (odbc_error()) {
 echo odbc_errormsg($conn);
}
else{
// BELOW THIS WILL BE DISPLAYED BELOW IF CONNECTION IS SUCCESSFUL


//IF THE FORM IS SUBMITTED, EXECUTE SCRIPT
if(isset($_POST['Submit'])){
$a = "0";

if(empty($_POST['uniqueID'])){$uniqueIDErr = "<br />Unique ID Required<br />";$a++;}else{if (filter_var($_POST['uniqueID'], FILTER_VALIDATE_INT)){$uniqueID = test_input($_POST['uniqueID']);if(strlen($_POST['uniqueID']) < 10){$uniqueIDErr = "<br />10-Digits are Required<br />";$a++;}}else{$uniqueIDErr = "<br />Numbers Only<br />";$a++;}}
if(empty($_POST['email'])){$emailErr = "<br />Email Required";$a++;}else{if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){$emailErr="<br />Invalid email";$email = test_input($_POST['email']);$a++;}else{$email = test_input($_POST['email']);}}

//IF STATEMENT TO SHOW APPLICATION FORM IF $a HAS ANY MISSING REQUIRED FIELDS
if($a > 0){
echo "<style>";
echo "div.applicationForm{display:block;}";
echo "div.applicationResults{display:none;}";
echo "div.createNewUser{display:none;}";
echo "</style>";
}else {
//FORM IS VALIDATED SO INSERTS CAN BE MADE AND RESULTS ARE SHOWN
echo "<style>";
echo "div.applicationForm{display:none;}";
echo "div.applicationResults{display:block;}";
echo "div.createNewUser{display:none;}";
echo "</style>";

//CHECK TO SEE IF RECORD EXISTS IN TRA
$sql_checkTRA = "SELECT COUNT(active) as leadCount, leader_ID, firstName, lastName, email, altEmail, primaryPhone, canvas_ID, uniqueID FROM tbl_leader WHERE (email = '$email' OR altEmail = '$email') AND uniqueID = '$uniqueID'";
$result_checkTRA = odbc_exec($conn, $sql_checkTRA);
while($row_checkTRA = odbc_fetch_array($result_checkTRA)) {
if(($row_checkTRA['leadCount']) > 0){
if(($row_checkTRA['canvas_ID']) > 0){

$leader_ID = $row_checkTRA['leader_ID'];
$firstName = $row_checkTRA['firstName'];
$lastName = $row_checkTRA['lastName'];
$email = $row_checkTRA['email'];
$altEmail = $row_checkTRA['altEmail'];
$firstName = $row_checkTRA['firstName'];
$primaryPhone = $row_checkTRA['primaryPhone'];
$uniqueID = $row_checkTRA['uniqueID'];

/////////////////////////////////////////////////////////
//////////////VALIDATION FORM GOES HERE//////////////////
/////////////////////////////////////////////////////////

//$nextStep = "SHOW VALIDATION FORM<br />";
$nextStep="<form method='post' action='leader_dashboard.php'>";
$nextStep.="<input type='hidden' name='leader_ID' value='".$leader_ID."'>";
$nextStep.="<h3>Please Update Your Contact Information</h3>";
$nextStep.="<table cellpadding'0' cellspacing='0' class='validateForm'>";
$nextStep.="<tr><td class='validateTD'>First Name:</td><td>".$firstName."</td></tr>";
$nextStep.="<tr><td class='validateTD'>Last Name:</td><td>".$lastName."</td></tr>";
$nextStep.="<tr><td class='validateTD'>Email:</td><td><input type='textbox' name='email' value='".$email."' /></td></tr>";
$nextStep.="<tr><td class='validateTD'>Alt. Email:</td><td><input type='textbox' name='altEmail' value='".$altEmail."' /></td></tr>";
$nextStep.="<tr><td class='validateTD'>Primary Phone:</td><td><input type='textbox' name='primaryPhone' value='".$primaryPhone."' /></td></tr>";
$nextStep.="<tr><td colspan='2' align='center'><input type='Submit' id='goValidate' name='Update_CL' value='Continue' /></td></tr>";
$nextStep.="</table>";
$nextStep.="</form>";


/////////////////////////////////////////////////////////
//////////////VALIDATION FORM GOES HERE//////////////////
/////////////////////////////////////////////////////////

}else{

$leader_ID = $row_checkTRA['leader_ID'];
$firstName = $row_checkTRA['firstName'];
$lastName = $row_checkTRA['lastName'];
$email = $row_checkTRA['email'];
$firstName = $row_checkTRA['firstName'];
$primaryPhone = $row_checkTRA['primaryPhone'];
$uniqueID = $row_checkTRA['uniqueID'];

/////////////////////////////////////////////////////////
//////////////CHECK FOR THE CANVAS ID////////////////////
/////////////////////////////////////////////////////////
$access_token =  "15745~K0i2uEP3xSD9xbmyseE4nqOyJj6d5rhUC06nDGNwZwqOl3YRihvlEpERYBfjJvTr";
$user_email = $_POST['email'];

$crl = curl_init("https://tealearn.instructure.com/api/v1/accounts/1/users?search_term=".$user_email);

$headr = array();
$headr[] = 'Content-length: 0';
$headr[] = 'Content-type: application/json';
$headr[] = 'Authorization: Bearer '.$access_token;

curl_setopt($crl, CURLOPT_HTTPHEADER, $headr);
curl_setopt($crl, CURLOPT_HEADER, false);
curl_setopt($crl, CURLOPT_HTTPGET, true);
curl_setopt($crl, CURLOPT_FOLLOWLOCATION, false);
//curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);

$rest = curl_exec($crl);

//////////////// IF THE CANVAS USER EXISTS ///////////////
if (strpos($rest, ('"id":')) > -1){

/////////////////////////////////////////////////////////
//////////////INSERT CANVAS ID HERE//////////////////////
/////////////////////////////////////////////////////////

  $someArray = json_decode($rest, true);
echo "<style>";
echo "div.applicationForm{display:none;}";
echo "div.applicationResults{display:none;}";
echo "</style>";

//////////INSERT THE CANVAS ID INTO THE TRA DATABASE AND SHOW CONFIRMATION FORM/////////
$sql_updateTRA_canvasID = "UPDATE tbl_leader SET canvas_ID = '".$someArray[0]["id"]."' WHERE leader_ID = '".$leader_ID."'";
odbc_exec($conn, $sql_updateTRA_canvasID);

$nextStep="<form method='post' action='leader_dashboard.php'>";
$nextStep.="<input type='hidden' name='leader_ID' value='".$leader_ID."'>";
$nextStep.="<h3>Please Update Your Contact Information</h3>";
$nextStep.="<table cellpadding'0' cellspacing='0' class='validateForm'>";
$nextStep.="<tr><td class='validateTD'>First Name:</td><td>".$firstName."</td></tr>";
$nextStep.="<tr><td class='validateTD'>Last Name:</td><td>".$lastName."</td></tr>";
$nextStep.="<tr><td class='validateTD'>Email:</td><td><input type='textbox' name='email' value='".$email."' /></td></tr>";
$nextStep.="<tr><td class='validateTD'>Alt. Email:</td><td><input type='textbox' name='altEmail' value='".$altEmail."' /></td></tr>";
$nextStep.="<tr><td class='validateTD'>Primary Phone:</td><td><input type='textbox' name='primaryPhone' value='".$primaryPhone."' /></td></tr>";
$nextStep.="<tr><td colspan='2' align='center'><input type='Submit' id='goValidate2' name='Update_CL' value='Continue' /></td></tr>";
$nextStep.="</table>";
$nextStep.="</form>";

echo "<style>";
echo "div.applicationForm{display:none;}";
echo "div.applicationResults{display:block;}";
echo "div.createNewUser{display:none;}";
echo "</style>";

}

//////////////// IF THE CANVAS USER DOES NOT EXIST ///////////////
else{

$leader_ID = $row_checkTRA['leader_ID'];
$firstName = $row_checkTRA['firstName'];
$lastName = $row_checkTRA['lastName'];
$email = $row_checkTRA['email'];
$firstName = $row_checkTRA['firstName'];
$primaryPhone = $row_checkTRA['primaryPhone'];
$uniqueID = $row_checkTRA['uniqueID'];
/////////////////////////////////////////////////////////
//////////////CREATE A CANVAS USER HERE//////////////////
/////////////////////////////////////////////////////////

//CREATE CANVAS USER AND UPDATE TRA DATABASE
include '../includes/api_CreateNewUser.php';

$nextStep2="<form method='post' action='leader_dashboard.php'>";
$nextStep2.="<input type='hidden' name='leader_ID' value='".$leader_ID."'>";
$nextStep2.="<h3>Please Update Your Contact Information</h3>";
$nextStep2.="<table cellpadding'0' cellspacing='0' class='validateForm'>";
$nextStep2.="<tr><td class='validateTD'>First Name:</td><td>".$firstName."</td></tr>";
$nextStep2.="<tr><td class='validateTD'>Last Name:</td><td>".$lastName."</td></tr>";
$nextStep2.="<tr><td class='validateTD'>Email:</td><td><input type='textbox' name='email' value='".$email."' /></td></tr>";
$nextStep2.="<tr><td class='validateTD'>Alt. Email:</td><td><input type='textbox' name='altEmail' value='".$altEmail."' /></td></tr>";
$nextStep2.="<tr><td class='validateTD'>Primary Phone:</td><td><input type='textbox' name='primaryPhone' value='".$primaryPhone."' /></td></tr>";
$nextStep2.="<tr><td colspan='2' align='center'><input type='Submit' id='goValidate4' name='Update_CL' value='Continue' /></td></tr>";
$nextStep2.="</table>";
$nextStep2.="</form>";


echo "<style>";
echo "div.applicationForm{display:none;}";
echo "div.applicationResults{display:block;}";
echo "div.createNewUser{display:none;}";
echo "</style>";

/////////////////////////////////////////////////////////
//////////////CREATE A CANVAS USER HERE//////////////////
/////////////////////////////////////////////////////////
}


/////////////////////////////////////////////////////////
//////////////CHECK FOR THE CANVAS ID////////////////////
/////////////////////////////////////////////////////////


}
//ENDS IF $row_checkTRA COUNT
}else{
echo "<style>";
echo "div.applicationForm{display:block;}";
echo "div.applicationResults{display:none;}";
echo "div.createNewUser{display:none;}";
echo "</style>";
$b=0;
$sql_checkTRA_exist = "SELECT COUNT(leader_ID) as Count_Email FROM tbl_leader WHERE (email = '$email' OR altEmail = '$email')";
$result_checkTRA_exist = odbc_exec($conn, $sql_checkTRA_exist);
while($row_checkTRA_exist = odbc_fetch_array($result_checkTRA_exist)) {
if($row_checkTRA_exist['Count_Email'] > 0){
$Invalid = "<span class='error'>Unique ID Does Not Match Existing Email</span>";
$b++;
}}
$sql_checkTRA_exist2 = "SELECT COUNT(leader_ID) as Count_UniqueID FROM tbl_leader WHERE (uniqueID = '$uniqueID')";
$result_checkTRA_exist2 = odbc_exec($conn, $sql_checkTRA_exist2);
while($row_checkTRA_exist2 = odbc_fetch_array($result_checkTRA_exist2)) {
if($row_checkTRA_exist2['Count_UniqueID'] > 0){
$Invalid = "<span class='error'>Email Does Not Match Existing UniqueID</span>";
$b++;
}}
if($b==0){
echo "<style>";
echo "div.applicationForm{display:none;}";
echo "div.applicationResults{display:none;}";
echo "div.createNewUser{display:block;}";
echo "</style>";

$notFound = "User account cannot be found.";
$Invalid = "<span style='font-size:9pt;'>Click on the Create New User button to start<br />the Cohort Leader Application process.</span>";

}

}

//ENDS IF WHILE $row_checkTRA
}
//ENDS IF $a VALIDATED
}
// ENDS IF SUBMIT
}

?>




<div class="applicationForm">
<table cellspacing="0" cellpadding="0" border="0" class="box_Login">
<tr><td class="box_A1">&nbsp;</td><td class="box_A2">Cohort Leader<br />Login</td><td class="box_A3">&nbsp;</td></tr>
<tr><td class="box_B1"></td>
<td class="box_B2"><form method="POST" action="<?php echo ($_SERVER["PHP_SELF"]);?>">
Email: <input type='text' name='email' value='<?php echo $email; ?>' maxlength="45" /><span class="error"> * <?php echo $emailErr; ?></span><br />
TEA Unique ID: <input type='text' name='uniqueID' value='<?php echo $uniqueID; ?>' size="10" maxlength="10" /><span class="error"> *<?php echo $uniqueIDErr; ?></span><br />
<span style='font-size:9pt;'>If you have an existing Canvas account at tealearn.com, use the email address associated with that account.</span><br />
<input type='Submit' name="Submit" value='Account Search' id='goLogin' /></form><?php echo $Invalid; ?>
<span style='font-size:9pt;'>Please enter your email address and your TEA Unique ID number to search for your Reading Academy account.  If it is not found, you will be prompted to create a new account.<br /><br />Find more
information using the <br /><a href='https://tea.texas.gov/sites/default/files/Instructions%20for%20Finding%20Your%20Unique%20ID.pdf' target='_blank'>TEA Unique ID information guide</a>.<br />If you have not worked in Texas or do
not have a Unique ID, send an email to <a href='reading@tea.texas.gov'>Reading@TEA.Texas.gov.</a></span><br />&nbsp;</td>
<td class="box_B3"></td></tr>
<tr><td class="box_C1"></td><td class="box_C2"></td><td class="box_C3"></td></tr>
</table>
</div>

<div class="createNewUser">
<table cellspacing="0" cellpadding="0" border="0" class="box_Login">
<tr><td class="box_A1">&nbsp;</td><td class="box_A2">Cohort Leader<br />Login</td><td class="box_A3">&nbsp;</td></tr>
<tr><td class="box_B1"></td>
<td class="box_B2">
<?php echo $notFound; ?>
<form method="POST" action="../leader_application.php">
<input type='Submit' name="CreateUser" value="Create New User" id='goLogin' /></form>
<?php echo $Invalid; ?></td>
<td class="box_B3"></td></tr>
<tr><td class="box_C1"></td><td class="box_C2"></td><td class="box_C3"></td></tr>
</table>
</div>


<div class="applicationResults">
<table cellspacing="0" cellpadding="0" border="0" class="box_Login">
<tr><td class="box_A1">&nbsp;</td><td class="box_A2">Cohort Leader<br />Login</td><td class="box_A3">&nbsp;</td></tr>
<tr><td class="box_B1"></td>
<td class="box_B2"><?php 
echo $nextStep;
echo $nextStep2;
?></td>
<td class="box_B3"></td></tr>
<tr><td class="box_C1"></td><td class="box_C2"></td><td class="box_C3"></td></tr>
</table>
</div>
</center>
</td></tr></table>
</body>
</html>

<?php
odbc_close($conn);
}
?>