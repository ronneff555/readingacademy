<?php
include '../includes/api_getCanvasUserID.php';

$getToday = date('Y-m-d');
$windowend='2021-05-14';
//$nextWindow = 'July 5th';


?>


<html>
<head>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

<style>
body{font-family:Lucida Grande, Tahoma;font-size:14pt;margin-bottom:40px;}
table{width:1000px;}
table.tblDetails{background-color:#f7f6f6;width:490px;border:1px solid black;}
td.header{background-color:#999999;font-weight:bold;}
td.altrow, tr.altrow{background-color:#eeeeee;}
td.redtext{color:red;font-style:italic;font-weight:bold;font-size:11pt;}
span.texttip{font-size:9pt;background-color:#ffffcc;}
select{margin-top:5px;}
span.error{color:red;}
div.applicationResults{display:none;}
a{color:#f06037;}
h3{color:#0d6cb9;}
#goToLogin, #goRegister1, #goRegister2{color:white;background-color:#0d6cb9;width:200px;height:30px;font-weight:bold;}
td.passed, td.failed, td.not_taken{text-align:center;}
td.passed{background-color:green;color:white;font-weight:bold;}
td.failed{background-color:red;color:white;font-weight:bold;}
td.not_taken{background-color:#eeeeee;font-style:italic;font-weight:bold;}
td.blackcell{background-color:white;width:5px!important;padding:0px;}
td.whitecell{width:5px!important;padding:0px;}
</style>

</head>
<body>
<table cellpadding="5" cellspacing="0" border="0">
<tr><td>
<a href="../index.php"><img src="../images/TRA_logo.jpg" border="0" /></a>
<div style="float:right;"><a href='../index.php'>Sign Out</a></div></td></tr>
<tr><td><hr /></td></tr>
<tr><td>
<center>

<?PHP
// DEFINE BLANK VARIABLES
$leader_ID = $canvas_ID = $firstName = $lastName = $altEmail = $primaryPhone = $email = $uniqueID = $user_email = "";
$emailErr = $uniqueIDErr = $Invalid = $ENROLL = $courseID = $bilit_Course = $bilit_display =$elar_Course = $elar_display = "";
$nextStep = $activeWindow = $currentWindow = $regWindow = $enrollEnd = $enrollStart = "";
$screenerBilit = $screenerEng = $canvasEng = $canvasBilit = $panelEng = $panelBilit = "";
$enrollConfirm = "Thank you for enrolling in the Cohort Leader Screener. You will receive a detailed confirmation email.  You have seven days from today to complete the full screener process. ";
//FUNCTION TO STRIP THE SLASHES AND SPECIAL CHARACTERS OUT OF THE POST VALUES
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = str_replace("'", "\\'", $data);
  $data = htmlspecialchars($data);
  return $data;
}


//CONNECTION INFORMATION DETAILS
$servername = "localhost";
$username = "root";
$password = "R3@d1ngAc@d";
$dbname = "readacad";


//CREATE THE CONNECTION TO THE DATABASE
$conn = odbc_connect("ReadingAcademy", $username, $password);
if (odbc_error()) {
 echo odbc_errormsg($conn);
}
else{
// BELOW THIS WILL BE DISPLAYED BELOW IF CONNECTION IS SUCCESSFUL


$sql = "SELECT windowStart, bilit_all, bilit_canvas, bilit_panel, elar_all, elar_canvas, elar_panel FROM tbl_window_dates WHERE active = '1'";
$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {

$nextWindow = date_format(date_create($row['windowStart']), 'F jS');

$Bilit_All =$row['bilit_all'];
$Bilit_Canvas =$row['bilit_canvas'];
$Bilit_Panel = $row['bilit_panel'];
$ELAR_All = $row['elar_all'];
$ELAR_Canvas = $row['elar_canvas'];
$ELAR_Panel = $row['elar_panel'];

}
odbc_free_result($result);






//////////UPDATE LEADER CONTACT DETAILS///////
if(isset($_POST['Update_CL'])){
$sql_leaderDetails = "UPDATE tbl_leader SET email = '".$_POST['email']."', altEmail = '".$_POST['altEmail']."', primaryPhone = '".$_POST['primaryPhone']."' WHERE leader_ID = ".$_POST['leader_ID'];
//echo $sql_leaderDetails;
odbc_exec($conn, $sql_leaderDetails);
}

///////// ENROLL THE USER IN CANVAS COURSE ///////////
if(isset($_POST['Enroll_Bilit_Full'])){
$access_token =  "15745~K0i2uEP3xSD9xbmyseE4nqOyJj6d5rhUC06nDGNwZwqOl3YRihvlEpERYBfjJvTr";
$courseID = $_POST['courseID'];
$leader_ID = $_POST['leader_ID'];
$user_ID = $_POST['user_ID'];
$regWindow = $_POST['currentWindow'];
$enrollType='StudentEnrollment';
$enrollStatus='active';
$startDay = Date("Y-m-d H:i:s");
$startTime = Date("Y-m-d H:i:s", strtoTime($startDay)-36800);
$endTime = Date("Y-m-d H:i:s", strtoTime($startDay)+586800);

$crl2 = curl_init("https://tealearn.instructure.com/api/v1/courses/".$courseID."/enrollments");

$data2 = [
"enrollment" => array(
'user_id'      => $user_ID,
'type'  => $enrollType,
'enrollment_state' => $enrollStatus,
'start_at' => $startTime,
'end_at' => $endTime,
'notify' => false
)
];

$payload2 = ($data2);

$headr2 = array();
$headr2[] = 'Authorization: Bearer '.$access_token;

curl_setopt($crl2, CURLOPT_HTTPHEADER, $headr2);
curl_setopt($crl2, CURLOPT_HEADER, false);
curl_setopt($crl2, CURLOPT_POST, true);
curl_setopt($crl2, CURLOPT_FOLLOWLOCATION, false);
curl_setopt($crl2, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($crl2, CURLOPT_RETURNTRANSFER, true);
curl_setopt($crl2, CURLOPT_POSTFIELDS, http_build_query($payload2));

if($getToday == $windowend){
$rest2 = curl_exec($crl2);
}

if (strpos($rest2, ('"id":')) > -1){
  $someArray = json_decode(("[".$rest2."]"), true);
//$section_id = $someArray[0]["id"]; // Access Array data

$canvas_Enroll_ID =  $someArray[0]["id"]; // Access Array data
$enroll_Start = date("Y-m-d H:i:s", strtoTime($someArray[0]["start_at"]));
$enroll_End = date("Y-m-d H:i:s", strtoTime($someArray[0]["end_at"]));

$sql_enroll = "INSERT INTO tbl_canvas_reg (leader_ID, canvas_Enroll_ID, canvas_Course_ID, enroll_Start, enroll_End, win) VALUES ('".$leader_ID."', '".$canvas_Enroll_ID."', '".$courseID."', '".$enroll_Start."', '".$enroll_End."', '".$regWindow."')";
odbc_exec($conn, $sql_enroll);

$sql = "INSERT INTO tbl_api_log (canvasAPICall, canvasResult, canvasStatus) VALUES ('CREATE New User', '".$rest2."', 'Success')";
odbc_exec($conn, $sql);

}
curl_close($crl2);
}




if(isset($_POST['Enroll_Full'])){
$access_token =  "15745~K0i2uEP3xSD9xbmyseE4nqOyJj6d5rhUC06nDGNwZwqOl3YRihvlEpERYBfjJvTr";
$courseID = $_POST['courseID'];
$user_ID = $_POST['user_ID'];
$leader_ID = $_POST['leader_ID'];
$regWindow = $_POST['currentWindow'];
$enrollType='StudentEnrollment';
$enrollStatus='active';
$startDay = Date("Y-m-d H:i:s");
$startTime = Date("Y-m-d H:i:s", strtoTime($startDay)-36800);
$endTime = Date("Y-m-d H:i:s", strtoTime($startDay)+586800);

$crl2 = curl_init("https://tealearn.instructure.com/api/v1/courses/".$courseID."/enrollments");

$data2 = [
"enrollment" => array(
'user_id'      => $user_ID,
'type'  => $enrollType,
'enrollment_state' => $enrollStatus,
'start_at' => $startTime,
'end_at' => $endTime,
'notify' => false
)
];

$payload2 = ($data2);

$headr2 = array();
$headr2[] = 'Authorization: Bearer '.$access_token;

curl_setopt($crl2, CURLOPT_HTTPHEADER, $headr2);
curl_setopt($crl2, CURLOPT_HEADER, false);
curl_setopt($crl2, CURLOPT_POST, true);
curl_setopt($crl2, CURLOPT_FOLLOWLOCATION, false);
curl_setopt($crl2, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($crl2, CURLOPT_RETURNTRANSFER, true);
curl_setopt($crl2, CURLOPT_POSTFIELDS, http_build_query($payload2));

if($getToday == $windowend){
$rest2 = curl_exec($crl2);
}

if (strpos($rest2, ('"id":')) > -1){
  $someArray = json_decode(("[".$rest2."]"), true);
//$section_id = $someArray[0]["id"]; // Access Array data

$canvas_Enroll_ID =  $someArray[0]["id"]; // Access Array data
$enroll_Start = date("Y-m-d H:i:s", strtoTime($someArray[0]["start_at"]));
$enroll_End = date("Y-m-d H:i:s", strtoTime($someArray[0]["end_at"]));

$sql_enroll = "INSERT INTO tbl_canvas_reg (leader_ID, canvas_Enroll_ID, canvas_Course_ID, enroll_Start, enroll_End, win) VALUES ('".$leader_ID."', '".$canvas_Enroll_ID."', '".$courseID."', '".$enroll_Start."', '".$enroll_End."', '".$regWindow."')";
odbc_exec($conn, $sql_enroll);

$sql = "INSERT INTO tbl_api_log (canvasAPICall, canvasResult, canvasStatus) VALUES ('CREATE New User', '".$rest2."', 'Success')";
odbc_exec($conn, $sql);

}
curl_close($crl2);
}


?>

<h1>Cohort Leader Dashboard</h1>
<hr />
<?php
if(isset($_POST['leader_ID'])){
$leader_ID=$_POST['leader_ID'];

$sql = "SELECT uniqueID, canvas_ID, firstName, lastName, primaryPhone, email, canvasAdmin, altEmail, passedScreener, bilit, leaderType, trainingNotes, trainingRequested, trainingConfirmed, welcomeEmail, sandboxSetup, zoomSetup, emailSetup, setupEmail, centralEnrolled FROM tbl_leader WHERE leader_ID = ".$leader_ID;

$result = odbc_exec($conn, $sql);
while($row = odbc_fetch_array($result)) {
$uniqueID = $row['uniqueID'];
$email = $row['email'];
$canvas_ID = $row['canvas_ID'];

//if ($result->num_rows > 0) {
 //while($row = $result->fetch_assoc()) {


echo "<table cellspacing='0' cellpadding='5' border='0' width='1000px'>";
echo "<tr><td valign='top' wdith='50%'>";
echo "<h2>".$row['firstName']." ".$row['lastName']."</h2><br />";
//echo $row['providerName']."</h2>";
echo "Email: <a href='".$row['email']."'>".$row['email']."</a><br />";
echo "Alt. Email: <a href='".$row['altEmail']."'>".$row['altEmail']."</a><br />";
echo "Phone: ". $row['primaryPhone']."<br />";


echo "</td><td valign='top' width='50%'>";

echo "<table cellspacing='0' cellpadding='5' class='tblDetails'>";
echo "<tr><td valign='top' colspan='3'>";
echo "<div style='float:right;padding: 5px 5px 0px 0px;'><a href='http://tealearn.com' target='_blank'><img src='../images/TRA_Canvas_Login.png' /></a></div>";
echo "<h4>Quick View:</h4>";
echo "ELAR Certified: ";
if($row['passedScreener']==1){
echo "<span style='color:green;'>Yes</span>";
}else{
echo "<span style='color:red;'>No</span>";
}
echo "<br />";
echo "Bilit Certified: ";
if($row['bilit']==1){
echo "<span style='color:green;'>Yes</span>";
}else{
echo "<span style='color:red;'>No</span>";
}
echo "</td></tr>";
echo "</table>";

echo "</td></tr>";
echo "</table>";

echo "<style>";
echo "div.registerNoWindow{display:block;}";
echo "div.registerFailed{display:none;}";
echo "div.registerUpcoming{display:none;}";
echo "</style>";

echo "<hr /><h3>Register for Upcoming Cohort Leader Screener</h3>";
echo "<span style='font-size:10pt;'>You will have 12 months from the time of your first enrollment to successfuly complete the Cohort Leader training.  You must pass the Pre-Screener, Canvas Module, and Panel-Graded sections within this time frame to earn certification.  You will be given a maximum of 3 attempts during the time frame to pass all sections.  If you are unsuccessful during this time, your account will be reset 12 months from your first enrollment.</span>";
echo "<hr />";
$getToday = date('Y-m-d');
$sql_getWindow = "SELECT window_ID, windowName, windowStart, windowEnd FROM tbl_window_dates WHERE windowStart <= '".$getToday."' AND windowEnd >= '".$getToday."'";
$result_getWindow = odbc_exec($conn, $sql_getWindow);
while($row_getWindow = odbc_fetch_array($result_getWindow)) {
if($row_getWindow['window_ID'] > 0){
$currentWindow = $row_getWindow['windowName'];
echo "<h4 style='color:#f06037;'>The ".$row_getWindow['windowName']." screener is active!</h4>";
//echo Date("M d", strtoTime($row_getWindow['windowStart']))." - ".Date("M d", strtoTime($row_getWindow['windowEnd']));
$sql_getAttempts = "SELECT COUNT(id) as Attempts FROM tbl_screener_grades WHERE uniqueID = ".$uniqueID." AND archived='0'";
$result_getAttempts = odbc_exec($conn, $sql_getAttempts);
while($row_getAttempts = odbc_fetch_array($result_getAttempts)) {
if($row_getAttempts['Attempts'] > 2){
echo "<style>";
echo "div.registerNoWindow{display:none;}";
echo "div.registerFailed{display:block;}";
echo "div.registerUpcoming{display:none;}";
echo "</style>";
}else{
echo "<style>";
echo "div.registerNoWindow{display:none;}";
echo "div.registerFailed{display:none;}";
echo "div.registerUpcoming{display:block;}";
echo "</style>";
}
///ENDS GET COUNT OF ATTEMPTS
}

}else{
echo "<style>";
echo "div.registerNoWindow{display:block;}";
echo "div.registerFailed{display:none;}";
echo "div.registerUpcoming{display:none;}";
echo "</style>";
}
///ENDS THE SELECT GET WINDOWN ///
}

echo "<div class='registerNoWindow'>";
echo "<h4 style='color:red;background-color:yellow;'>There are no current screener windows open at this time.<br />The next window will open ".$nextWindow.".<br />Please check back to register when the window opens.</h4>";
echo "</div>";
echo "<div class='registerFailed'>";
echo "<b>You have exceeded your allotted number of attempts for this 12-month window.<br />You will be eligible for screener registration 12 months after your first attempt below.</b><br />&nbsp;";
echo "</div>";
echo "<div class='registerUpcoming'>";
echo "<table cellpadding='5px' cellspacing='2px'>";
echo "<tr style='font-weight:bold;color:white;text-align:center;'><td style='background-color:#0d6cb9;'>ELAR Track</td><td style='background-color:#f06037;'>Biliteracy/ELAR Track</td></tr>";
echo "<tr><td style='text-align:center;padding-top:10px;vertical-align:top;'>";

echo "<style>";
echo "div.regButtonELAR{display:block;}";
echo "div.regButtonBilit{display:block;}";
echo "div.registerExistsELAR{display:none;}";
echo "div.registerExistsBilit{display:none;}";
echo "</style>";

$sql_getReg = "SELECT COUNT(ID) as alreadyRegistered FROM tbl_canvas_reg WHERE leader_ID = ".$leader_ID." AND win = '".$currentWindow."'";
$result_getReg = odbc_exec($conn, $sql_getReg);
while($row_getReg = odbc_fetch_array($result_getReg)) {
if($row_getReg['alreadyRegistered'] > 0){
echo "<style>";
echo "div.regButtonELAR{display:none;}";
echo "div.regButtonBilit{display:none;}";
echo "div.registerExistsELAR{display:block;}";
echo "div.registerExistsBilit{display:block;}";
echo "</style>";
}
//$enrollEnd = $row_getReg['enroll_End'];
//$enrollStart = date('M-d-Y', strtoTime($row_getReg['enroll_Start']));
}



echo "<div class='registerExistsELAR'>";
echo "<h4>You have already registered for this window.</h4>Please login to Canvas to complete your course.";
echo "</div>";

$sql_getGrades = "SELECT COUNT(id) as panelEnglish FROM tbl_screener_grades WHERE (canvas_ID='".$canvas_ID."' OR email = '".$email."' OR uniqueID = ".$uniqueID.") AND panelEng = 'Yes'";
$result_getGrades = odbc_exec($conn, $sql_getGrades);
while($row_getGrades = odbc_fetch_array($result_getGrades)) {
if($row_getGrades['panelEnglish'] > 0){
echo "<img src='../images/TRA_ELAR_CERT.jpg' border='0' />";
$elar_display = 'display:none';
echo "<style>";
echo "div.regButtonELAR{display:none;}";
echo "div.registerExistsELAR{display:none;}";
echo "</style>";
}else{
$sql_getGrades = "SELECT COUNT(id) as canvasEnglish FROM tbl_screener_grades WHERE (canvas_ID='".$canvas_ID."' OR email = '".$email."' OR uniqueID = ".$uniqueID.") AND canvasEng = 'Yes' AND archived='0'";
$result_getGrades = odbc_exec($conn, $sql_getGrades);
while($row_getGrades = odbc_fetch_array($result_getGrades)) {
if($row_getGrades['canvasEnglish'] > 0){
//PANEL ONLY COURSE
$elar_Course = $ELAR_Panel;
}else{
$sql_getGrades = "SELECT COUNT(id) as screenerEnglish FROM tbl_screener_grades WHERE (canvas_ID='".$canvas_ID."' OR email = '".$email."' OR uniqueID = ".$uniqueID.") AND preScreenEng = 'Yes' and archived = '0'";
$result_getGrades = odbc_exec($conn, $sql_getGrades);
while($row_getGrades = odbc_fetch_array($result_getGrades)) {
if($row_getGrades['screenerEnglish'] > 0){
//CANVAS AND PANEL COURSE
$elar_Course = $ELAR_Canvas;
}else{
//SCREENER, CANVAS, AND PANEL COURSE
$elar_Course = $ELAR_All;
//$elar_Course = "2208";
}
}
}
}
}
}


if(isset($_POST['Enroll_Full'])){
	echo $enrollConfirm;
	//////// SNED CONFIRMATION EMAIL ///////
include 'email_RegConfirmation.php';

}else{
echo "<div class='regButtonELAR'>";
echo "Select the Enroll button to enroll in the ELAR Cohort Leader Screener.  You will immediately be enrolled in the course.<br />You will have 7 days to complete all course requirements from the time of enrollment.<br /><br />";
echo "<form method='POST' action='leader_dashboard.php' style='".$elar_display."'>";
echo "<input type='hidden' name='leader_ID' value='".$leader_ID."' />";
echo "<input type='hidden' name='user_ID' value='".$canvas_ID."' />";
echo "<input type='hidden' name='courseID' value='".$elar_Course."' />";
echo "<input type='hidden' name='currentWindow' value='".$currentWindow."' />";
echo "<input type='submit' name='Enroll_Full' value='Enroll in ELAR Training' id='goRegister1' />";
echo "</form>";
echo "</div>";
}
echo "</td>";
echo "<td width='50%' style='text-align:center;padding-top:10px;vertical-align:top;'>";


echo "<div class='registerExistsBilit'>";
echo "<h4>You have already registered for this window.</h4>Please login to Canvas to complete your course.";
echo "</div>";

$sql_getGrades = "SELECT COUNT(id) as panelBilit FROM tbl_screener_grades WHERE (canvas_ID='".$canvas_ID."' OR email = '".$email."' OR uniqueID = ".$uniqueID.") AND panelBilit = 'Yes'";
$result_getGrades = odbc_exec($conn, $sql_getGrades);
while($row_getGrades = odbc_fetch_array($result_getGrades)) {
if($row_getGrades['panelBilit'] > 0){
echo "<img src='../images/TRA_BILIT_CERT.jpg' border='0' />";
$bilit_display = 'display:none';
echo "<style>";
echo "div.regButtonBilit{display:none;}";
echo "div.registerExistsBilit{display:none;}";
echo "</style>";
}else{
$sql_getGrades = "SELECT COUNT(id) as canvasBilit FROM tbl_screener_grades WHERE (canvas_ID='".$canvas_ID."' OR email = '".$email."' OR uniqueID = ".$uniqueID.") AND canvasBilit = 'Yes' AND archived='0'";
$result_getGrades = odbc_exec($conn, $sql_getGrades);
while($row_getGrades = odbc_fetch_array($result_getGrades)) {
if($row_getGrades['canvasBilit'] > 0){
//PANEL ONLY COURSE
$bilit_Course = $Bilit_Panel;
}else{
$sql_getGrades = "SELECT COUNT(id) as screenerBilit FROM tbl_screener_grades WHERE (canvas_ID='".$canvas_ID."' OR email = '".$email."' OR uniqueID = ".$uniqueID.") AND preScreenBilit = 'Yes' AND archived='0'";
$result_getGrades = odbc_exec($conn, $sql_getGrades);
while($row_getGrades = odbc_fetch_array($result_getGrades)) {
if($row_getGrades['screenerBilit'] > 0){
//CANVAS AND PANEL COURSE
$bilit_Course = $Bilit_Canvas;
}else{
//SCREENER, CANVAS AND PANEL COURSE
$bilit_Course = $Bilit_All;
//$bilit_Course = "2211";
} 
}
}
}
}
}
if(isset($_POST['Enroll_Bilit_Full'])){
	echo $enrollConfirm;
	//////// SNED CONFIRMATION EMAIL ///////
include 'email_RegConfirmation.php';

}else{
echo "<div class='regButtonBilit'>";
echo "Select the Enroll button to enroll in the Biliteracy/ELAR Cohort Leader Screener.  You will immediately be enrolled in the course.<br />You will have 7 days to complete all course requirements from the time of enrollment.<br /><br />";
echo "<form method='POST' action='leader_dashboard.php' style='".$bilit_display."'>";
echo "<input type='hidden' name='leader_ID' value='".$leader_ID."' />";
echo "<input type='hidden' name='user_ID' value='".$canvas_ID."' />";
echo "<input type='hidden' name='courseID' value='".$bilit_Course."' />";
echo "<input type='hidden' name='currentWindow' value='".$currentWindow."' />";
echo "<input type='submit' name='Enroll_Bilit_Full' value='Enroll in Bilit/ELAR Training' id='goRegister2' />";
echo "</form>";
echo "</div>";
}

echo "</td></tr></table>";
echo "</div>";

echo "<hr /><h3>Cohort Leader Screener Results (Current 12 Month Cycle)</h3>";

echo "<hr />";
echo "<table cellspacing='2' cellpadding='10'>";
echo "<tr style='font-weight:bold;color:white;text-align:center;'><td></td><td colspan='3' style='background-color:#0d6cb9;'>ELAR Track</td><td class='blackcell'>&nbsp;</td><td colspan='3' style='background-color:#f06037;'>Biliteracy/ELAR Track</td></tr>";
echo "<tr style='font-weight:bold;text-decoration:underline;'><td></td><td>Pre-Screener</td><td>Canvas Module</td><td>Panel Graded</td><td class='blackcell'></td><td>Pre-Screener</td><td>Canvas Module</td><td>Panel Graded</td></tr>";
$alt=0;
$sql_getGrades = "SELECT preScreenEng, preScreenBilit, canvasEng, canvasBilit, panelEng, panelBilit, win, windowStart, windowEnd FROM tbl_screener_grades G INNER JOIN tbl_window_dates D ON G.win = D.windowName WHERE G.email = '".$email."' AND G.uniqueID = ".$uniqueID." AND G.archived='0' ORDER BY G.win DESC";
$result_getGrades = odbc_exec($conn, $sql_getGrades);
while($row_getGrades = odbc_fetch_array($result_getGrades)) {
$screenerEng = $row_getGrades['preScreenEng'];
$screenerBilit = $row_getGrades['preScreenBilit'];
$canvasEng = $row_getGrades['canvasEng'];
$canvasBilit = $row_getGrades['canvasBilit'];
$panelEng = $row_getGrades['panelEng'];
$panelBilit = $row_getGrades['panelBilit'];
$win = $row_getGrades['win'];
$windowStart = Date("M d", strtoTime($row_getGrades['windowStart']));
$windowEnd = Date("M d, Y", strtoTime($row_getGrades['windowEnd']));

echo "<tr><td><b>".$win."</b><br /><span style='font-size:10px;'>".$windowStart." - ".$windowEnd."</span></td>";
if($screenerEng == 'Yes'){echo "<td class='passed'>Passed</td>";}else if($screenerEng =='No'){echo "<td class='failed'>Not Passed</td>";}else{echo "<td class='not_taken'>N/A</td>";}
if($canvasEng == 'Yes'){echo "<td class='passed'>Passed</td>";}else if($canvasEng =='No'){echo "<td class='failed'>Not Passed</td>";}else{echo "<td class='not_taken'>N/A</td>";}
if($panelEng == 'Yes'){echo "<td class='passed'>Passed</td>";}else if($panelEng =='No'){echo "<td class='failed'>Not Passed</td>";}else{echo "<td class='not_taken'>N/A</td>";}
echo "<td class='blackcell'></td>";
if($screenerBilit == 'Yes'){echo "<td class='passed'>Passed</td>";}else if($screenerBilit =='No'){echo "<td class='failed'>Not Passed</td>";}else{echo "<td class='not_taken'>N/A</td>";}
if($canvasBilit == 'Yes'){echo "<td class='passed'>Passed</td>";}else if($canvasBilit =='No'){echo "<td class='failed'>Not Passed</td>";}else{echo "<td class='not_taken'>N/A</td>";}
if($panelBilit == 'Yes'){echo "<td class='passed'>Passed</td>";}else if($panelBilit =='No'){echo "<td class='failed'>Not Passed</td>";}else{echo "<td class='not_taken'>N/A</td>";}

}
echo "</table>";

echo "<hr /><br /><h3 style='color:#fa7d7d;'>Archived Cohort Leader Screener Results (Previous 12 Month Cycle)</h3>";

echo "<hr />";
echo "<table cellspacing='2' cellpadding='10' style='background-color:#fa7d7d;'>";
echo "<tr style='font-weight:bold;color:white;text-align:center;'><td></td><td colspan='3' style='background-color:#0d6cb9;'>ELAR Track</td><td class='whitecell'>&nbsp;</td><td colspan='3' style='background-color:#f06037;'>Biliteracy/ELAR Track</td></tr>";
echo "<tr style='font-weight:bold;text-decoration:underline;'><td></td><td>Pre-Screener</td><td>Canvas Module</td><td>Panel Graded</td><td class='whitecell'></td><td>Pre-Screener</td><td>Canvas Module</td><td>Panel Graded</td></tr>";
$alt=0;
$sql_getGrades = "SELECT preScreenEng, preScreenBilit, canvasEng, canvasBilit, panelEng, panelBilit, win, windowStart, windowEnd FROM tbl_screener_grades G INNER JOIN tbl_window_dates D ON G.win = D.windowName WHERE G.email = '".$email."' AND G.uniqueID = ".$uniqueID." AND G.archived='1' ORDER BY G.win DESC";
$result_getGrades = odbc_exec($conn, $sql_getGrades);
while($row_getGrades = odbc_fetch_array($result_getGrades)) {
$screenerEng = $row_getGrades['preScreenEng'];
$screenerBilit = $row_getGrades['preScreenBilit'];
$canvasEng = $row_getGrades['canvasEng'];
$canvasBilit = $row_getGrades['canvasBilit'];
$panelEng = $row_getGrades['panelEng'];
$panelBilit = $row_getGrades['panelBilit'];
$win = $row_getGrades['win'];
$windowStart = Date("M d", strtoTime($row_getGrades['windowStart']));
$windowEnd = Date("M d, Y", strtoTime($row_getGrades['windowEnd']));

echo "<tr><td><b>".$win."</b><br /><span style='font-size:10px;'>".$windowStart." - ".$windowEnd."</span></td>";
if($screenerEng == 'Yes'){echo "<td class='passed'>Passed</td>";}else if($screenerEng =='No'){echo "<td class='failed'>Not Passed</td>";}else{echo "<td class='not_taken'>N/A</td>";}
if($canvasEng == 'Yes'){echo "<td class='passed'>Passed</td>";}else if($canvasEng =='No'){echo "<td class='failed'>Not Passed</td>";}else{echo "<td class='not_taken'>N/A</td>";}
if($panelEng == 'Yes'){echo "<td class='passed'>Passed</td>";}else if($panelEng =='No'){echo "<td class='failed'>Not Passed</td>";}else{echo "<td class='not_taken'>N/A</td>";}
echo "<td class='whitecell'></td>";
if($screenerBilit == 'Yes'){echo "<td class='passed'>Passed</td>";}else if($screenerBilit =='No'){echo "<td class='failed'>Not Passed</td>";}else{echo "<td class='not_taken'>N/A</td>";}
if($canvasBilit == 'Yes'){echo "<td class='passed'>Passed</td>";}else if($canvasBilit =='No'){echo "<td class='failed'>Not Passed</td>";}else{echo "<td class='not_taken'>N/A</td>";}
if($panelBilit == 'Yes'){echo "<td class='passed'>Passed</td>";}else if($panelBilit =='No'){echo "<td class='failed'>Not Passed</td>";}else{echo "<td class='not_taken'>N/A</td>";}

}
echo "</table>";

	





echo "<span style='display:none;'>";
echo "<hr /><h3>COHORTS ASSIGNED</h3>";
echo "<hr /><h3>CONTINUING EDUCATION??</h3>";
echo "</span>";
echo "<hr />";
//}

//THIS ENDS THE IF ROWS EXIST
}
//THIS ENDS THE POST IF
}else{

echo "Your session has timed out.  Please click here to log back in.<br /><br />";
echo "<form action='/cl'>";
echo "<input type='submit' id='goToLogin' value='Go To Login' />";
echo "</form>";
}
?>

</center>
</td></tr></table>
</body>
</html>

<?php
odbc_close($conn);
}
?>