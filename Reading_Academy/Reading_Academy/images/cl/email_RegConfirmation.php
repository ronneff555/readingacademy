<?php
//$to = "rpeacock@esc11.net, rorypeacock@yahoo.com";
$subject = "Texas Reading Academy Registration";

$message = "
<html>
<head>
<title>HTML email</title>
</head>
<body>
<p>Thank you for registering to take the Texas Reading Academies Screener. You will have seven days from registration to complete the full screener process. This includes the</>
<ul>
<li>pre-screener;</li>
<li>Canvas-graded screener; and</li> 
<li>panel-graded screener.</li> 
</ul>
<p>You must pass the pre-screener with an 80% or higher to advance to the Canvas-graded artifacts, and must pass the Canvas-graded artifacts with an 80% or higher to gain access to the panel-graded artifacts.  If you do not pass one of the sections with an 80% or higher, you will not be permitted to advance to the next section.  You will, however, have the opportunity to take the screener again in another window.  Please note that you are only permitted to take the screener three times in a 12-month window.<?p>
<p>To access your screener, login to Canvas:</p>
<ol>
<li>Go to <a href='http://www.TEALearn.com'>tealearn.com</a></li>
<li>Type the primary email address you entered during the registration process</li>
<li>Your password is your 10-digit unique ID</li>
</ol>
<p>If you are unable to sign in, click the Forgot Password? hyperlink.  You will receive an email from Canvas to reset your password.  If you do not receive the password reset email from Canvas, please check your spam folder.</p>
<p>Once you are logged in to Canvas, please update your profile information.  Click on Account in the blue navigation bar on the left side of the screen, then click TEALearn Profile Information.  Fill out the required fields then click save.</p>
<p>You will access the screener from your Canvas Dashboard.  Click the Dashboard icon in the blue navigation bar on the left side of the screen.  Click the course card to begin the screener.  </p>
<p>If you have technical issues signing in to your Canvas account, please contact <a href='mailto:TEALearnHelp@esc11.net'>TEALearnHelp@esc11.net</a>.</p>
<p>If you have any questions about the screener process, please email TEA at <a href='mailto:reading@tea.texas.gov'>reading@tea.texas.gov</a>.</p>
</body>
</html>
";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: Texas Reading Academy Registration<TEALearnHelp@esc11.net>' . "\r\n";

mail($email,$subject,$message,$headers);
?>