USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[assignment_group_dim]    Script Date: 6/15/2021 1:26:58 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[assignment_group_dim]') AND type in (N'U'))
DROP TABLE [dbo].[assignment_group_dim]
GO

/****** Object:  Table [dbo].[assignment_group_dim]    Script Date: 6/15/2021 1:26:58 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[assignment_group_dim](
	[id] [bigint] NOT NULL,
	[canvas_id] [bigint] NULL,
	[course_id] [bigint] NULL,
	[name] [varchar](8000) NULL,
	[default_assignment_name] [varchar](8000) NULL,
	[workflow_state] [varchar](8000) NULL,
	[position] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL
	, CONSTRAINT PK_assignment_group_dim_ID PRIMARY KEY CLUSTERED (id)
) ON [PRIMARY]
GO


