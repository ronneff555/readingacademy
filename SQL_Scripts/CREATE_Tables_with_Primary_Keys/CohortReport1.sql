SELECT user_dim.name AS UserName
	, user_dim.sortable_name AS UserSortableName
	, user_dim.id AS User_ID
	, user_dim.canvas_id AS User_Canvas_ID
	, role_dim.name AS RoleName
	, course_dim.canvas_id as CourseCanvasID
	, course_dim.[name] as CourseName
	, account_dim.name AS CourseAccountName

-- , module_dim.course_id AS CourseID
	--, course_dim.account_id AS CourseAccountID
	--, enrollment_dim.id AS EnrollmentID
	--, enrollment_dim.user_id AS EnrollmentUserID
	--, enrollment_dim.canvas_id AS Enroll_canvas_id
	--, enrollment_dim.root_account_id AS Enroll_root_account
	--, enrollment_dim.course_id AS EnrollCourseID
	--, enrollment_dim.course_section_id AS Enroll_course_section
	--, enrollment_dim.role_id AS Role_ID	
	--, enrollment_dim.workflow_state AS EnrollWorkflowState
	--, enrollment_dim.created_at AS EnrollCreated
	--, enrollment_dim.updated_at AS EnrollUpdated
	--, enrollment_dim.start_at AS EnrollStartAt
	--, enrollment_dim.end_at AS EnrollEndAt
	--, enrollment_dim.completed_at AS EnrollCompletedAt
	--, enrollment_dim.self_enrolled AS EnrollSelfEnroled
	--, enrollment_dim.sis_source_id AS EnrollSIS_SourceID
	--, enrollment_dim.last_activity_at AS EnrollLastActivityAt
	--, module_dim.name AS ModuleName
	--, module_dim.workflow_state AS Module_workflow

FROM	enrollment_dim INNER JOIN
		user_dim ON enrollment_dim.user_id = user_dim.id INNER JOIN
		course_dim ON enrollment_dim.course_id = course_dim.id INNER JOIN
		account_dim ON account_dim.id = course_dim.account_id INNER JOIN
		module_dim ON course_dim.id = module_dim.course_id INNER JOIN
		role_dim ON enrollment_dim.role_id = role_dim.id
WHERE	(role_dim.name = 'Bil Cohort Leader' OR role_dim.name = 'Eng Cohort Leader')
and course_dim.canvas_id IN (263, 254, 255, 1750, 2216, 2376, 2669, 2960, 2357, 2358)
ORDER BY UserSortableName