USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[assignment_override_dim]    Script Date: 6/15/2021 1:36:39 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[assignment_override_dim]') AND type in (N'U'))
DROP TABLE [dbo].[assignment_override_dim]
GO

/****** Object:  Table [dbo].[assignment_override_dim]    Script Date: 6/15/2021 1:36:39 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[assignment_override_dim](
	[id] [bigint] NOT NULL,
	[canvas_id] [bigint] NULL,
	[assignment_id] [bigint] NULL,
	[course_section_id] [bigint] NULL,
	[group_id] [bigint] NULL,
	[quiz_id] [bigint] NULL,
	[all_day] [varchar](256) NULL,
	[all_day_date] [date] NULL,
	[assignment_version] [int] NULL,
	[created_at] [datetime] NULL,
	[due_at] [datetime] NULL,
	[due_at_overridden] [varchar](256) NULL,
	[lock_at] [datetime] NULL,
	[lock_at_overridden] [varchar](256) NULL,
	[set_type] [varchar](256) NULL,
	[title] [text] NULL,
	[unlock_at] [datetime] NULL,
	[unlock_at_overridden] [varchar](256) NULL,
	[updated_at] [datetime] NULL,
	[quiz_version] [int] NULL,
	[workflow_state] [varchar](256) NULL
	, CONSTRAINT PK_assignment_override_dim_ID PRIMARY KEY CLUSTERED (id)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


