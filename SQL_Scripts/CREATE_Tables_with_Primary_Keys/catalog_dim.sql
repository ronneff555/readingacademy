USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[catalog_dim]    Script Date: 6/15/2021 2:08:03 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[catalog_dim]') AND type in (N'U'))
DROP TABLE [dbo].[catalog_dim]
GO

/****** Object:  Table [dbo].[catalog_dim]    Script Date: 6/15/2021 2:08:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[catalog_dim](
	[id] [bigint] NOT NULL,
	[parent_id] [bigint] NULL,
	[name] [varchar](8000) NULL,
	[currency] [varchar](8000) NULL,
	[country] [varchar](8000) NULL,
	[time_zone] [varchar](8000) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL
	, CONSTRAINT PK_catalog_dim_ID PRIMARY KEY CLUSTERED (id)
) ON [PRIMARY]
GO


