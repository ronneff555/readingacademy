USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[campuses]    Script Date: 6/15/2021 2:06:10 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[campuses]') AND type in (N'U'))
DROP TABLE [dbo].[campuses]
GO

/****** Object:  Table [dbo].[campuses]    Script Date: 6/15/2021 2:06:10 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[campuses](
	[id] [bigint] NOT NULL,
	[campus_code] [varchar](8000) NULL,
	[name] [varchar](8000) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[deleted_at] [datetime] NULL,
	[district_id] [bigint] NULL,
	[workflow_state] [varchar](15) NULL,
	[source] [varchar](15) NULL
	, CONSTRAINT PK_campuses_ID PRIMARY KEY CLUSTERED (id)
) ON [PRIMARY]
GO


