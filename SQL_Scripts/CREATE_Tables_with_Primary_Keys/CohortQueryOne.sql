SELECT        TOP (200) user_dim.name, enrollment_dim.id, enrollment_dim.canvas_id, enrollment_dim.root_account_id, enrollment_dim.course_section_id, enrollment_dim.role_id, enrollment_dim.type, enrollment_dim.workflow_state, 
                         enrollment_dim.created_at, enrollment_dim.updated_at, enrollment_dim.start_at, enrollment_dim.end_at, enrollment_dim.completed_at, enrollment_dim.self_enrolled, enrollment_dim.sis_source_id, enrollment_dim.course_id, 
                         enrollment_dim.user_id, enrollment_dim.last_activity_at, course_dim.name AS CourseName, course_dim.account_id, account_dim.name AS AccountName, module_dim.course_id AS CourseID, 
                         module_dim.workflow_state AS module_workflow, module_dim.require_sequential_progress, module_dim.name AS ModuleName
FROM            enrollment_dim INNER JOIN
                         user_dim ON enrollment_dim.user_id = user_dim.id INNER JOIN
                         course_dim ON enrollment_dim.course_id = course_dim.id INNER JOIN
                         account_dim ON course_dim.account_id = account_dim.id INNER JOIN
                         module_dim ON course_dim.id = module_dim.course_id
WHERE        (enrollment_dim.role_id = 157450000000000038)
ORDER BY user_dim.sortable_name