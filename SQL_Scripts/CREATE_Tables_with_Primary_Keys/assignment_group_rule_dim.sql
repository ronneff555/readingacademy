USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[assignment_group_rule_dim]    Script Date: 6/15/2021 4:23:50 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[assignment_group_rule_dim]') AND type in (N'U'))
DROP TABLE [dbo].[assignment_group_rule_dim]
GO

/****** Object:  Table [dbo].[assignment_group_rule_dim]    Script Date: 6/15/2021 4:23:50 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[assignment_group_rule_dim](
	[assignment_group_id] [bigint] NULL,
	[drop_lowest] [int] NULL,
	[drop_highest] [int] NULL,
 [PriKey] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_assignment_group_rule_dim] PRIMARY KEY CLUSTERED 
(
	[PriKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


