USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[assignment_override_user_fact]    Script Date: 6/15/2021 1:41:18 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[assignment_override_user_fact]') AND type in (N'U'))
DROP TABLE [dbo].[assignment_override_user_fact]
GO

/****** Object:  Table [dbo].[assignment_override_user_fact]    Script Date: 6/15/2021 1:41:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[assignment_override_user_fact](
	[assignment_override_user_id] [bigint] NOT NULL,
	[account_id] [bigint] NULL,
	[assignment_group_id] [bigint] NULL,
	[assignment_id] [bigint] NULL,
	[assignment_override_id] [bigint] NULL,
	[course_id] [bigint] NULL,
	[enrollment_term_id] [bigint] NULL,
	[quiz_id] [bigint] NULL,
	[user_id] [bigint] NULL
	, CONSTRAINT PK_assignment_override_user_fact_ID PRIMARY KEY CLUSTERED (assignment_override_user_id)
) ON [PRIMARY]
GO


