USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[assignment_group_score_dim]    Script Date: 6/15/2021 1:32:44 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[assignment_group_score_dim]') AND type in (N'U'))
DROP TABLE [dbo].[assignment_group_score_dim]
GO

/****** Object:  Table [dbo].[assignment_group_score_dim]    Script Date: 6/15/2021 1:32:44 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[assignment_group_score_dim](
	[id] [bigint] NOT NULL,
	[canvas_id] [bigint] NULL,
	[assignment_group_id] [bigint] NULL,
	[enrollment_id] [bigint] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[workflow_state] [varchar](256) NULL
	, CONSTRAINT PK_assignment_group_score_dim_ID PRIMARY KEY CLUSTERED (id)
) ON [PRIMARY]
GO


