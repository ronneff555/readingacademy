----DECLARE @C_Leaders TABLE(
----	UserName varchar(1000),
----	UserSortableName varchar(1000),
----	User_ID bigint,
----	User_Canvas_ID bigint,
----	RoleName varchar(1000),
----	CourseCanvasID bigint,
----	CourseName varchar(1000),
----	CourseAccountName varchar(1000),
----	RecCount int)

----IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'#C_Leaders') AND type in (N'U'))
----DROP TABLE #C_Leaders
----CREATE TABLE #C_Leaders(
----	UserName varchar(1000),
----	UserSortableName varchar(1000),
----	User_ID bigint,
----	User_Canvas_ID bigint,
----	RoleName varchar(1000),
----	CourseCanvasID bigint,
----	CourseName varchar(1000),
----	CourseAccountName varchar(1000),
----	RecCount int)
--INSERT dbo.C_Leaders
--SELECT        user_dim.name AS UserName, user_dim.sortable_name AS UserSortableName, user_dim.id AS User_ID, user_dim.canvas_id AS User_Canvas_ID, role_dim.name AS RoleName, course_dim.canvas_id AS CourseCanvasID, 
--                         course_dim.name AS CourseName, account_dim.name AS CourseAccountName, COUNT(user_dim.name) AS RecCount
--FROM            enrollment_dim INNER JOIN
--                         user_dim ON enrollment_dim.user_id = user_dim.id INNER JOIN
--                         course_dim ON enrollment_dim.course_id = course_dim.id INNER JOIN
--                         account_dim ON account_dim.id = course_dim.account_id INNER JOIN
--                         module_dim ON course_dim.id = module_dim.course_id INNER JOIN
--                         role_dim ON enrollment_dim.role_id = role_dim.id
--GROUP BY user_dim.name, user_dim.sortable_name, user_dim.id, user_dim.canvas_id, role_dim.name, course_dim.canvas_id, course_dim.name, account_dim.name
--HAVING	(role_dim.name = 'Bil Cohort Leader' OR
--        role_dim.name = 'Eng Cohort Leader') 
--		AND (course_dim.canvas_id IN (263, 254, 255, 1750, 2216, 2376, 2669, 2960, 2357, 2358)
--)


--select * 
--FROM C_Leaders
--ORDER BY CourseName

SELECT        user_dim.name AS UserName, user_dim.sortable_name AS UserSortableName, user_dim.id AS User_ID, user_dim.canvas_id AS User_Canvas_ID, role_dim.name AS RoleName, course_dim.canvas_id AS CourseCanvasID, 
                         module_dim.name AS ModuleName, account_dim.name AS CourseAccountName, course_dim.name AS CourseName, enrollment_dim.created_at
FROM            role_dim INNER JOIN
                         enrollment_dim ON role_dim.id = enrollment_dim.role_id INNER JOIN
                         user_dim ON enrollment_dim.user_id = user_dim.id INNER JOIN
                         account_dim INNER JOIN
                         course_dim ON account_dim.id = course_dim.account_id INNER JOIN
                         module_dim ON course_dim.id = module_dim.course_id ON enrollment_dim.course_id = course_dim.id
WHERE        
(role_dim.name = 'Bil Cohort Leader' OR
role_dim.name = 'Eng Cohort Leader' )
AND course_dim.canvas_id IN (263, 254, 255, 1750, 2216, 2376, 2669, 2960, 2357, 2358)						 --- 
ORDER BY course_dim.name


