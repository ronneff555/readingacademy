USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[account_dim]    Script Date: 6/15/2021 1:12:05 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[account_dim]') AND type in (N'U'))
DROP TABLE [dbo].[account_dim]
GO

/****** Object:  Table [dbo].[account_dim]    Script Date: 6/15/2021 1:12:05 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[account_dim](
	[id] [bigint] NULL,
	[canvas_id] [bigint] NULL,
	[name] [varchar](8000) NULL,
	[depth] [int] NULL,
	[workflow_state] [varchar](8000) NULL,
	[parent_account] [varchar](8000) NULL,
	[parent_account_id] [bigint] NULL,
	[grandparent_account] [varchar](8000) NULL,
	[grandparent_account_id] [bigint] NULL,
	[root_account] [varchar](8000) NULL,
	[root_account_id] [bigint] NULL,
	[subaccount1] [varchar](8000) NULL,
	[subaccount1_id] [bigint] NULL,
	[subaccount2] [varchar](8000) NULL,
	[subaccount2_id] [bigint] NULL,
	[subaccount3] [varchar](8000) NULL,
	[subaccount3_id] [bigint] NULL,
	[subaccount4] [varchar](8000) NULL,
	[subaccount4_id] [bigint] NULL,
	[subaccount5] [varchar](8000) NULL,
	[subaccount5_id] [bigint] NULL,
	[subaccount6] [varchar](8000) NULL,
	[subaccount6_id] [bigint] NULL,
	[subaccount7] [varchar](8000) NULL,
	[subaccount7_id] [bigint] NULL,
	[subaccount8] [varchar](8000) NULL,
	[subaccount8_id] [bigint] NULL,
	[subaccount9] [varchar](8000) NULL,
	[subaccount9_id] [bigint] NULL,
	[subaccount10] [varchar](8000) NULL,
	[subaccount10_id] [bigint] NULL,
	[subaccount11] [varchar](8000) NULL,
	[subaccount11_id] [bigint] NULL,
	[subaccount12] [varchar](8000) NULL,
	[subaccount12_id] [bigint] NULL,
	[subaccount13] [varchar](8000) NULL,
	[subaccount13_id] [bigint] NULL,
	[subaccount14] [varchar](8000) NULL,
	[subaccount14_id] [bigint] NULL,
	[subaccount15] [varchar](8000) NULL,
	[subaccount15_id] [bigint] NULL,
	[sis_source_id] [varchar](8000) NULL,
	[PriKey] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_account_dim] PRIMARY KEY CLUSTERED 
(
	[PriKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO




