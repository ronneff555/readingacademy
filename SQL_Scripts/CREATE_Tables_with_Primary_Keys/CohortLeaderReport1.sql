SELECT        user_dim.name AS UserName, user_dim.sortable_name AS UserSortableName, user_dim.id AS User_ID, user_dim.canvas_id AS User_Canvas_ID, role_dim.name AS RoleName, course_dim.canvas_id AS CourseCanvasID, 
                         course_dim.name AS CourseName, account_dim.name AS CourseAccountName, COUNT(user_dim.name) AS RecCount
FROM            enrollment_dim INNER JOIN
                         user_dim ON enrollment_dim.user_id = user_dim.id INNER JOIN
                         course_dim ON enrollment_dim.course_id = course_dim.id INNER JOIN
                         account_dim ON account_dim.id = course_dim.account_id INNER JOIN
                         module_dim ON course_dim.id = module_dim.course_id INNER JOIN
                         role_dim ON enrollment_dim.role_id = role_dim.id
GROUP BY user_dim.name, user_dim.sortable_name, user_dim.id, user_dim.canvas_id, role_dim.name, course_dim.canvas_id, course_dim.name, account_dim.name
HAVING        (role_dim.name = 'Bil Cohort Leader' OR
                         role_dim.name = 'Eng Cohort Leader') AND (course_dim.canvas_id IN (263, 254, 255, 1750, 2216, 2376, 2669, 2960, 2357, 2358))
ORDER BY course_dim.name
