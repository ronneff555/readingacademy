SELECT        user_dim.id AS USER_ID, user_dim.canvas_id AS User_Canvas_ID, user_dim.sortable_name AS UserSortableName, user_dim.name AS UserName, enrollment_dim.id AS EnrollmentID, 
                         enrollment_dim.user_id AS EnrollmentUserID, enrollment_dim.canvas_id AS Enroll_canvas_id, enrollment_dim.course_id AS EnrollCourseID, enrollment_dim.role_id AS Role_ID, role_dim.name AS RoleName, 
                         course_dim.name AS CourseName, course_dim.account_id AS CourseAccountID, course_dim.canvas_id AS CourseCanvasID, enrollment_term_dim.date_start, enrollment_term_dim.date_end, enrollment_dim.start_at, 
                         enrollment_dim.end_at, enrollment_dim.completed_at, course_dim.start_at AS Expr1
FROM            enrollment_dim INNER JOIN
                         user_dim ON enrollment_dim.user_id = user_dim.id INNER JOIN
                         course_dim ON enrollment_dim.course_id = course_dim.id INNER JOIN
                         role_dim ON enrollment_dim.role_id = role_dim.id INNER JOIN
                         enrollment_term_dim ON course_dim.enrollment_term_id = enrollment_term_dim.id
WHERE        (role_dim.name = 'Bil Cohort Leader' OR
                         role_dim.name = 'Eng Cohort Leader') 
						 AND (course_dim.canvas_id IN (263, 254, 255, 1750, 2216, 2376, 2669, 2960, 2357, 2358))
ORDER BY CourseName