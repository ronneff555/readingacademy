USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[authorized_providers]    Script Date: 6/15/2021 1:55:12 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[authorized_providers]') AND type in (N'U'))
DROP TABLE [dbo].[authorized_providers]
GO

/****** Object:  Table [dbo].[authorized_providers]    Script Date: 6/15/2021 1:55:12 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[authorized_providers](
	[id] [bigint] NOT NULL,
	[name] [varchar](8000) NULL,
	[workflow_state] [varchar](15) NULL,
	[primary_contact_info] [text] NULL,
	[canvas_sub_account_id] [int] NULL,
	[catalog_id] [int] NULL,
	[purchase_order_validation_enabled] [bit] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[deleted_at] [datetime] NULL,
	[educational_service_center_id] [bigint] NULL,
	[educational_service_center_code] [varchar](64) NULL
	, CONSTRAINT PK_authorized_providers_ID PRIMARY KEY CLUSTERED (id)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


