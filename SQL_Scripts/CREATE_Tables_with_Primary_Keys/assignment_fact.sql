USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[assignment_fact]    Script Date: 6/15/2021 4:17:05 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[assignment_fact]') AND type in (N'U'))
DROP TABLE [dbo].[assignment_fact]
GO

/****** Object:  Table [dbo].[assignment_fact]    Script Date: 6/15/2021 4:17:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[assignment_fact](
	[assignment_id] [bigint] NULL,
	[course_id] [bigint] NULL,
	[course_account_id] [bigint] NULL,
	[enrollment_term_id] [bigint] NULL,
	[points_possible] [float] NULL,
	[peer_review_count] [int] NULL,
	[assignment_group_id] [bigint] NULL,
	[external_tool_id] [bigint] NULL,
	[PriKey] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_assignment_fact] PRIMARY KEY CLUSTERED 
(
	[PriKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


