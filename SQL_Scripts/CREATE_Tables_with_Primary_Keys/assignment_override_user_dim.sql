USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[assignment_override_user_dim]    Script Date: 6/15/2021 1:40:06 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[assignment_override_user_dim]') AND type in (N'U'))
DROP TABLE [dbo].[assignment_override_user_dim]
GO

/****** Object:  Table [dbo].[assignment_override_user_dim]    Script Date: 6/15/2021 1:40:06 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[assignment_override_user_dim](
	[id] [bigint] NOT NULL,
	[canvas_id] [bigint] NULL,
	[assignment_id] [bigint] NULL,
	[assignment_override_id] [bigint] NULL,
	[quiz_id] [bigint] NULL,
	[user_id] [bigint] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL
	, CONSTRAINT PK_assignment_override_user_dim_ID PRIMARY KEY CLUSTERED (id)
) ON [PRIMARY]
GO


