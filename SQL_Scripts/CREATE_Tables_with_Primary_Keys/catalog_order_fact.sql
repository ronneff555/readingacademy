USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[catalog_order_fact]    Script Date: 6/15/2021 2:13:04 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[catalog_order_fact]') AND type in (N'U'))
DROP TABLE [dbo].[catalog_order_fact]
GO

/****** Object:  Table [dbo].[catalog_order_fact]    Script Date: 6/15/2021 2:13:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[catalog_order_fact](
	[catalog_order_id] [bigint] NOT NULL,
	[catalog_id] [bigint] NULL,
	[parent_catalog_id] [bigint] NULL,
	[user_id] [bigint] NULL,
	[total] [float] NULL
	, CONSTRAINT PK_catalog_order_fact_ID PRIMARY KEY CLUSTERED (catalog_order_id)
) ON [PRIMARY]
GO


