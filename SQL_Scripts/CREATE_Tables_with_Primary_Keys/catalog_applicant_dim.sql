USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[catalog_applicant_dim]    Script Date: 6/15/2021 2:07:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[catalog_applicant_dim]') AND type in (N'U'))
DROP TABLE [dbo].[catalog_applicant_dim]
GO

/****** Object:  Table [dbo].[catalog_applicant_dim]    Script Date: 6/15/2021 2:07:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[catalog_applicant_dim](
	[id] [bigint] NOT NULL,
	[catalog_id] [bigint] NULL,
	[catalog_product_id] [bigint] NULL,
	[user_id] [bigint] NULL,
	[notified_of_opening_at] [datetime] NULL,
	[activated] [bit] NULL,
	[status] [varchar](8000) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL
	, CONSTRAINT PK_catalog_applicant_dim_ID PRIMARY KEY CLUSTERED (id)
) ON [PRIMARY]
GO


