USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[assignment_group_fact]    Script Date: 6/15/2021 1:28:49 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[assignment_group_fact]') AND type in (N'U'))
DROP TABLE [dbo].[assignment_group_fact]
GO

/****** Object:  Table [dbo].[assignment_group_fact]    Script Date: 6/15/2021 1:28:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[assignment_group_fact](
	[assignment_group_id] [bigint] NOT NULL,
	[course_id] [bigint] NULL,
	[group_weight] [float] NULL
	, CONSTRAINT PK_assignment_group_fact_ID PRIMARY KEY CLUSTERED (assignment_group_id)
) ON [PRIMARY]
GO


