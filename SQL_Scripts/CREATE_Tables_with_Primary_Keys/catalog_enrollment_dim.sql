USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[catalog_enrollment_dim]    Script Date: 6/15/2021 2:09:42 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[catalog_enrollment_dim]') AND type in (N'U'))
DROP TABLE [dbo].[catalog_enrollment_dim]
GO

/****** Object:  Table [dbo].[catalog_enrollment_dim]    Script Date: 6/15/2021 2:09:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[catalog_enrollment_dim](
	[id] [bigint] NOT NULL,
	[catalog_product_id] [bigint] NULL,
	[user_id] [bigint] NULL,
	[root_program_id] [bigint] NULL,
	[status] [varchar](8000) NULL,
	[requirements_completed_at] [datetime] NULL,
	[ends_at] [datetime] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL
	, CONSTRAINT PK_catalog_enrollment_dim_ID PRIMARY KEY CLUSTERED (id)
) ON [PRIMARY]
GO


