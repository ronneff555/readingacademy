USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[assignment_override_user_rollup_fact]    Script Date: 6/15/2021 1:42:32 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[assignment_override_user_rollup_fact]') AND type in (N'U'))
DROP TABLE [dbo].[assignment_override_user_rollup_fact]
GO

/****** Object:  Table [dbo].[assignment_override_user_rollup_fact]    Script Date: 6/15/2021 1:42:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[assignment_override_user_rollup_fact](
	[assignment_id] [bigint] NOT NULL,
	[assignment_override_id] [bigint] NULL,
	[assignment_override_user_adhoc_id] [bigint] NULL,
	[assignment_group_id] [bigint] NULL,
	[course_id] [bigint] NULL,
	[course_account_id] [bigint] NULL,
	[course_section_id] [bigint] NULL,
	[enrollment_id] [bigint] NULL,
	[enrollment_term_id] [bigint] NULL,
	[group_category_id] [bigint] NULL,
	[group_id] [bigint] NULL,
	[group_parent_account_id] [bigint] NULL,
	[group_wiki_id] [bigint] NULL,
	[nonxlist_course_id] [bigint] NULL,
	[quiz_id] [bigint] NULL,
	[user_id] [bigint] NULL
	, CONSTRAINT PK_assignment_override_user_rollup_fact_ID PRIMARY KEY CLUSTERED (assignment_id)
) ON [PRIMARY]
GO


