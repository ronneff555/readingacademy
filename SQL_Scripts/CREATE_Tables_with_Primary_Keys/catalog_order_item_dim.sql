USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[catalog_order_item_dim]    Script Date: 6/15/2021 2:14:09 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[catalog_order_item_dim]') AND type in (N'U'))
DROP TABLE [dbo].[catalog_order_item_dim]
GO

/****** Object:  Table [dbo].[catalog_order_item_dim]    Script Date: 6/15/2021 2:14:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[catalog_order_item_dim](
	[id] [bigint] NOT NULL,
	[catalog_order_id] [bigint] NULL,
	[catalog_product_id] [bigint] NULL,
	[catalog_promotion_id] [bigint] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL
	, CONSTRAINT PK_catalog_order_item_dim_ID PRIMARY KEY CLUSTERED (id)
) ON [PRIMARY]
GO


