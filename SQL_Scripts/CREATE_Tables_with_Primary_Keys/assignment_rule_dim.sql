USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[assignment_rule_dim]    Script Date: 6/15/2021 1:53:27 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[assignment_rule_dim]') AND type in (N'U'))
DROP TABLE [dbo].[assignment_rule_dim]
GO

/****** Object:  Table [dbo].[assignment_rule_dim]    Script Date: 6/15/2021 1:53:27 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[assignment_rule_dim](
	[assignment_id] [bigint] NOT NULL,
	[drop_rule] [varchar](8000) NULL
	, CONSTRAINT PK_assignment_rule_dim_ID PRIMARY KEY CLUSTERED (assignment_id)
) ON [PRIMARY]
GO


