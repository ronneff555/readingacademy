USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[assignment_dim]    Script Date: 6/15/2021 4:25:21 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[assignment_dim]') AND type in (N'U'))
DROP TABLE [dbo].[assignment_dim]
GO

/****** Object:  Table [dbo].[assignment_dim]    Script Date: 6/15/2021 4:25:21 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[assignment_dim](
	[id] [bigint] NULL,
	[canvas_id] [bigint] NULL,
	[course_id] [bigint] NULL,
	[title] [varchar](8000) NULL,
	[description] [text] NULL,
	[due_at] [datetime] NULL,
	[unlock_at] [datetime] NULL,
	[lock_at] [datetime] NULL,
	[points_possible] [float] NULL,
	[grading_type] [varchar](8000) NULL,
	[submission_types] [varchar](8000) NULL,
	[workflow_state] [varchar](8000) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[peer_review_count] [int] NULL,
	[peer_reviews_due_at] [datetime] NULL,
	[peer_reviews_assigned] [bit] NULL,
	[peer_reviews] [bit] NULL,
	[automatic_peer_reviews] [bit] NULL,
	[all_day] [bit] NULL,
	[all_day_date] [date] NULL,
	[could_be_locked] [bit] NULL,
	[grade_group_students_individually] [bit] NULL,
	[anonymous_peer_reviews] [bit] NULL,
	[muted] [bit] NULL,
	[assignment_group_id] [bigint] NULL,
	[position] [int] NULL,
	[visibility] [varchar](256) NULL,
	[external_tool_id] [bigint] NULL,
	[PriKey] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_assignment_dim] PRIMARY KEY CLUSTERED 
(
	[PriKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


