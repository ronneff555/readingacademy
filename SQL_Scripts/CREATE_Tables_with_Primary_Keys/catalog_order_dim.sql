USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[catalog_order_dim]    Script Date: 6/15/2021 2:11:24 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[catalog_order_dim]') AND type in (N'U'))
DROP TABLE [dbo].[catalog_order_dim]
GO

/****** Object:  Table [dbo].[catalog_order_dim]    Script Date: 6/15/2021 2:11:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[catalog_order_dim](
	[id] [bigint] NOT NULL,
	[catalog_id] [bigint] NULL,
	[user_id] [bigint] NULL,
	[full_id] [varchar](8000) NULL,
	[purchased_at] [datetime] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[currency] [varchar](8000) NULL
	, CONSTRAINT PK_catalog_order_dim_ID PRIMARY KEY CLUSTERED (id)
) ON [PRIMARY]
GO


