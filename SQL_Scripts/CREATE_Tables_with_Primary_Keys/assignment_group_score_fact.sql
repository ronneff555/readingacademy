USE [Canvas_Test]
GO

/****** Object:  Table [dbo].[assignment_group_score_fact]    Script Date: 6/15/2021 1:34:16 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[assignment_group_score_fact]') AND type in (N'U'))
DROP TABLE [dbo].[assignment_group_score_fact]
GO

/****** Object:  Table [dbo].[assignment_group_score_fact]    Script Date: 6/15/2021 1:34:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[assignment_group_score_fact](
	[score_id] [bigint] NOT NULL,
	[canvas_id] [bigint] NULL,
	[account_id] [bigint] NULL,
	[course_id] [bigint] NULL,
	[assignment_group_id] [bigint] NULL,
	[enrollment_id] [bigint] NULL,
	[current_score] [float] NULL,
	[final_score] [float] NULL,
	[muted_current_score] [float] NULL,
	[muted_final_score] [float] NULL
	, CONSTRAINT PK_assignment_group_score_fact_ID PRIMARY KEY CLUSTERED (score_id)
) ON [PRIMARY]
GO


